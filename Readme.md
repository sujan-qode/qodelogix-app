1. npm install
	- installs dependencies
2. ionic cordova platform add <platform_name> ( android, ios, browser )
	- adds platforms
3. ionic cordova build <platform_name> 
	- adds build in www
4. ionic cordova resources
	- regenerates icons and splash icons

## Note for IOS issues
1. For Braintree Issue in IOS
	- Remove cordova-plugin-braintree
		ionic cordova plugin remove cordova-plugin-braintree
	- Add stable Braintree plugin
		ionic cordova plugin add https://github.com/dpa99c/cordova-plugin-braintree --save

2. For CORS issue while using Assurant API in IOS
	- ionic cordova plugin remove cordova-plugin-ionic-webview --save
	- rm -rf platforms/
	- rm -rf plugins/
	- ionic cordova build ios
	
## Custom Splash Screen in app.html
	https://coursetro.com/posts/code/51/How-to-Make-an-Animated-Ionic-Splash-Page-with-HTML-&-CSS