import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
// import { RentalManagerRentalInformationPage } from '../rental-manager-rental-information/rental-manager-rental-information';
import { customFunctions } from '../../providers/functions';
// import { RentalManagerSuccessPage } from '../rental-manager-success/rental-manager-success';

/**
 * Generated class for the RentalManagerLocateVehicle_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-locate-vehicle-2',
  templateUrl: 'rental-manager-locate-vehicle-2.html',
})
export class RentalManagerLocateVehicle_2Page {

  isRootPage = false;
  alertsCount; 

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  vehicle;
  customer;

  operation;

  constructor(private func: customFunctions, private api:ApiProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.api.getAllAlertsCount();
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');

    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');
    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)
    if(this.vehicle.customer_id && this.operation == "rental_manager_return_loaner"){
      this.func.presentLoading();
      this.getCustomerProfile(this.vehicle.customer_id)
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentalManagerLocateVehicle_2Page');
  }

  getCustomerProfile(customer_id){
    let data = {
      "CustomerId" : customer_id,
      "DealerId" : ""
    }
    this.api.getCustomerAccessTokenExternalAPI()
      .then((token)=> {
        this.api.getCustomerProfileFromExternalAPI(data, token)
          .then(data => {
            console.log(data);
            this.func.dismissLoading();
            this.customer = data;
          })
        })
      .catch(err => {
        console.log(err)
        this.func.dismissLoading();
        this.func.presentToast("An Error Occurred While Fetching The Data");
        this.navCtrl.pop();
      })

  }

  gotoRentalInformation(){
    if(this.operation == "rental_manager_return_loaner"){
      this.func.presentLoading();
      this.api.returnLoanerAssigned(this.vehicle.customer_id, this.vehicle.vin_number)
        .then(data => {
          console.log(data)
          if(data){
            this.func.dismissLoading();
            this.func.presentToast(data['message'], 3000, "bottom")
            this.navCtrl.push('RentalManagerSuccessPage', {operationType: this.operation, vehicle: this.vehicle, customer: this.customer}, this.animationsOptions);
          }else{
            throw('error occured');
          }
        })
        .catch( err => {
          this.func.dismissLoading()
          console.log(err)
        })
    }else{
      this.navCtrl.push('RentalManagerRentalInformationPage', {operationType: this.operation, vehicle: this.vehicle, customer: this.customer}, this.animationsOptions);
    }
  }

  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back() {
    if(this.navCtrl.canGoBack()){
      this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('HomePage')
    }
  }

}
