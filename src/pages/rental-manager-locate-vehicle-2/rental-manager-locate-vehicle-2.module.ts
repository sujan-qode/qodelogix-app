import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerLocateVehicle_2Page } from './rental-manager-locate-vehicle-2';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RentalManagerLocateVehicle_2Page,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(RentalManagerLocateVehicle_2Page),
  ],
})
export class RentalManagerLocateVehicle_2PageModule {}
