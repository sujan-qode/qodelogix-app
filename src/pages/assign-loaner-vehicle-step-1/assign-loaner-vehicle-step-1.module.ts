import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignLoanerVehicleStep_1Page } from './assign-loaner-vehicle-step-1';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AssignLoanerVehicleStep_1Page,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AssignLoanerVehicleStep_1Page),
  ],
})
export class AssignLoanerVehicleStep_1PageModule {}
