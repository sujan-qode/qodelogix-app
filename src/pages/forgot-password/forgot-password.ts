import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { customFunctions } from '../../providers/functions';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  email;
  isEmailSent = false;

  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform, public navCtrl: NavController, public navParams: NavParams, private auth: AuthProvider, private func: customFunctions) {
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  sendEmail(){
    this.func.presentLoading();
    this.auth.changePassword(this.email)
      .then(data => {
        console.log(data)
        if(data['status'] == "Error"){
          this.func.showAlert("Error", data['message'])
        }else{
          this.isEmailSent = true;
        }
        this.func.dismissLoading();
      })

  }

  goBack(){
    this.navCtrl.push('LoginPage');
  }

}
