// import { VehicleProfileNewPage } from './../vehicle-profile-new/vehicle-profile-new';
import { env } from './../../environment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { VehicleProfilePage } from '../vehicle-profile/vehicle-profile';
// import { HomePage } from '../home/home';
import { customFunctions } from '../../providers/functions';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
// import { VehicleProfileWithAnimationNewPage } from '../vehicle-profile-with-animation-new/vehicle-profile-with-animation-new';

/**
 * Generated class for the AlertsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alerts',
  templateUrl: 'alerts.html',
})
export class AlertsPage {

  alertsCount;
  vehicle_status = -1;
  dropdown_text = "All Alerts";
  filterData;
  alertsData;
  isSearchbarEmpty:boolean= true;
  searchItem:string = '';
  isRootPage = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }
  baseUrl = env.base;
  vinDetail = [];


  isTabletOrIpad:boolean;
  interface:string="popover";

  alltempData;

  fuelData;
  fueltempData;

  mileageData
  mileagetempData;
  
  batteryData;
  batterytempData;

  gpsData
  gpstempData;

  disconnectData
  disconnecttempData;

  firstCall = false;
  
  constructor(private func:customFunctions, 
              private platform:Platform, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private _api: ApiProvider,
              private nativePageTransitions: NativePageTransitions,
    ) {
    this._api.getAllAlertsCount();
    // this.getAllAlerts();
    console.log(this.navParams.get('vehicle_status'))
    if(parseInt(this.navParams.get('vehicle_status')) >= 0){
          this.vehicle_status = parseInt(this.navParams.get('vehicle_status'))
          this.getAllAlerts();
          this.filterVehicleOnLoad();
    }else{
      this.getAllAlerts();
    }

    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;

    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if(!this.isTabletOrIpad){
      this.interface='action-sheet';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehicleListPage');
  }

  ionViewDidLeave(){
  }

  async getAllAlerts() {
    await this._api.getAllAlerts()
    .then(data => {
      console.log(data)
      this.alertsData = data;
      // this.filterData = data;
      this.alltempData = data;
    })
    .then(()=>{
      this.getBatteryAlerts();
    })
    .then(()=>{
      this.getFuelAlerts();
    })
    .then(()=>{
      this.getMileageAlerts();
    })
    .then(()=>{
      this.getGpsAlerts();
    })
    .then(()=>{
      this.getDisconnectAlerts();
    })
    .then(()=>{
    })
  }

  getItems(ev: any) {
    let val = ev.target.value;
    this.searchItem = val;
    console.log(val)
    this.OnVehicleStatusChange();
  }

  filterVehicleOnLoad(){
    if(this.vehicle_status == 0){
      this.getAllBatteryAlerts();
      this.dropdown_text = "Battery Alerts"
    }
    else if(this.vehicle_status == 1){
      this.getAllFuelAlerts();
      this.dropdown_text = "Fuel Alerts"
    }
    else if(this.vehicle_status == 2){
      this.getAllMileageAlerts();
      this.dropdown_text = "Mileage Alerts"
    }
    else if(this.vehicle_status == 3){
      this.getAllGpsAlerts();
      this.dropdown_text = "GPS Alerts"
    }
    else{
    }
  }



  OnVehicleStatusChange(){
    console.log(this.vehicle_status)
    let val = this.searchItem;
    if(this.vehicle_status == -1){
      this.filterAllAlerts(val);
      this.dropdown_text = "All Alerts"
    }
    else if(this.vehicle_status == 0){
      this.filterBatteryAlerts(val);
      this.dropdown_text = "Battery Alerts"
    }
    else if(this.vehicle_status == 1){
      this.filterFuelAlerts(val);
      this.dropdown_text = "Fuel Alerts"
    }
    else if(this.vehicle_status == 2){
      this.filterMileageAlerts(val);
      this.dropdown_text = "Mileage Alerts"
    }
    else if(this.vehicle_status == 3){
      this.filterGPSAlerts(val);
      this.dropdown_text = "GPS Alerts"
    }
    else if(this.vehicle_status == 4){
      this.filterDisconnectAlerts(val);
      this.dropdown_text = "Disconnect Alerts"
    }
    else{
    }
  }

  filterAllAlerts(val){
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.alertsData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
    }
    else{
      this.filterData = this.alltempData;
      this.alertsData = this.alltempData;
      this.isSearchbarEmpty = true;
    }
  }

  filterBatteryAlerts(val){
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.batteryData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
    }
    else{
      this.filterData = this.batterytempData;
      this.batteryData = this.batterytempData;
      this.isSearchbarEmpty = true;
    }
  }

  filterFuelAlerts(val){
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.fuelData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
      console.log(this.filterData);
    }
    else{
      this.filterData = this.fueltempData;
      this.fuelData = this.fueltempData;
      this.isSearchbarEmpty = true;
    }
  }


  filterMileageAlerts(val){
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.mileageData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
    }
    else{
      this.filterData = this.mileagetempData;
      this.mileageData = this.mileagetempData;
      this.isSearchbarEmpty = true;
    }
  }


  filterGPSAlerts(val){
    console.log(val && val.trim() != '')
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.gpsData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
    }
    else{
      this.filterData = this.gpstempData;
      this.gpsData = this.gpstempData;
      this.isSearchbarEmpty = true;
    }
  }

  filterDisconnectAlerts(val){
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.disconnectData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
    }
    else{
      this.filterData = this.disconnecttempData;
      this.disconnectData = this.disconnecttempData;
      this.isSearchbarEmpty = true;
    }
  }


  // OnVehicleStatusChange(){
  //   if(this.vehicle_status == -1){
  //     this.getAllAlerts(2);
  //     this.dropdown_text = "All Alerts"
  //   }
  //   else if(this.vehicle_status == 0){
  //     this.getBatteryAlerts();
  //     this.dropdown_text = "Battery Alerts"
  //   }
  //   else if(this.vehicle_status == 1){
  //     this.getFuelAlerts();
  //     this.dropdown_text = "Fuel Alerts"
  //   }
  //   else if(this.vehicle_status == 2){
  //     this.getMileageAlerts();
  //     this.dropdown_text = "Mileage Alerts"
  //   }
  //   else if(this.vehicle_status == 3){
  //     this.getGpsAlerts();
  //     this.dropdown_text = "GPS Alerts"
  //   }
  //   else if(this.vehicle_status == 4){
  //     this.getDisconnectAlerts();
  //     this.dropdown_text = "Disconnect Alerts"
  //   }
  //   else{
  //   }
  // }

  // filterByVin(val) {
  //   if (val && val.trim() != '') {
  //     this.isSearchbarEmpty = false;
  //     this.filterData = this.alertsData.filter((vehicle) => {
  //       let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
  //       return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
  //     });
  //   }
  //   else{
  //     if(this.vehicle_status == -1){
  //       this.getAllAlerts(3);
  //     }
  //     this.isSearchbarEmpty = true;
  //   }
    
  // }

  // getAllAlerts(initialCall = 1) {
  //   if(initialCall == 1) this.func.presentLoading();
  //   this._api.getAllAlerts()
  //   .then(data => {
  //     console.log(data)
  //     this.alertsData = data;
  //     this.filterData = data;
  //   }).then(() => {
  //     if(initialCall == 1) this.func.dismissLoading();
  //     if(initialCall == 3) return;
  //     this.filterByVin(this.searchItem);
  //   });
  // }

  getAllBatteryAlerts(){
    this.func.presentLoading();
    this._api.getBatteryAlerts()
    .then(data => {
      console.log(data);
      this.alertsData = data;
      this.filterData = data;
      this.func.dismissLoading();
    })
    .catch(err=> {
      console.log(err);
      this.func.dismissLoading();
    })
  }

  getAllFuelAlerts(){
    this.func.presentLoading();
    this._api.getFuelAlerts()
    .then(data => {
      console.log(data);
      this.alertsData = data;
      this.filterData = data;
      this.func.dismissLoading();
    })
    .catch(err=> {
      console.log(err);
      this.func.dismissLoading();
    })
  }

  getAllMileageAlerts(){
    this.func.presentLoading();
    this._api.getMileageAlerts()
    .then(data => {
      console.log(data);
      this.alertsData = data;
      this.filterData = data;
      this.func.dismissLoading();
    })
    .catch(err=> {
      console.log(err);
      this.func.dismissLoading();
    })
  }

  getAllGpsAlerts(){
    this.func.presentLoading();
    this._api.getGpsAlerts()
    .then(data => {
      console.log(data)
      this.alertsData = data;
      this.filterData = data;
      this.func.dismissLoading();
    })
    .catch(err=> {
      console.log(err);
      this.func.dismissLoading();
    })
  }

  getBatteryAlerts(){
    this._api.getBatteryAlerts()
    .then(data => {
      // console.log(data);
      this.batteryData = data;
      this.batterytempData = data;
    }).then(() => {
    });
  }

  getFuelAlerts(){
    this._api.getFuelAlerts()
    .then(data => {
      // console.log(data);
      this.fuelData = data;
      this.fueltempData = data;
    }).then(() => {
    });
  }

  getMileageAlerts(){
    this._api.getMileageAlerts()
    .then(data => {
      // console.log(data);
      this.mileageData = data;
      this.mileagetempData = data;
    }).then(() => {
    });
  }

  getGpsAlerts(){
    this._api.getGpsAlerts()
    .then(data => {
      // console.log(data)
      this.gpsData = data;
      this.gpstempData = data;
    }).then(() => {
    });
  }

  getDisconnectAlerts(){
    this._api.getDisconnectAlerts()
    .then(data => {
      // console.log(data)
      this.disconnectData = data;
      this.disconnecttempData = data;
    })
    .then(()=>{
    })
  }

  gotoVehicleProfileWithAnimation(e, i, v){
    console.log(v)
    let id = "component"+i;
    let el = document.getElementById(id);
    let topContent = document.getElementById('page-content');
    let options: NativeTransitionOptions = {
      duration: 300
     };
    this.nativePageTransitions.fade(options);
    // this.navCtrl.push('VehicleProfileWithAnimationPage', { offsetTop:el.offsetTop, topContent:topContent.offsetTop, vehicle: v}, {animate: false});
    this.navCtrl.push('VehicleProfileWithAnimationNewPage', { offsetTop:el.offsetTop, topContent:topContent.offsetTop, vehicle: v}, {animate: false});
  }


  gotoVehicleProfile(vehicle){
    this.func.presentLoading();
    // this.navCtrl.push('VehicleProfilePage', {vehicle: vehicle}, this.animationsOptions);
    this.navCtrl.push('VehicleProfileNewPage', {vehicle: vehicle}, this.animationsOptions);
  }


  back(){
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }
  
}
