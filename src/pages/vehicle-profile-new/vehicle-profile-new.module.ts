import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleProfileNewPage } from './vehicle-profile-new';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VehicleProfileNewPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(VehicleProfileNewPage),
  ],
})
export class VehicleProfileNewPageModule {}
