import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DamageReportingAddPopoverPage } from './damage-reporting-add-popover';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DamageReportingAddPopoverPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(DamageReportingAddPopoverPage),
  ],
})
export class DamageReportingAddPopoverPageModule {}
