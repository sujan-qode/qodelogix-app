import { customFunctions } from './../../providers/functions';
import { Component } from '@angular/core';
import { IonicPage, NavController, Platform, NavParams, ViewController, ActionSheetController, LoadingController, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { File, FileEntry } from "@ionic-native/file";
import { env } from '../../environment';
import { normalizeURL } from 'ionic-angular';
/**
 * Generated class for the DamageReportingAddPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// declare function unescape(s:string): string;
// declare var cordova;
// declare function escape(s:string): string;

@IonicPage()
@Component({
  selector: 'page-damage-reporting-add-popover',
  templateUrl: 'damage-reporting-add-popover.html',
})

export class DamageReportingAddPopoverPage {

  
  temp:any;

  damage_location_get:any= {
    id: '',
    vin_number: '',
    damage_location_id: -1,
    damage_type_id: -1,
    comment: '',
    image_path: '',
    isSaved: 0,
  };

  vehicle;
  checkin_type;
  imageDataUri:any= '';
  imageName:any = '';
  myPhoto;
  damage_locations;
  damage_types;

  damage_location = -1;
  damage_location_error = false;
  damage_location_dropdown_text = "Damage Location";
  damage_type = -1;
  damage_type_error = false;
  damage_type_dropdown_text = "Damage Type";

  
  loading = this.loadingCtrl.create({
    content: 'Saving...'
  });


  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewCtrl: ViewController,
              public api: ApiProvider,         
              public actionSheetCtrl : ActionSheetController,
              private camera: Camera,
              private readonly file: File,
              private loadingCtrl: LoadingController,
              private func: customFunctions,
              private platform:Platform,
              public alertCtrl: AlertController,
            ) {
    this.getDamageLocations();
    this.getDamageTypes();

    if(!parseInt(this.navParams.get('damage_location'))){
      this.temp = this.navParams.get('damage_location');
      this.damage_location_get.id = this.temp.id;
      this.damage_location_get.damage_location_id = this.temp.damage_location_id;
      this.damage_location_get.damage_type_id = this.temp.damage_type_id;
      this.damage_location_get.comment = this.temp.comment == null ? '' : this.temp.comment ;
      // console.log(this.temp.image_path)
      this.damage_location_get.image_path = this.temp.image_path == null ? '' : env.base+this.temp.image_path;
      this.damage_location_get.vin_number = this.temp.vin_number;
      this.damage_location_get.isSaved = 1;
      this.damage_type = this.damage_location_get.damage_type_id;
      this.damage_location = this.damage_location_get.damage_location_id;
    }else{
      this.damage_location_get.damage_location_id = parseInt(this.navParams.get('damage_location'));
      this.damage_location = this.damage_location_get.damage_location_id;
    }
    this.vehicle = this.navParams.get('vehicle');
    this.checkin_type = parseInt(this.navParams.get('checkin_type'));
    console.log("checkin type -> "+this.checkin_type)

  }

  getDamageLocations(){
    this.api.getDamageLocations()
      .then(data => {
        this.damage_locations = data;
        this.damage_location_dropdown_text = this.getDamageLocationName(this.damage_location_get.damage_location_id);
      });
  }


  getDamageTypes(){
    this.api.getDamageTypes()
      .then(data => {
        this.damage_types = data;
        this.damage_type_dropdown_text = this.getDamageTypeName(this.damage_location_get.damage_type_id);
      });
  }


  OnDamageLocationStatusChange(e){
    this.damage_location_error = false;
    if(this.damage_location >= 0){
      this.damage_location_dropdown_text = this.getDamageLocationName(parseInt(e));
    }
    else{
      this.damage_location_dropdown_text = "Damage Location"
    }
  }

  

  OnDamageTypeStatusChange(e){
    this.damage_type_error = false;
    if(this.damage_type >= 0){
      this.damage_type_dropdown_text = this.getDamageTypeName(parseInt(e));
    }
    else{
      this.damage_type_dropdown_text = "Damage Type"
    }
  }


  getDamageLocationName(id) {
    let name='';
    this.damage_locations.some(function(el) {
       if (el.id === id){
        name =  el.damage_location;
       }
    }); 
    return name;
  }


  getDamageTypeName(id) {
    let name='';
    this.damage_types.some(function(el) {
       if (el.id === id){
        name =  el.damage_type;
       }
    }); 
    if(name == ''){
      return this.damage_type_dropdown_text;
    }else{
      return name;
    }
  }


  dismissPopover(){
    this.viewCtrl.dismiss();
  }


  save(){
     this.loading.present();
      const vehicleInfo = new FormData();
      vehicleInfo.append('checkin_type', this.checkin_type);
      vehicleInfo.append('asset_id', this.vehicle.asset_id);
      vehicleInfo.append('vin_number', this.vehicle.vin_number);
      vehicleInfo.append('damage_location_id', this.damage_location.toString());
      vehicleInfo.append('damage_type_id', this.damage_type.toString());
      vehicleInfo.append('comment', this.damage_location_get.comment);
      // vehicleInfo.append('image', this.imageDataUri, this.imageName);
      vehicleInfo.append('dealer_id', localStorage.getItem('dealer_id'));
      vehicleInfo.append('user_id', localStorage.getItem('auth_id'));
      if(this.imageDataUri != ''){
        vehicleInfo.append('image', this.imageDataUri, this.imageName);
     }

    console.log(vehicleInfo)
    if(this.validateData()){
      this.api.insertVehicleDamageInfo(vehicleInfo)
      .then(data => {
        this.loading.dismiss();
        // this.showAlert('Message',data);
        this.navCtrl.pop();
        this.func.presentToast(data, 3000, 'bottom')
      })
      .catch(err => {
        console.log(err)
        this.loading.dismiss();
        this.func.presentToast('Error Occured. Please Try again', 3000, 'bottom')
      })
    }else{
      this.func.presentToast('Invalid data. Try again', 3000, 'bottom')
      this.loading.dismiss();
    }
    // this.navCtrl.pop();
  }


  update(){
      this.loading.present();
      const vehicleInfo = new FormData();
      vehicleInfo.append('id', this.damage_location_get.id);
      vehicleInfo.append('asset_id', this.vehicle.asset_id);
      vehicleInfo.append('vin_number', this.vehicle.vin_number);
      vehicleInfo.append('damage_location_id', this.damage_location.toString());
      vehicleInfo.append('damage_type_id', this.damage_type.toString());
      vehicleInfo.append('comment', this.damage_location_get.comment);
      vehicleInfo.append('checkin_type', this.checkin_type);
      if(this.imageDataUri != ''){
        vehicleInfo.append('image', this.imageDataUri, this.imageName);
     }
     vehicleInfo.append('dealer_id', localStorage.getItem('dealer_id'));
     vehicleInfo.append('user_id', localStorage.getItem('auth_id'));
      
    if(this.validateData()){
      this.api.updateVehicleDamageInfo(vehicleInfo)
      .then(data => {
        this.loading.dismiss();
          // this.showAlert('Message',data);
          this.navCtrl.pop();
          this.func.presentToast(data, 3000, 'bottom')
      })
      .catch(err => {
        console.log(err);
        this.loading.dismiss();
      });
    }else{
      this.func.presentToast('Invalid data. Try again', 3000, 'bottom')
      this.loading.dismiss();
    }
    // this.navCtrl.pop();
  }


  delete(){
    const confirm = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'Do you want to delete this damage report?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.confirmDelete();
          }
        }
      ]
    });
    confirm.present();
  }

  confirmDelete(){
    const vehicleInfo = new FormData();
    vehicleInfo.append('id', this.damage_location_get.id);
    vehicleInfo.append('checkin_type', this.checkin_type);
    vehicleInfo.append('dealer_id', localStorage.getItem('dealer_id'));
    vehicleInfo.append('user_id', localStorage.getItem('auth_id'));
    if(this.validateData()){
      this.api.deleteVehicleDamageInfo(vehicleInfo)
      .then(data => {
        this.func.presentToast(data, 3000, 'bottom')
        this.navCtrl.pop();
      })
    }else{
      this.func.presentToast('An error occured. Try again', 3000, 'bottom')
    }
  }


  validateData(){
    this.damage_location_error = false;
    this.damage_type_error = false;
    if(this.damage_type < 0){
      this.damage_type_error = true;
      return false;
    }
    if(this.damage_location < 0){
      this.damage_location_error = true;
      return false;
    }
    return true;
  }


  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Image Source',
      buttons: [
        {
          text: 'Camera',
          handler: () => {
            this.takePhoto();
          }
        }, {
          text: 'Local',
          handler: () => {
            this.selectPhoto();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }

  takePhoto() {

    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.PNG,
      targetWidth: 512,
      targetHeight: 512,
      saveToPhotoAlbum: true,
      correctOrientation: true,
    }
    this.camera.getPicture(options)
      .then(imageData => {
        this.damage_location_get.image_path = normalizeURL(imageData);
        this.uploadPhoto(imageData);
      }, error => {
        this.func.presentToast(JSON.stringify(error), 3000, 'bottom')
      });
    }

  selectPhoto(): void {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      quality: 70,
      allowEdit:true,
      targetHeight: 300,
      targetWidth: 300,
      encodingType: this.camera.EncodingType.PNG,
      correctOrientation: true
    }).then(imageData => {
      // this.myPhoto = "data:image/jpeg;base64," +imageData;
      this.damage_location_get.image_path = normalizeURL(imageData);
      this.uploadPhoto(imageData);
    }, error => {
      this.func.presentToast(JSON.stringify(error), 3000, 'bottom')
    });
  }

  uploadPhoto(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }

  private readFile(file: any) {
    const reader = new FileReader();

    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        this.imageDataUri = imgBlob;
        this.imageName = this.func.getRandomString()+file.name;
      };
    }else{
      reader.onload = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        this.imageDataUri = imgBlob;
        this.imageName = this.func.getRandomString()+file.name;
      };
    }
    
    reader.readAsArrayBuffer(file);
  }

}
