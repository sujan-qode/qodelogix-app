// import { ServiceVehiclePage } from './../service-vehicle/service-vehicle';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
import { ApiProvider } from '../../providers/api/api';
// import { DamageReportingPage } from '../damage-reporting/damage-reporting';
// import { AccessoriesChecklistPage } from '../accessories-checklist/accessories-checklist';
// import { CheckInPage } from '../check-in/check-in';
import { customFunctions } from '../../providers/functions';
import { env } from '../../environment';
// import { DeliveryVehiclePage } from '../delivery-vehicle/delivery-vehicle';

/**
 * Generated class for the ReviewAndCheckInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review-and-check-in',
  templateUrl: 'review-and-check-in.html',
})
export class ReviewAndCheckInPage {
  
  vinDetail = null;
  baseUrl = env.base;

  alertsCount;
  vehicle;
  damageReport = {
    check_engine: '0',
    check_engine_note : '   ',
    tire_pressure : '0',
    tire_pressure_note : '',
    airbag : '0',
    airbag_note : '',
    dsc : '0',
    dsc_note : '',
    left_front : '0',
    left_front_tire_thread : '',
    left_front_tire_pressure : '',
    right_front : '0',
    right_front_tire_thread : '',
    right_front_tire_pressure : '',
    left_rear : '0',
    left_rear_tire_thread : '',
    left_rear_tire_pressure : '',
    right_rear : '0',
    right_rear_tire_thread : '',
    right_rear_tire_pressure : '',
    ext_body_note : '',
    windshield_note : '',
    wiper_note : '',
    light_note : '',
    asset_id : '',
    vin_number : ''
  };
  accessories = {
    books: false,
    books_note: '',
    pouch: false,
    pouch_note: null,
    keys: false,
    keys_note: '',
    tablet: false,
    tablet_note: '',
    headphones: false,
    headphones_note: '',
    mats: false,
    mats_note: '',
  };
  damageInfo;
  
  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }
  checkin_type;
  vin;
  asset_id;

  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              public api: ApiProvider,
              public func: customFunctions  
            ) 
              {
    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    // this.accessories = this.navParams.get('accessories');
    // this.damageReport = this.navParams.get('damageReport');
    this.vehicle = this.navParams.get('vehicle');
    this.vin = this.vehicle.vin_number;
    this.asset_id = this.vehicle.asset_id;

    this.checkin_type = this.navParams.get('checkin_type');
    console.log(this.checkin_type)

    this.getDamageInfo(this.vehicle.vin_number, this.checkin_type);

    this.getVehicleDamage(this.vehicle.vin_number, this.checkin_type);

    this.getAccessoriesChecklist(this.vehicle.vin_number, this.checkin_type);
  }


  getAccessoriesChecklist(vin_number,checkin_type){
    this.api.getAccessoriesChecklist(vin_number, checkin_type)
      .then( data => {
        console.log(data)
          this.accessories = data[0];
          console.log(this.accessories)
      })
  }

  getVehicleDamage(vin_number,checkin_type){
    this.api.getVehicleDamage(vin_number, checkin_type)
      .then( data => {
        console.log(data)
          this.damageReport = data[0];
      })
  }
  
  getDamageInfo(vin_number,checkin_type){
    this.api.getDamageInfo(vin_number, checkin_type)
        .then( data => {
          console.log(data)
          this.damageInfo = data;
        })
  }  

  ionViewDidLoad() {
    console.log('ionViewDidLoad DamageReportingPage');
  }

  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }
  
  goback(){
    this.navCtrl.push('AccessoriesChecklistPage', {
      vehicle: this.vehicle,
      checkin_type: this.checkin_type
    },
    this.animationsOptions)
  }

  gotoDamagereportingpage(){
    this.navCtrl.push('DamageReportingPage', {
      vehicle: this.vehicle,
      checkin_type: this.checkin_type
    },
    this.animationsOptions)
  }

  gotoCheckin(){
    console.log('check_in_type: '+this.checkin_type);
    /*
      Save Delivery Vehicles
    */
    if(this.checkin_type == 3){
      this.navCtrl.push('DeliveryVehiclePage', {
        vehicle: this.vehicle
      },
      this.animationsOptions)
      // this.func.presentLoading("Deliverying vehicle");
      // this.api.decodeVin(this.vin)
      //   .then(data =>{
      //     let checkinData = this.vin+"&year="+data['year']+"&make="+data['make']+"&model_trim="+data['model_trim']+'&model_image='+data['model_image'];
      //     this.api.checkIn(checkinData, this.checkin_type)
      //       .then(data => {
      //         this.func.dismissLoading();
      //         this.func.presentToast(data)
      //         this.navCtrl.push('DeliveryVehiclePage', {
      //           vehicle: this.vehicle
      //         },
      //         this.animationsOptions)
      //       })
      //   })
    }
    /*
      Save Services Vehicles
    */
    if(this.checkin_type == 4){
      this.navCtrl.push('ServiceVehiclePage', {
        vehicle: this.vehicle
      },
      this.animationsOptions)
      // this.func.presentLoading("Please Wait");
      // this.api.decodeVin(this.vin)
      //   .then(data =>{
      //     let checkinData = this.vin+"&year="+data['year']+"&make="+data['make']+"&model_trim="+data['model_trim']+'&model_image='+data['model_image'];
      //     this.api.checkIn(checkinData, this.checkin_type)
      //       .then(data => {
      //         this.func.dismissLoading();
      //         this.func.presentToast(data)
      //         this.navCtrl.push('ServiceVehiclePage', {
      //           vehicle: this.vehicle
      //         },
      //         this.animationsOptions)
      //       })
      //   })
    }
    if(parseInt(this.checkin_type) < 3){
      this.func.presentLoading("Checking In");
      this.api.checkIn(this.vehicle.vin_number, this.checkin_type)
      .then(data => {
        this.func.dismissLoading();
        this.func.presentToast(data)
        this.navCtrl.push('CheckInPage', {
          vehicle: this.vehicle
        },
        this.animationsOptions)
      }).catch(err => {
        this.func.dismissLoading();
        this.func.presentToast('Error cooured')
        this.navCtrl.push('CheckInPage', {
          vehicle: this.vehicle
        },
        this.animationsOptions)
      })
    }
  }

  back(){
    this.navCtrl.pop()
  }
  
}

