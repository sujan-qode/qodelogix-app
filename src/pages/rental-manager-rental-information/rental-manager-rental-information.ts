import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { customFunctions } from '../../providers/functions';
// import { RentalManagerRentalInformation_2Page } from '../rental-manager-rental-information-2/rental-manager-rental-information-2';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { env } from '../../environment';

/**
 * Generated class for the RentalManagerRentalInformationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-rental-information',
  templateUrl: 'rental-manager-rental-information.html',
})
export class RentalManagerRentalInformationPage {

  isRootPage = false;
  alertsCount; 

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  vehicle;
  customer;


  isTabletOrIpad: boolean;
  interface: string = "popover";

  addDriver = false;
  rentalTypes;

  rentalType = "-1";
  rentalFee = "0";

  note='';
  serviceAdvisor;
  pickupDate;
  dropoffDate;
  roNumber;

  today;

  cameraOptions: CameraOptions;

driver = {
  FirstName : '',
  LastName : '',
  DLFront : '',
  DLBack : '',
  ICFront : '',
  ICBack : '',
  DLFrontName : '',
  DLBackName : '',
  ICFrontName : '',
  ICBackName : '',
}

operation;

  constructor(private platform : Platform,
               private api:ApiProvider, 
               public navCtrl: NavController, 
               public func: customFunctions,
               private camera: Camera,
               private transfer: FileTransfer,
               public navParams: NavParams) {
    this.api.getAllAlertsCount();
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if (!this.isTabletOrIpad) {
      this.interface = 'action-sheet';
    }

    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)

    this.today = new Date().toISOString();

    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');

    this.getRentalType();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad page-rental-manager-rental-information');
  }

  setRentalInfo(){
    let rentalInfo = JSON.parse(localStorage.getItem('rentalInfo'));
    console.log(rentalInfo)
    this.rentalType = rentalInfo.vehicle_type_id ? rentalInfo.vehicle_type_id : '-1';
    if(this.rentalType != "-1"){
      this.onChangeRentalType();
    }
    this.note= rentalInfo.additional_notes;
    this.serviceAdvisor= rentalInfo.service_advisor;
    this.pickupDate= rentalInfo.pickup_date;
    this.dropoffDate= rentalInfo.drop_off_date;
    this.roNumber= rentalInfo.ro_number;

    this.addDriver = rentalInfo.additional_driver == "true" ? true : false;

    if(this.addDriver){
      this.driver.FirstName = rentalInfo.driver_first_name;
      this.driver.LastName = rentalInfo.driver_last_name;
      if(rentalInfo.driver_license_front){
        this.driver.DLFront = env.base + rentalInfo.driver_license_front;
      }
      if(rentalInfo.driver_license_back){
        this.driver.DLBack = env.base + rentalInfo.driver_license_back;
      }
      if(rentalInfo.insurance_card_front){
        this.driver.ICFront = env.base + rentalInfo.insurance_card_front;
      }
      if(rentalInfo.insurance_card_back){
        this.driver.ICBack = env.base + rentalInfo.insurance_card_back;
      }
    }
    
  }

  getRentalType(){
    this.func.presentLoading();
    this.api.getRentalType()
      .then(data => {
        console.log(data)
        this.rentalTypes = data;
      })
      .then(()=>{
        this.rentalTypes = this.rentalTypes.map((type)=>{
          let temp = {};
          temp['id'] = type['id'];
          temp['name'] = type['name'];

          if(type.daily_rate == null || type.daily_rate == "null" || !type.daily_rate){
            temp['daily_rate'] = '0';
          }else{
            temp['daily_rate'] = type['daily_rate'];
          }

          return temp;
        })
        console.log(this.rentalTypes)
        this.func.dismissLoading();
      })
      .then(()=>{
        if(this.operation == "rental_manager_edit_loaner"){
          this.setRentalInfo();
        }
      })
      .catch((err)=>{
        console.log(err);
        this.func.dismissLoading();
      });
  }

  onChangeRentalType(){
    let tempType = this.rentalTypes.filter((type) => {
        return (type.id == this.rentalType)
    });
    this.rentalFee = tempType[0].daily_rate;
    console.log(tempType)
  }

  gotoRentalInformation2(){
    // alert(JSON.stringify(this.driver.DLFrontName));
    let test = this.checkFields();
    if(test){
      let rentalInfo = {
        rentalType: this.rentalType,
        rentalFee: this.rentalFee,
        serviceAdvisor: this.serviceAdvisor,
        roNumber: this.roNumber,
        pickupDate: this.pickupDate,
        dropoffDate: this.dropoffDate,
        note: this.note,
      }

      let driver = {
        addDriver: this.addDriver,
        FirstName: this.driver.FirstName,
        LastName: this.driver.LastName,
        DLFrontName: this.driver.DLFrontName,
        DLBackName: this.driver.DLBackName,
        ICFrontName: this.driver.ICFrontName,
        ICBackName: this.driver.ICBackName
      }
    
      this.navCtrl.push('RentalManagerRentalInformation_2Page', {operationType: this.operation, rentalInfo: rentalInfo, vehicle: this.vehicle, customer: this.customer, driver: driver}, this.animationsOptions);
      // this.navCtrl.push('RentalManagerRentalInformation_2Page')
    }
    else{
      // this.func.showAlert("Notice", "Please fill all the fields correctly.");
    }
  }

  checkFields(){
    if(this.rentalType == "-1"
        || this.isEmpty(this.serviceAdvisor)
        || this.isEmpty(this.roNumber)
        || this.isEmpty(this.pickupDate)
        || this.isEmpty(this.dropoffDate))
    {
      this.func.showAlert("Notice", "Please fill all the fields correctly.");
      return false;
    }else{
      if(this.addDriver){
        if(this.isEmpty(this.driver.FirstName) 
            || this.isEmpty(this.driver.LastName))
        {
          this.func.showAlert("Notice", "First and Last name of the driver is compulsory.");
          return false;
        }else{
          return true;
        }
      }else{
        return true;
      }
    }
  }

  isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

capture_dl_front(){
  const options:CameraOptions={
    quality:50,
    destinationType:this.camera.DestinationType.DATA_URL,
    encodingType:this.camera.EncodingType.JPEG,
    mediaType:this.camera.MediaType.PICTURE,
    targetWidth: 512,
    targetHeight: 512,
    saveToPhotoAlbum: false,
    correctOrientation: true,
  }
  this.camera.getPicture(options).then((async ImageData=>{
    this.driver.DLFront="data:image/jpeg;base64,"+ImageData;
    this.uploadImagAndGetImageName(this.driver.DLFront, 'DLFront');
  }),error=>{
    this.func.presentToast(JSON.stringify(error));
  })
}

capture_dl_back(){
  const options:CameraOptions={
    quality:50,
    destinationType:this.camera.DestinationType.DATA_URL,
    encodingType:this.camera.EncodingType.JPEG,
    mediaType:this.camera.MediaType.PICTURE,
    targetWidth: 512,
    targetHeight: 512,
    saveToPhotoAlbum: false,
    correctOrientation: true,
  }
  this.camera.getPicture(options).then((async ImageData=>{
    this.driver.DLBack="data:image/jpeg;base64,"+ImageData;
    this.uploadImagAndGetImageName(this.driver.DLFront, 'DLBack');
  }),error=>{
    this.func.presentToast(JSON.stringify(error));
  })
}

capture_ic_front(){
  const options:CameraOptions={
    quality:50,
    destinationType:this.camera.DestinationType.DATA_URL,
    encodingType:this.camera.EncodingType.JPEG,
    mediaType:this.camera.MediaType.PICTURE,
    targetWidth: 512,
    targetHeight: 512,
    saveToPhotoAlbum: false,
    correctOrientation: true,
  }
  this.camera.getPicture(options).then((async ImageData=>{
    this.driver.ICFront="data:image/jpeg;base64,"+ImageData;
    this.uploadImagAndGetImageName(this.driver.DLFront, 'ICFront');
  }),error=>{
    this.func.presentToast(JSON.stringify(error));
  })
}

capture_ic_back(){
  const options:CameraOptions={
    quality:50,
    destinationType:this.camera.DestinationType.DATA_URL,
    encodingType:this.camera.EncodingType.JPEG,
    mediaType:this.camera.MediaType.PICTURE,
    targetWidth: 512,
    targetHeight: 512,
    saveToPhotoAlbum: false,
    correctOrientation: true,
  }
  this.camera.getPicture(options).then((async ImageData=>{
    this.driver.ICBack="data:image/jpeg;base64,"+ImageData;
    this.uploadImagAndGetImageName(this.driver.DLFront, 'ICBack');
  }),error=>{
    this.func.presentToast(JSON.stringify(error));
  })
}

async uploadImagAndGetImageName(base64Image, type = ''){
  this.func.presentLoading('Saving Image...');
  let imageName = '';

  const fileTransfer: FileTransferObject = this.transfer.create();

  let options: FileUploadOptions = {
    fileKey: "image",
    fileName: "driverpapers.jpg",
    chunkedMode: false,
    mimeType: "image/jpeg",
    headers: {'Authorization': "Bearer "+ localStorage.getItem('auth_token')}
  }

  fileTransfer.upload(base64Image, env.url+'uploadCardImages', options).then(data => {
    let resp = JSON.parse(data['response']);
    if(resp['status'] == 'Success'){
      imageName = resp['message'];
      // alert(imageName)
      if(type == "DLFront"){ this.driver.DLFrontName = imageName; }
      if(type == "DLBack"){ this.driver.DLBackName = imageName; }
      if(type == "ICFront"){ this.driver.ICFrontName = imageName; }
      if(type == "ICBack"){ this.driver.ICBackName = imageName; }
    }
    this.func.dismissLoading();
  }, error => {
    console.log(error);
    alert(JSON.stringify(error));
    this.func.showAlert('Error', 'Image Could not be uploaded.');
    this.func.dismissLoading();
  });
  return imageName;
}

  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back() {
    if(this.navCtrl.canGoBack()){
      this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('HomePage')
    }
  }

}

