import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerRentalInformationPage } from './rental-manager-rental-information';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    RentalManagerRentalInformationPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(RentalManagerRentalInformationPage),
  ],
})
export class RentalManagerRentalInformationPageModule {}
