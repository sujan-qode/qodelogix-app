import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the RentalManagerSelectCustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-select-customer',
  templateUrl: 'rental-manager-select-customer.html',
})
export class RentalManagerSelectCustomerPage {

  isRootPage = false;
  alertsCount; 
  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  operation;

  constructor(public navCtrl: NavController, public navParams: NavParams, private api:ApiProvider) {
    this.api.getAllAlertsCount();
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentalManagerSelectCustomerPage');
  }

  back() {
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

}
