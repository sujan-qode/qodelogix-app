import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerSelectCustomerPage } from './rental-manager-select-customer';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RentalManagerSelectCustomerPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(RentalManagerSelectCustomerPage),
  ],
})
export class RentalManagerSelectCustomerPageModule {}
