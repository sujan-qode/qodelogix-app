import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerLocateVehiclePage } from './rental-manager-locate-vehicle';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RentalManagerLocateVehiclePage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(RentalManagerLocateVehiclePage),
  ],
})
export class RentalManagerLocateVehiclePageModule {}
