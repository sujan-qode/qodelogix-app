import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { ApiProvider } from '../../providers/api/api';
import { Geolocation } from '@ionic-native/geolocation';
// import { RentalManagerLocateVehicle_2Page } from '../rental-manager-locate-vehicle-2/rental-manager-locate-vehicle-2';

/**
 * Generated class for the RentalManagerLocateVehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@IonicPage()
@Component({
  selector: 'page-rental-manager-locate-vehicle',
  templateUrl: 'rental-manager-locate-vehicle.html',
})
export class RentalManagerLocateVehiclePage {

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }
  vehicle;
  customer;

  isRootPage = false; 
  isTabletOrIpad: boolean;
  interface: string = "popover";

  alertsCount;
  allVehicles;

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  displaySurroundingVehicles = true;
  surroundingVehicleMarkers = [];
  myLatitude;
  myLongitude;

  operation;
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private platform: Platform,
    private geolocation: Geolocation
  ) {

    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)

    this.getAllVehicles();
    this.getMyLocation();

    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if (!this.isTabletOrIpad) {
      this.interface = 'action-sheet';
    }
    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');

    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    
  }

  ionViewDidLoad() {
    setTimeout(()=>{
      this.loadMap();
    }, 500)
  }

  getAllVehicles() {
    this.api.getAllVehicles()
      .then(data => {
        console.log(data)
        this.allVehicles = data;
      })
      .then(() => {
        this.toggleSurroundingVehicles()
      })
  }

  getMyLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords.latitude, resp.coords.longitude)
      this.addMyLocationToMap(resp)
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  addMyLocationToMap(resp) {
    this.myLatitude = resp.coords.latitude
    this.myLongitude = resp.coords.longitude
    this.addMarker(this.vehicle)
  }

  loadMap() {
    let latLng = new google.maps.LatLng(this.vehicle.latitude, this.vehicle.longitude);
    let mapOptions = {
      center: latLng,
      zoom: 20,
      tilt: 0,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      disableDefaultUI: true
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.vehicle);
  }

  addMarker(vehicle) {
    let image = { url: 'assets/update/vehicleMarker.png' };
    let me = { url: 'assets/update/myMarker.png' };

    let vehicleMarker = [];

    let marker = new google.maps.Marker({
      map: this.map,
      // animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(vehicle.latitude, vehicle.longitude),
      icon: image
    });
    let content = "<p style='font-size:15px'>VIN: <br><strong>" + vehicle.vin_number + "<strong></p>";
    this.addInfoWindow(marker, content);
    vehicleMarker.push(marker);

    if (this.myLatitude) {
      let myPositionMarker = new google.maps.Marker({
        map: this.map,
        // animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(this.myLatitude, this.myLongitude),
        // position: new google.maps.LatLng(43.75806,-79.79277),
        icon: me
      });
      let myContent = "<p style='font-size:15px'><br><strong> I'm here. <strong></p>";
      this.addInfoWindow(myPositionMarker, myContent);
      vehicleMarker.push(myPositionMarker);
      console.log('im here')
    }

    // force map to show all the markers in its viewport
    var bounds = new google.maps.LatLngBounds();
    if (vehicleMarker.length > 0) {
      for (var j = 0; j < vehicleMarker.length; j++) {
        bounds.extend(vehicleMarker[j].getPosition());
      }
      this.map.fitBounds(bounds);
      this.map.setCenter(bounds.getCenter());
    }
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }


  toggleSurroundingVehicles() {
    // console.log(this.displaySurroundingVehicles)
    if (this.displaySurroundingVehicles) {
      //set markers to the array
      this.setSorroundingvehicleMarkers();
      // add markers to the map
      this.setMapOnAll(this.map);
    } else {
      //remove all sourrounding markers
      this.setMapOnAll(null);
    }
  }

  setSorroundingvehicleMarkers() {
    this.surroundingVehicleMarkers = [];
    let image = { url: 'assets/icon/car-icon-grey.png' };
    for (var i = 0; i < this.allVehicles.length; i++) {
      let v = this.allVehicles[i];
      let distanceInKm = this.calculateDistance(this.vehicle.latitude, v.latitude, this.vehicle.longitude, v.longitude);
      if (distanceInKm == 0) { }
      else {
        // display surrounding vehicles if they are in the radius of 10 meters from the current one
        if (distanceInKm <= 0.01) {
          // console.log('in range: '+distanceInKm);
          let marker1 = new google.maps.Marker({
            map: this.map,
            // animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(v.latitude, v.longitude),
            icon: image
          });
          this.surroundingVehicleMarkers.push(marker1)
          let content1 = "<p style='font-size:15px'>VIN: <br><strong>" + v.vin_number + "<strong></p>";
          this.addInfoWindow(marker1, content1);
        }
      }
    }
  }

  // Sets the map on all markers in the array.
  setMapOnAll(map) {
    for (var i = 0; i < this.surroundingVehicleMarkers.length; i++) {
      this.surroundingVehicleMarkers[i].setMap(map);
    }
  }

  calculateDistance(lat1: number, lat2: number, long1: number, long2: number) {
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    return dis; // in km
  }

  gotoLocateVehicle2(){
    this.navCtrl.push('RentalManagerLocateVehicle_2Page', {operationType: this.operation, vehicle: this.vehicle, customer: this.customer}, this.animationsOptions)
  }



  gotoAlertsPage() {
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }

  back() {
    if(this.navCtrl.canGoBack()){
      this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('HomePage')
    }
  }

}
