import { env } from './../../environment';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Slides } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
// import { customFunctions } from '../../providers/functions';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the VehicleProfileWithAnimationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;
declare var navigator: any;

@IonicPage()
@Component({
  selector: 'page-vehicle-profile-with-animation-new',
  templateUrl: 'vehicle-profile-with-animation-new.html',
})
export class VehicleProfileWithAnimationNewPage {


  @ViewChild('map') mapElement: ElementRef;
  map: any;

  @ViewChild('mySlider') slider: Slides;
  toggleTabs = "features";
  slides = [
    {
      id: "features"
    },
    {
      id: "review-section"
    }
  ];

  baseUrl = env.base;
  alertsCount;

  isRootPage = false;

  vehicle;

  allVehicles;
  currentSlideIndex = 0;
  totalSlides;
  damageReport = false;
  damageInfo;
  accessories = false;
  displaySurroundingVehicles = true;
  surroundingVehicleMarkers = [];
  vinDetail = null;
  vinDetailTemp = null;

  vehicleType;

  displayMap = true;
  moreDetail = false;
  displayBottomIcon = false;


  isTabletOrIpad: boolean;
  interface: string = "popover";

  shoAdvanceMenu = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  moreDetailHeight = 0;

  myLatitude;
  myLongitude;
  watch;

  distance_unit;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private platform: Platform,
    // private func: customFunctions,
    private geolocation: Geolocation
  ) {

    this.distance_unit =  localStorage.getItem('distance_unit') ? localStorage.getItem('distance_unit') : 'km';
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if (!this.isTabletOrIpad) {
      this.interface = 'action-sheet';
    }
    this.vehicle = this.navParams.get('vehicle');
    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;

    this.api.getAllAlertsCount();
    this.vehicle = this.navParams.get('vehicle');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.getDamageInfo(this.vehicle.vin_number, this.vehicle.check_in_type);
    this.getVehicleDamage(this.vehicle.vin_number, this.vehicle.check_in_type);
    this.getAccessoriesChecklist(this.vehicle.vin_number, this.vehicle.check_in_type);
    console.log(this.vehicle);
    this.getAllVehicles();
    this.getVehicleTypes();
    this.getAlertsCount();
    // this.getMyLocation();

    setTimeout(() => {
      let el = document.getElementById('more-detail');
      console.log(el.offsetHeight)
      // this.moreDetailHeight = el.offsetHeight;
      this.moreDetailHeight = document.documentElement.clientHeight
      el.style.height = "0px"
      // el.style.display = "none"
    }, 300)
    
  }

  getAlertsCount(){
    this.api.getAllAlertsCount()
    .then(data=>{
      if(data){
        console.log(localStorage.getItem('allAlertsCount'))
        let AllAlertsCount = localStorage.getItem('allAlertsCount');
        (AllAlertsCount) ? this.alertsCount = parseInt(AllAlertsCount) : this.alertsCount = 0;
      }else{
        // this.navCtrl.setRoot('LoginPage')
        // localStorage.clear();
      }
    });
  }


  ionViewDidLoad() {
    setTimeout(()=>{
      this.loadMap();
    }, 1200)

    var top_component_content = document.getElementById('top_component_content');
    top_component_content.style.marginBottom = '100vh'

    // console.log('ionViewDidLoad VehicleProfileWithAnimationPage');
    let domElement = document.getElementById('component');
    if(this.navParams.get('offsetTop')){
      domElement.style.position = "absolute";
      domElement.style.top = this.navParams.get('offsetTop').toString() + 'px';
    }
    domElement.style.left = '0px'
    //Animate vehicle detail 
    setTimeout(() => {
      domElement.style.top = this.navParams.get('topContent').toString() + 'px';
      domElement.style.transition = "top 1s"
      top_component_content.style.marginBottom = '0px'
      top_component_content.style.transition = "all 1200ms"
    }, 200)
  }

  ionViewDidLeave() {
    // Clear watch
    navigator.geolocation.clearWatch(this.watch);
  }

  ionViewWillLeave(){
    // this.back();
  }
  ionViewDidEnter() {
    this.slider.lockSwipes(true);
  }

  ngAfterViewInit() {
    this.slider.autoHeight = true;
  }

  onSegmentChanged(segmentButton) {

    this.slider.lockSwipes(false);
    console.log("Segment changed to", segmentButton.value);
    const selectedIndex = this.slides.findIndex((slide) => {
      return slide.id === segmentButton.value;
    });
    this.slider.slideTo(selectedIndex);
    if (segmentButton.value == "review-section") {
      this.setSLiderContentHeight();
    }else{
      this.setSLiderContentHeightToOriginal();
    }

    this.slider.lockSwipes(true);
  }

  setSLiderContentHeight() {
    let featuresHeight = document.getElementById('features').offsetHeight;
    let reviewHeight = document.getElementById('review-section').offsetHeight;

    var style = document.createElement('style');
    if (featuresHeight < reviewHeight) {
      style.innerHTML =
      '.swiper-wrapper {' +
        'height:'+reviewHeight+'px !important;' +
      '}';
    }
    else {
      style.innerHTML =
      '.swiper-wrapper {' +
        'height:'+featuresHeight+'px !important;' +
      '}';
    }
    // Get the first script tag
    var ref = document.querySelector('script');
    // Insert our new styles before the first script tag
    ref.parentNode.insertBefore(style, ref);
  }

  setSLiderContentHeightToOriginal(){
    let featuresHeight = document.getElementById('features').offsetHeight;
    var style = document.createElement('style');
      style.innerHTML =
      '.swiper-wrapper {' +
        'height:'+featuresHeight+'px !important;' +
      '}';
    var ref = document.querySelector('script');
    ref.parentNode.insertBefore(style, ref);
  }

  getMyLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords.latitude, resp.coords.longitude)
      this.addMyLocationToMap(resp)
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    // listen to my location change
    // this.watch = this.geolocation.watchPosition();
    // this.watch.subscribe((resp) => {
    // console.log(resp.coords.latitude, resp.coords.longitude)
    // this.addMyLocationToMap(resp)
    // });

    // Add watch;
    // setTimeout(()=>{
    //   this.watch = navigator.geolocation.watchPosition((resp) => {
    //     console.log(resp.coords.latitude, resp.coords.longitude)
    //     this.addMyLocationToMap(resp)
    //   } , (error) => {
    //       console.log(error)
    //   });
    // }, 2100)
  }

  addMyLocationToMap(resp) {
    this.myLatitude = resp.coords.latitude
    this.myLongitude = resp.coords.longitude
    this.addMarker(this.vehicle)
  }

  getVehicleTypes() {
    this.api.getvehicleTypes()
      .then(data => {
        this.vehicleType = data;
        this.vehicleType.unshift({
          name: "Vehicle Type"
        })
        // console.log(data)
      })
      .catch(err => {
        console.log(err)
      })
  }

  getVehicleDamage(vin_number, checkin_type) {
    this.api.getVehicleDamage(vin_number, checkin_type)
      .then(data => {
        // console.log("damageReport",data[0]);
        if (data[0] != undefined) {
          this.damageReport = data[0];
        }
      })
  }

  getDamageInfo(vin_number, checkin_type) {
    this.api.getDamageInfo(vin_number, checkin_type)
      .then(data => {
        // console.log("damageInfo",data);
        this.damageInfo = data;
      })
  }

  getAccessoriesChecklist(vin_number, checkin_type) {
    this.api.getAccessoriesChecklist(vin_number, checkin_type)
      .then(data => {
        // console.log("accessoriesChecklist",data[0]);
        this.accessories = data[0];
      })
  }

  getAllVehicles() {
    this.api.getAllVehicles()
      .then(data => {
        this.allVehicles = data;
      })
      .then(() => {
        this.toggleSurroundingVehicles()
      })
  }


  loadMap() {
    let latLng = new google.maps.LatLng(this.vehicle.latitude, this.vehicle.longitude);
    let mapOptions = {
      center: latLng,
      zoom: 20,
      tilt: 0,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      disableDefaultUI: true
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.vehicle);
  }

  addMarker(vehicle) {
    let image = { url: 'assets/update/vehicleMarker.png' };
    let me = { url: 'assets/update/myMarker.png' };

    let vehicleMarker = [];

    let marker = new google.maps.Marker({
      map: this.map,
      // animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(vehicle.latitude, vehicle.longitude),
      icon: image
    });
    let content = "<p style='font-size:15px'>VIN: <br><strong>" + vehicle.vin_number + "<strong></p>";
    this.addInfoWindow(marker, content);
    vehicleMarker.push(marker);

    if (this.myLatitude) {
      let myPositionMarker = new google.maps.Marker({
        map: this.map,
        // animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(this.myLatitude, this.myLongitude),
        // position: new google.maps.LatLng(43.75806,-79.79277),
        icon: me
      });
      let myContent = "<p style='font-size:15px'><br><strong> I'm here. <strong></p>";
      this.addInfoWindow(myPositionMarker, myContent);
      vehicleMarker.push(myPositionMarker);
      console.log('im here')
    }

    // force map to show all the markers in its viewport
    var bounds = new google.maps.LatLngBounds();
    if (vehicleMarker.length > 0) {
      for (var j = 0; j < vehicleMarker.length; j++) {
        bounds.extend(vehicleMarker[j].getPosition());
      }
      this.map.fitBounds(bounds);
      this.map.setCenter(bounds.getCenter());
    }
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }


  toggleSurroundingVehicles() {
    // console.log(this.displaySurroundingVehicles)
    if (this.displaySurroundingVehicles) {
      //set markers to the array
      this.setSorroundingvehicleMarkers();
      // add markers to the map
      this.setMapOnAll(this.map);
    } else {
      //remove all sourrounding markers
      this.setMapOnAll(null);
    }
  }

  setSorroundingvehicleMarkers() {
    this.surroundingVehicleMarkers = [];
    let image = { url: 'assets/icon/car-icon-grey.png' };
    for (var i = 0; i < this.allVehicles.length; i++) {
      let v = this.allVehicles[i];
      let distanceInKm = this.calculateDistance(this.vehicle.latitude, v.latitude, this.vehicle.longitude, v.longitude);
      if (distanceInKm == 0) { }
      else {
        // display surrounding vehicles if they are in the radius of 10 meters from the current one
        if (distanceInKm <= 0.01) {
          // console.log('in range: '+distanceInKm);
          let marker1 = new google.maps.Marker({
            map: this.map,
            // animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(v.latitude, v.longitude),
            icon: image
          });
          this.surroundingVehicleMarkers.push(marker1)
          let content1 = "<p style='font-size:15px'>VIN: <br><strong>" + v.vin_number + "<strong></p>";
          this.addInfoWindow(marker1, content1);
        }
      }
    }
  }

  // Sets the map on all markers in the array.
  setMapOnAll(map) {
    for (var i = 0; i < this.surroundingVehicleMarkers.length; i++) {
      this.surroundingVehicleMarkers[i].setMap(map);
    }
  }

  calculateDistance(lat1: number, lat2: number, long1: number, long2: number) {
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    return dis; // in km
  }

  toggleContent() {
    //  this.displayMap = !this.displayMap;
    //  let map= document.getElementById('map-content')
    //  let review= document.getElementById('review-section')

    //  if(this.displayMap){
    //     map.style.zIndex = '1'
    //     map.style.visibility = 'visible'
    //     map.style.marginTop = '10px'
    //     setTimeout(()=>{
    //       review.style.display = "none"
    //     },500)
    //  }else{
    //     map.style.marginTop = '300px'
    //     setTimeout(()=>{
    //       map.style.visibility = 'hidden'
    //       map.style.zIndex = '0'
    //     },500)
    //     review.style.display = "block"
    //  }
    //  if(this.displayMap){
    //     setTimeout(()=> {
    //       this.loadMap();
    //     }, 10)
    //   }
  }

  toggleMoreDetail() {
    this.moreDetail = !this.moreDetail;
    let more = document.getElementById('more-detail');
    let map = document.getElementById('map-content');
    let toggleDiv = document.getElementById('toggle-div');

    let height = this.moreDetailHeight + "px"
    // this.moreDetail ? more.style.height = height : more.style.height = "0px"

    if (this.moreDetail) {
      more.style.height = height
      setTimeout(() => {
        map.style.height = '0px'
        // map.style.transition = "all 100ms"
        toggleDiv.style.display = "none"
        // review.style.display = "block"
      }, 500)
      this.slider.slideTo(0);
      this.toggleTabs = "features";
      this.setSLiderContentHeightToOriginal();
    } else {
      setTimeout(() => {
        map.style.height = 'calc(100vh - 185px)'
        map.style.transition = "all 100ms"
        toggleDiv.style.display = "block"
        // review.style.display = "none"
      }, 100)
      setTimeout(()=>{
        this.toggleTabs = "features";
        this.slider.slideTo(0);
      },500)
      more.style.height = "0px"
      var style = document.createElement('style');
          style.innerHTML =
          '.swiper-wrapper {' +
            'height: 0px !important;' +
          '}';
        var ref = document.querySelector('script');
        ref.parentNode.insertBefore(style, ref);
    }

    if (!this.displayBottomIcon) {
      setTimeout(() => {
        this.displayBottomIcon = !this.displayBottomIcon;
      }, 800)
    } else {
      this.displayBottomIcon = !this.displayBottomIcon;
    }

  }


  gotoAlertsPage() {
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }

  back() {
    if(this.navCtrl.canGoBack()){
      this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('HomePage')
    }
  }

}
