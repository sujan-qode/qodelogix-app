import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleProfileWithAnimationNewPage } from './vehicle-profile-with-animation-new';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VehicleProfileWithAnimationNewPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(VehicleProfileWithAnimationNewPage),
  ],
})
export class VehicleProfileWithAnimationNewPageModule {}
