import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReturnLoanerVehicleStep_1Page } from './return-loaner-vehicle-step-1';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ReturnLoanerVehicleStep_1Page,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(ReturnLoanerVehicleStep_1Page),
  ],
})
export class ReturnLoanerVehicleStep_1PageModule {}
