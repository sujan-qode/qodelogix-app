import { ApiProvider } from './../../providers/api/api';
import { customFunctions } from './../../providers/functions';
// import { AssignLoanerVehicleStep_2Page } from './../assign-loaner-vehicle-step-2/assign-loaner-vehicle-step-2';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
import { env } from '../../environment';

/**
 * Generated class for the ReturnLoanerVehicleStep_1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-return-loaner-vehicle-step-1',
  templateUrl: 'return-loaner-vehicle-step-1.html',
})
export class ReturnLoanerVehicleStep_1Page {

  searchedParam;
  alertsCount;
  loanerData;
  filterData;

  baseUrl = env.base;

  isSearchbarEmpty:boolean= true;
  searchItem;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  searchbarInput;

  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform, public navCtrl: NavController, public navParams: NavParams, private func: customFunctions, private _api: ApiProvider) {
    this._api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.searchedParam = this.navParams.get('searchedParam') ? this.navParams.get('searchedParam') : false;
    this.getAllLoaners();
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignLoanerVehicleStep_1Page');
  }

  ionViewDidLeave(){
    this.func.dismissLoading();
  }

  getAllLoaners() {
    this.func.presentLoading();
    this._api.getLoanerAssigned()
    .then(data => {
      console.log(data)
      this.loanerData = data;
      this.filterData = data;
      this.func.dismissLoading();
    }).then(()=> {
      if(this.searchedParam){
        console.log('searchbarInput')
        this.searchbarInput = this.searchedParam;
        this.searchedParam = false;
        this.filterByVin(this.searchbarInput)
      }
    });
  }

  onSearchCancel(ev:any){
    this.getAllLoaners();
    this.searchbarInput = '';
  }

  getItems(ev: any) {
    let val = ev.target.value;
    this.filterByVin(val)
  }

  filterByVin(val) {
    console.log(val)
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.loanerData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number + " " + vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
    }
    else{
      this.getAllLoaners();
      this.isSearchbarEmpty = true;
    }
  }

  gotoAssignLoanerStep2(v){
    this.func.presentLoading();
    this.navCtrl.push('AssignLoanerVehicleStep_2Page', {vehicle: v, operation: 'return'}, this.animationsOptions);
  }

  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back(){
    this.navCtrl.pop();
  }


}
