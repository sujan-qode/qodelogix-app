import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { customFunctions } from '../../providers/functions';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { VehicleProfilePage } from '../vehicle-profile/vehicle-profile';
// import { HomePage } from '../home/home';
import { env } from '../../environment';
// import { VehicleProfileNewPage } from '../vehicle-profile-new/vehicle-profile-new';
// import { VehicleProfileWithAnimationNewPage } from '../vehicle-profile-with-animation-new/vehicle-profile-with-animation-new';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  
  @ViewChild('map') mapElement: ElementRef;
  map: any;

  baseUrl = env.base;

  vehicle;
  alertsCount;
  vehicle_status = "-1";
  alert_type = "-1";
  searchVin;
  allVehicles = null;
  filterData = null;
  filterData1 = null;
  filterData2 = null;
  backupData = null;
  emptySearchbox = true;
  selectedMarker = null;
  vinDetail = null;
  selectedVehicle = null;

  isRootPage = false;

  displaySurroundingVehicles = false;
  displayVehicleInfo = false;

  currentVehicle = [];
  allVehicleMarkers = [];
  filteredVehicleMarkers = [];
  surroundingVehicleMarkers = [];
  surroundingVehicles = [];

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  geofences;
  // facilityblock = new google.maps.Polygon({ })
  facilityblock;

  isTabletOrIpad: boolean;
  interface: string = "popover";
  vehicleType;

  vehicle_lot = 1;

  defaultGeofence;

  constructor(private platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    private _api: ApiProvider,
    public func: customFunctions,
  ) {
    this.func.presentLoading();

    this._api.getAllAlertsCount();
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if (!this.isTabletOrIpad) {
      this.interface = 'action-sheet';
    }

    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.getAllVehicles();

    var interval = setInterval(()=>{
      if(this.allVehicles){
        this.getGeofence();
        // setTimeout(() => {
        //   this.populateVehiclesOnMap();
        // }, 1000);
        this.getVehicleTypes();
        clearInterval(interval);
      }
    }, 200)

    //   this.getGeofence();
    //   setInterval(() => {
    //     this.populateVehiclesOnMap();
    //   }, 1000);
    //   this.getVehicleTypes();
  }

  ionViewDidLoad() {
    var interval = setInterval(()=>{
      if(this.allVehicles){
        this.loadMap();
        this.func.dismissLoading();
        clearInterval(interval);
      }
      // this.func.dismissLoading();
    })
  }


  getVehicleTypes(){
    this._api.getvehicleTypes()
      .then( data => {
        this.vehicleType = data;
        console.log(data)
      })
      .catch(err => {
        console.log(err)
      })
  }

  getGeofence() {
    this._api.getGeofence()
      .then(data => {
        this.geofences = data;
        this.setMapSettingsDropdown(this.map);
        this.setGeofenceDropdown(this.map);
        this.setMapZoomControl(this.map);
        console.log(data)
      })
      .then(()=>{
        this.defaultGeofence = this.geofences.filter(geofence =>{
          if(geofence.is_default == 1){
            return geofence;
          }
        })
      })
      .then(()=>{
        this.plotDefaultGeofence();
        this.setAllOnLotVehiclesMarkers();
      })
  }
  
  plotDefaultGeofence(){
    //getting default geofence
    var polygonCoords = this.defaultGeofence[0]["plotData"];
    // console.log(polygonCoords)
    let bounds = new google.maps.LatLngBounds();
    let poly = [];
    for(var j = 0; j <polygonCoords.length; j++){
      let point = new google.maps.LatLng(polygonCoords[j].lat.toString(), polygonCoords[j].lng.toString())
      bounds.extend(point)
      poly.push(point)
    }
    this.facilityblock = new google.maps.Polygon({
      paths: poly,
      strokeColor: '#FF0000',
      strokeOpacity: 0,
      strokeWeight: 1,
      fillColor: '#FF0000',
      fillOpacity: 0
    });
    this.facilityblock.setMap(this.map);
    this.map.fitBounds(bounds)
  }

  plotGeofence(geofence){
    console.log(geofence)
    var polygonCoords = geofence["plotData"];
    let bounds = new google.maps.LatLngBounds();
    let poly = [];
    for(var j = 0; j <polygonCoords.length; j++){
      let point = new google.maps.LatLng(polygonCoords[j].lat.toString(), polygonCoords[j].lng.toString())
      bounds.extend(point)
      poly.push(point)
    }
    this.facilityblock = new google.maps.Polygon({
      paths: poly,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#FF0000',
      fillOpacity: 0.2
    });
    this.facilityblock.setMap(this.map);
    this.map.fitBounds(bounds)
  }


  setGeofenceDropdown(map) {
    let self = this;
    var GeofencecontrolDiv = document.createElement('div');
    GeofencecontrolDiv.className = 'dropdown-container';

    var geoFenceDropdownWrapper = document.createElement('div');
    geoFenceDropdownWrapper.className = 'dropdown-content';
    geoFenceDropdownWrapper.innerText = 'Geo-Fence Settings';
    GeofencecontrolDiv.appendChild(geoFenceDropdownWrapper);

    var arrow = document.createElement('div');
    arrow.className = 'dropDownArrow';
    geoFenceDropdownWrapper.appendChild(arrow);
    // Adding dropdown to map
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(GeofencecontrolDiv);

    GeofencecontrolDiv.addEventListener('click', function () {
      // if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-none")) {
      //   document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-none")
      // }
      // document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-block");

      if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-none")) {
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-none")
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-block");
      }
      else if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-block")) {
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-block")
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-none");
      }else{
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-block");
      }

    });

    var MapOptionDiv = document.createElement('div');
    MapOptionDiv.className = 'geofence-dropdown-option-container';
    MapOptionDiv.style.display = "none";

    for(var i=0; i<this.geofences.length; i++){

      var MapOption1Div = document.createElement('div');
      MapOption1Div.className = 'option';
      MapOption1Div.id = ""+i+"";
      MapOption1Div.innerText = this.geofences[i]['region_name']
      MapOptionDiv.appendChild(MapOption1Div);

      MapOption1Div.addEventListener('click', function () {
        if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-block")) {
          document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-block")
        }
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-none");
      });
    }

    var MapOption2Div = document.createElement('div');
    MapOption2Div.className = 'option clear-fence';
    MapOption2Div.id = "clear-fence";
    MapOption2Div.innerText = "Clear"
      MapOptionDiv.appendChild(MapOption2Div);

      MapOption2Div.addEventListener('click', function () {
        if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-block")) {
          document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-block")
        }
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-none");
      });


    document.addEventListener('click',function(e){
      if(e.target && e.srcElement.id == "clear-fence"){
        self.facilityblock.setMap(null); // remove previous geofence
      }
      else if(e.target && e.srcElement.id){
        let id = e.srcElement.id;
        self.facilityblock.setMap(null); // remove previous geofence
        self.plotGeofence(self.geofences[id])
      }
   });
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(MapOptionDiv);
  }

  setMapSettingsDropdown(map) {
    var MapSettingscontrolDiv = document.createElement('div');
    MapSettingscontrolDiv.className = 'dropdown-container';

    var MapSettingsDropdownWrapper = document.createElement('div');
    MapSettingsDropdownWrapper.className = 'dropdown-content';
    MapSettingsDropdownWrapper.innerText = 'Map Settings';
    MapSettingscontrolDiv.appendChild(MapSettingsDropdownWrapper);

    var arrow = document.createElement('div');
    arrow.className = 'dropDownArrow';
    MapSettingsDropdownWrapper.appendChild(arrow);

    // Adding dropdown to map
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(MapSettingscontrolDiv);

    MapSettingscontrolDiv.addEventListener('click', function () {
      if (document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-none")) {
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-none")
        document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-block");
      }
      else if (document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-block")) {
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-block")
        document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-none");
      }else{
        document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-block");
      }

    });

    var MapOptionDiv = document.createElement('div');
    MapOptionDiv.className = 'dropdown-option-container';
    MapOptionDiv.style.display = "none";

    var MapOption1Div = document.createElement('div');
    MapOption1Div.className = 'option';
    MapOption1Div.innerText = 'Map';
    MapOptionDiv.appendChild(MapOption1Div);

    var MapOption2Div = document.createElement('div');
    MapOption2Div.className = 'option';
    MapOption2Div.innerText = 'Satellite';
    MapOptionDiv.appendChild(MapOption2Div);

    var MapOption3Div = document.createElement('div');
    MapOption3Div.className = 'option';
    MapOption3Div.innerText = 'Terrian';
    MapOptionDiv.appendChild(MapOption3Div);
    // Adding dropdown to map
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(MapOptionDiv);

    MapOption1Div.addEventListener('click', function () {
      map.setMapTypeId('roadmap')
      if (document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-block")) {
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-block")
      }
      document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-none");
    });
    MapOption2Div.addEventListener('click', function () {
      map.setMapTypeId('satellite')
      if (document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-block")) {
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-block")
      }
      document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-none");
    });
    MapOption3Div.addEventListener('click', function () {
      map.setMapTypeId('terrain')
      if (document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-block")) {
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-block")
      }
      document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-none");
    });

  }

  setMapZoomControl(map) {
    // Creating divs & styles for custom zoom control
    var controlDiv = document.createElement('div');
    controlDiv.className = 'zoom-btn-container';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.className = 'zoom-btn-controlWrapper';
    controlDiv.appendChild(controlWrapper);

    // Set CSS for the reset
    var resetButton = document.createElement('div');
    resetButton.className = 'zoom-btn-resetButton';
    controlWrapper.appendChild(resetButton);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.className = 'zoom-btn-zoomInButton';
    controlWrapper.appendChild(zoomInButton);

    // CSS for divider
    var bar = document.createElement('div');
    bar.className = 'zoom-btn-bar';
    // adding divider
    controlWrapper.appendChild(bar);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.className = 'zoom-btn-zoomOutButton';
    controlWrapper.appendChild(zoomOutButton);

    //css for bar2
    var bar2 = document.createElement('div');
    bar2.className = 'zoom-btn-bar2';
    // adding divider
    controlWrapper.appendChild(bar2);

    // Adding buttons to map
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);

    // Setup the click event listener - zoomIn
    zoomInButton.addEventListener('click', function () {
      map.setZoom(map.getZoom() + 1);
    });
    // Setup the click event listener - zoomOut
    zoomOutButton.addEventListener('click', function () {
      map.setZoom(map.getZoom() - 1);
    });
    // Setup the click event listener - reset
    let self = this;
    resetButton.addEventListener('click', function () {
      self.facilityblock.setMap(null); // remove previous geofence
      self.setAllVehiclesOnMap(null);
      self.setCurrentVehicleOnMap(null);
      self.setFilteredVehiclesOnMap(null);
      self.setAllVehiclesMarkers();
      // self.setAllVehiclesOnMap(map);
      self.vehicle_lot = 1;
      self.OnVehiclesLotChange('');
    });
  }


  getAllVehicles() {
    this._api.getAllVehicles()
      .then(data => {
        // console.log(data)
        this.filterData = data;
        this.allVehicles = data;
        this.backupData = data;
      });
  }

  populateVehiclesOnMap() {
    if (this.emptySearchbox) {
      this.setAllVehiclesMarkers();
      this.setAllVehiclesOnMap(this.map);
      this.emptySearchbox = false;
    }
  }

  populateFilteredvehiclesOnMap(filterData) {
    console.log(filterData)
    this.setAllVehiclesOnMap(null);
    this.setCurrentVehicleOnMap(null);
    this.setFilteredVehiclesOnMap(null);
    this.setFilteredVehiclesMarkers(filterData);
    this.setFilteredVehiclesOnMap(this.map)
  }


  selectVehicle(vehicle) {

        this.displaySurroundingVehicles = false;
        this.setSelectedMarker(vehicle);
        this.map.setCenter(new google.maps.LatLng(vehicle.latitude.toString(), vehicle.longitude.toString()));
        this.map.setZoom(20)
        this.displayVehicleInfo = true;

    // this.vehicle = vehicle;
    // this.setAllVehiclesOnMap(null);
    // this.setCurrentVehicleOnMap(null);
    // this.setcurrentVehicleMarker();
    // this.map.setCenter(new google.maps.LatLng(vehicle.latitude.toString(), vehicle.longitude.toString()));
    // this.map.setZoom(20);
    // this.displayVehicleInfo = true;
    // this.setSelectedMarker(vehicle);
    // this.vehicle.setMap(null);
    // this.addMarker();
  }

  searchItemsByVin(ev: any) {
    this.searchVin = ev.target.value;
    this.filterVehicle();
  }

  OnVehicleStatusChange(ev: any) {
    this.filterVehicle();
  }
  OnAlertTypeChange(ev: any) {
    this.filterVehicle();
  }

  OnVehiclesLotChange(ev: any) {
    // console.log(this.vehicle_lot)
    // offlot - Display all vehicles in road view
    if(this.vehicle_lot == 0){
      this.map.setMapTypeId('roadmap');
      this.setAllVehiclesOnMap(null);
      this.setCurrentVehicleOnMap(null);
      this.setFilteredVehiclesOnMap(null);

      if (!this.searchVin && this.alert_type == "-1") {
        setTimeout(() => {
          // this.populateVehiclesOnMap();
          this.setAllOffLotVehiclesMarkers();
        }, 1000);
      }
      else{
        this.filterVehicle();
      }
    }else{
      this.map.setMapTypeId('satellite');
      this.setAllVehiclesOnMap(null);
      this.setCurrentVehicleOnMap(null);
      this.setFilteredVehiclesOnMap(null);

      if (!this.searchVin && this.alert_type == "-1") {
        this.setAllOnLotVehiclesMarkers();
        this.plotDefaultGeofence();
      }
      else{
        this.filterVehicle();
      }

    }
  }

  filterVehicle() {
    this.displayVehicleInfo = false;
    let vin = this.searchVin;
    console.log(vin, this.vehicle_status, this.alert_type)
    this.emptySearchbox = false;
    // empty vehicle status and alert type
    if (this.vehicle_status == "-1" && this.alert_type == "-1") {
      this.allVehicles = this.backupData;
      if (vin !== undefined) {
        this.filterData = this.allVehicles.filter((vehicle) => {
          let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.stock_number;
          return ((makeModelVin.toLowerCase().indexOf(vin.toLowerCase()) > -1) && (this.vehicle_lot == vehicle.in_out_location));
          // return ((vehicle.vin_number.toLowerCase().indexOf(vin.toLowerCase()) > -1) );
        });
      }
      else {
        this.filterData = this.backupData;
      }
      console.log(this.filterData)
      this.populateFilteredvehiclesOnMap(this.filterData);
    }
    // empty alert type but vehicle status provided
    if (this.alert_type == "-1" && this.vehicle_status != "-1") {
      this.allVehicles = this.backupData;
      this.filterData = this.allVehicles.filter((vehicle) => {
        if (vin !== undefined) {
          let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.stock_number;
          // return ((makeModelVinType.toLowerCase().indexOf(vin.toLowerCase()) > -1));
          return ((vehicle.Vehicle_type.toLowerCase().indexOf(this.vehicle_status.toLowerCase()) > -1) && (makeModelVin.toLowerCase().indexOf(vin.toLowerCase()) > -1));
        } else {
          return ((vehicle.Vehicle_type.toLowerCase().indexOf(this.vehicle_status.toLowerCase()) > -1));
        }
      });
      this.populateFilteredvehiclesOnMap(this.filterData);
    }
    // empty vehicle status but alert type provided
    if (this.alert_type != "-1" && this.vehicle_status == "-1") {
      this.allVehicles = this.backupData;
      if (vin !== undefined) {
        if (this.alert_type == '0') { // battery alert
          this.getBatteryAlerts()
        }
        else if (this.alert_type == '1') { // fuel alert 
          this.getFuelAlerts();
        }
        else if (this.alert_type == '2') { // Mileage alert 
          this.getMileageAlerts();
        }
        else if (this.alert_type == '3') { // GPS alert 
          this.getGpsAlerts();
        }
        else if (this.alert_type == '4') { // Disconnect alert 
          this.getDisconnectAlerts();
        }
        else {
          this.alert_type = '-1';
          this.filterVehicle();
        }
      }
      else {
        if (this.alert_type == '0') { // battery alert
          this.allBatteryAlerts();
        }
        else if (this.alert_type == '1') { // fuel alert 
          this.allFuelAlerts();
        }
        else if (this.alert_type == '2') { // Mileage alert 
          this.allMileageAlerts();
        }
        else if (this.alert_type == '3') { // GPS alert 
          this.allGpsAlerts();
        }
        else if (this.alert_type == '4') { // Disconnect alert 
          this.allDisconnectAlerts();
        }
        else {
          this.alert_type = '-1';
          this.filterData = this.backupData
        }
      }
    }

    // if both vehicle status and alert type are provided
    if (this.alert_type != "-1" && this.vehicle_status != "-1") {
      this.allVehicles = this.backupData;

      if (this.alert_type == '0') { // battery alert
        this.getBatteryAlerts2(vin);
      }
      if (this.alert_type == '1') { // fuel alert 
        this.getFuelAlerts2(vin);

      }
      if (this.alert_type == '3') { // GPS alert 
        this.getGpsAlerts2(vin);
      }
      if (this.alert_type == '2') { // Mileage alert 
        this.getMileageAlerts2(vin);
      }
      if (this.alert_type == '4') { // Disconnect alert 
        this.getDisconnectAlerts2(vin);
      }
    }
  }

  allBatteryAlerts() {
    this._api.getBatteryAlerts()
      .then(data => {
        this.filterData = data;
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }
  allFuelAlerts() {
    this._api.getFuelAlerts()
      .then(data => {
        this.filterData = data;
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }
  allMileageAlerts() {
    this._api.getMileageAlerts()
      .then(data => {
        this.filterData = data;
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }
  allGpsAlerts() {
    this._api.getGpsAlerts()
      .then(data => {
        this.filterData = data;
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }
  allDisconnectAlerts() {
    this._api.getDisconnectAlerts()
      .then(data => {
        this.filterData = data;
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }


  getBatteryAlerts() {
    this._api.getBatteryAlerts()
      .then(data => {
        this.filterData2 = data;
      }).then(() => {
        this.filterByVin(this.searchVin);
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }
  getFuelAlerts() {
    this._api.getFuelAlerts()
      .then(data => {
        this.filterData2 = data;
      }).then(() => {
        this.filterByVin(this.searchVin);
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }
  getMileageAlerts() {
    this._api.getMileageAlerts()
      .then(data => {
        this.filterData2 = data;
      }).then(() => {
        this.filterByVin(this.searchVin);
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }
  getGpsAlerts() {
    this._api.getGpsAlerts()
      .then(data => {
        this.filterData2 = data;
      }).then(() => {
        this.filterByVin(this.searchVin);
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }

  getDisconnectAlerts() {
    this._api.getDisconnectAlerts()
      .then(data => {
        this.filterData2 = data;
      }).then(() => {
        this.filterByVin(this.searchVin);
      }).then(() => {
        this.populateFilteredvehiclesOnMap(this.filterData);
      })
  }



  getBatteryAlerts2(vin) {
    if (vin != undefined && vin.trim != '') {
      this._api.getBatteryAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            let makeModelVinType = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.Vehicle_type + " " + vehicle.stock_number;
            return ((this.vehicle_lot == vehicle.in_out_location) && (makeModelVinType.toLowerCase().indexOf(vin.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    } else {
      this._api.getBatteryAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            return ((this.vehicle_lot == vehicle.in_out_location) && (vehicle.Vehicle_type.toLowerCase().indexOf(this.vehicle_status.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    }
  }
  getFuelAlerts2(vin) {
    if (vin != undefined && vin.trim != '') {
      this._api.getFuelAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            let makeModelVinType = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.Vehicle_type + " " + vehicle.stock_number;
            return ((this.vehicle_lot == vehicle.in_out_location) && (makeModelVinType.toLowerCase().indexOf(vin.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    } else {
      this._api.getFuelAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            return ((this.vehicle_lot == vehicle.in_out_location) && (vehicle.Vehicle_type.toLowerCase().indexOf(this.vehicle_status.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    }
  }

  getMileageAlerts2(vin) {
    if (vin != undefined && vin.trim != '') {
      this._api.getMileageAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            let makeModelVinType = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.Vehicle_type + " " + vehicle.stock_number;
            return ((this.vehicle_lot == vehicle.in_out_location) && (makeModelVinType.toLowerCase().indexOf(vin.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    } else {
      this._api.getMileageAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            return ((this.vehicle_lot == vehicle.in_out_location) && (vehicle.Vehicle_type.toLowerCase().indexOf(this.vehicle_status.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    }
  }
  getGpsAlerts2(vin) {
    if (vin != undefined && vin.trim != '') {
      this._api.getGpsAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            let makeModelVinType = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.Vehicle_type + " " + vehicle.stock_number;
            return ((this.vehicle_lot == vehicle.in_out_location) && (makeModelVinType.toLowerCase().indexOf(vin.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    } else {
      this._api.getGpsAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            return ((this.vehicle_lot == vehicle.in_out_location) && (vehicle.Vehicle_type.toLowerCase().indexOf(this.vehicle_status.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    }
  }
  getDisconnectAlerts2(vin) {
    if (vin != undefined && vin.trim != '') {
      this._api.getDisconnectAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            let makeModelVinType = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.Vehicle_type + " " + vehicle.stock_number;
            return ((this.vehicle_lot == vehicle.in_out_location) && (makeModelVinType.toLowerCase().indexOf(vin.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    } else {
      this._api.getDisconnectAlerts()
        .then(data => {
          this.filterData2 = data;
        }).then(() => {
          this.filterData = this.filterData2.filter((vehicle) => {
            console.log(this.vehicle_status, vehicle.Vehicle_type)
            return ((this.vehicle_lot == vehicle.in_out_location) && (vehicle.Vehicle_type.toLowerCase().indexOf(this.vehicle_status.toLowerCase()) > -1));
          });
        }).then(() => {
          this.populateFilteredvehiclesOnMap(this.filterData);
        })
    }
  }


  filterByVin(val) {
    // this.emptySearchbox = false;
    this.filterData = this.filterData2.filter((vehicle) => {
      let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number + " " + vehicle.stock_number;
      return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) && (this.vehicle_lot == vehicle.in_out_location));
    });
  }

  loadMap() {
    let latLng = new google.maps.LatLng(this.allVehicles[0].latitude, this.allVehicles[0].longitude);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      tilt: 0,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      disableDefaultUI: true
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  }



  setcurrentVehicleMarker() {
    // let image = {url: 'assets/icon/marker_knob_blue.svg'};
    let image = { url: 'assets/icon/car-icon-black.png' };
    // for(var i=0;i<this.allVehicles.length;i++){
    let v = this.vehicle
    let marker1 = new google.maps.Marker({
      map: this.map,
      position: new google.maps.LatLng(v.latitude, v.longitude),
      icon: image
    });
    marker1.addListener('click', () => {
      this.displaySurroundingVehicles = false;
      this.setSelectedMarker(v);
      this.map.setZoom(20);
      this.displayVehicleInfo = true;
    });
    this.currentVehicle.push(marker1)
    // let content1 = "<p style='font-size:15px'>VIN: <br><strong>"+ v.vin_number+"<strong></p>";         
    // this.addInfoWindow(marker1, content1);
    // }
  }
  setCurrentVehicleOnMap(map) {
    for (var i = 0; i < this.currentVehicle.length; i++) {
      this.currentVehicle[i].setMap(map);
    }
  }

  addMarker() {
    // let image = {url: 'assets/icon/marker_knob_blue.svg'};
    let image = { url: 'assets/icon/car-icon-black.png' };
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(this.vehicle.latitude, this.vehicle.longitude),
      icon: image
    });
    let content = "<p style='font-size:15px'>VIN: <br><strong>test<strong></p>";
    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  setSelectedMarker(v) {
    this.selectedMarker = v;
  }



  setAllVehiclesMarkers() {
    this.surroundingVehicleMarkers = [];
    //filter all vhicles with on-lot vehicles
    // let allVehicles = this.allVehicles.filter(vehicle =>{
    //   if(vehicle.in_out_location == 1){
    //     return vehicle;
    //   }
    // })
    // let image = {url: 'assets/icon/marker_knob_blue.svg'};
    let image = { url: 'assets/icon/car-icon-black.png' };
    for (var i = 0; i < this.allVehicles.length; i++) {
      let v = this.allVehicles[i];
      let marker1 = new google.maps.Marker({
        map: this.map,
        position: new google.maps.LatLng(v.latitude, v.longitude),
        icon: image
      });
      marker1.addListener('click', () => {
        this.displaySurroundingVehicles = false;
        this.setSelectedMarker(v);
        this.map.setCenter(new google.maps.LatLng(v.latitude, v.longitude));
        this.map.setZoom(20)
        this.displayVehicleInfo = true;
      });
      this.allVehicleMarkers.push(marker1)
      let content1 = '<p><strong> VIN: <br>' + v.vin_number + '<strong></p>';
      this.addInfoWindow(marker1, content1);
    }
    // force map to show all the markers in its viewport
    var bounds = new google.maps.LatLngBounds();
    if (this.allVehicleMarkers.length > 0) {
      for (var j = 0; j < this.allVehicleMarkers.length; j++) {
        bounds.extend(this.allVehicleMarkers[j].getPosition());
      }
      this.map.fitBounds(bounds);
      this.map.setCenter(bounds.getCenter());
    }
  }

  setAllOnLotVehiclesMarkers() {
    this.surroundingVehicleMarkers = [];
    //filter all vhicles with on-lot vehicles
    let allVehicles = this.allVehicles.filter(vehicle =>{
      if(vehicle.in_out_location == 1){
        return vehicle;
      }
    })
    this.filterData = allVehicles;
    // let image = {url: 'assets/icon/marker_knob_blue.svg'};
    let image = { url: 'assets/icon/car-icon-black.png' };
    for (var i = 0; i < allVehicles.length; i++) {
      let v = allVehicles[i];
      let marker1 = new google.maps.Marker({
        map: this.map,
        position: new google.maps.LatLng(v.latitude, v.longitude),
        icon: image
      });
      marker1.addListener('click', () => {
        this.displaySurroundingVehicles = false;
        this.setSelectedMarker(v);
        this.map.setCenter(new google.maps.LatLng(v.latitude, v.longitude));
        this.map.setZoom(20)
        this.displayVehicleInfo = true;
      });
      this.allVehicleMarkers.push(marker1)
      let content1 = '<p><strong> VIN: <br>' + v.vin_number + '<strong></p>';
      this.addInfoWindow(marker1, content1);
    }
    // force map to show all the markers in its viewport
    // var bounds = new google.maps.LatLngBounds();
    // if (this.allVehicleMarkers.length > 0) {
    //   for (var j = 0; j < this.allVehicleMarkers.length; j++) {
    //     bounds.extend(this.allVehicleMarkers[j].getPosition());
    //   }
    //   this.map.fitBounds(bounds);
    //   this.map.setCenter(bounds.getCenter());
    // }
  }

  setAllOffLotVehiclesMarkers() {
    this.surroundingVehicleMarkers = [];
    //filter all vhicles with off-lot vehicles
    let allVehicles = this.allVehicles.filter(vehicle =>{
      if(vehicle.in_out_location == 0){
        return vehicle;
      }
    })
    this.filterData = allVehicles;
    console.log(allVehicles)
    // let image = {url: 'assets/icon/marker_knob_blue.svg'};
    let image = { url: 'assets/icon/car-icon-black.png' };
    for (var i = 0; i < allVehicles.length; i++) {
      let v = allVehicles[i];
      let marker1 = new google.maps.Marker({
        map: this.map,
        position: new google.maps.LatLng(v.latitude, v.longitude),
        icon: image
      });
      marker1.addListener('click', () => {
        this.displaySurroundingVehicles = false;
        this.setSelectedMarker(v);
        this.map.setCenter(new google.maps.LatLng(v.latitude, v.longitude));
        this.map.setZoom(20)
        this.displayVehicleInfo = true;
      });
      this.allVehicleMarkers.push(marker1)
      let content1 = '<p><strong> VIN: <br>' + v.vin_number + '<strong></p>';
      this.addInfoWindow(marker1, content1);
    }
    // force map to show all the markers in its viewport
    var bounds = new google.maps.LatLngBounds();
    if (this.allVehicleMarkers.length > 0) {
      for (var j = 0; j < this.allVehicleMarkers.length; j++) {
        bounds.extend(this.allVehicleMarkers[j].getPosition());
      }
      this.map.fitBounds(bounds);
      this.map.setCenter(bounds.getCenter());
    }
  }
  setAllVehiclesOnMap(map) {
    for (var i = 0; i < this.allVehicleMarkers.length; i++) {
      this.allVehicleMarkers[i].setMap(map);
    }
  }


  setFilteredVehiclesMarkers(filterData) {
    this.filteredVehicleMarkers = [];
    let image = { url: 'assets/icon/car-icon-black.png' };
    for (var i = 0; i < filterData.length; i++) {
      let v = filterData[i];
      let marker1 = new google.maps.Marker({
        map: this.map,
        position: new google.maps.LatLng(v.latitude, v.longitude),
        icon: image
      });
      marker1.addListener('click', () => {
        this.displaySurroundingVehicles = false;
        this.setSelectedMarker(v);
        this.map.setCenter(new google.maps.LatLng(v.latitude, v.longitude));
        this.map.setZoom(20)
        this.displayVehicleInfo = true;
      });
      this.filteredVehicleMarkers.push(marker1)
      let content1 = '<p><strong> VIN: <br>' + v.vin_number + '<strong></p>';
      this.addInfoWindow(marker1, content1);
    }
    // force map to show all the markers in its viewport
    var bounds = new google.maps.LatLngBounds();
    console.log(this.filteredVehicleMarkers.length)
    if (this.filteredVehicleMarkers.length > 0) {
      for (var j = 0; j < this.filteredVehicleMarkers.length; j++) {
        bounds.extend(this.filteredVehicleMarkers[j].getPosition());
      }
      this.map.fitBounds(bounds);
      this.map.setCenter(bounds.getCenter());
    }
  }
  setFilteredVehiclesOnMap(map) {
    for (var i = 0; i < this.filteredVehicleMarkers.length; i++) {
      this.filteredVehicleMarkers[i].setMap(map);
    }
  }



  toggleSurroundingVehicles(vehicle) {
    this.selectedVehicle = vehicle;
    if (this.displaySurroundingVehicles) {
      //set markers to the array
      this.setSorroundingvehicleMarkers();
      // add markers to the map
      this.setMapOnAll(this.map);
    } else {
      //remove all sourrounding markers
      this.setMapOnAll(null);
    }
    console.log(this.surroundingVehicles)
  }

  setSorroundingvehicleMarkers() {
    this.surroundingVehicleMarkers = [];
    this.surroundingVehicles = [];
    this.setMapOnAll(null);
    // let image = {url: 'assets/icon/marker_knob_grey.svg'};
    let image = { url: 'assets/icon/car-icon-grey.png' };
    for (var i = 0; i < this.backupData.length; i++) {
      let v = this.backupData[i];
      let distanceInKm = this.calculateDistance(this.selectedVehicle.latitude, v.latitude, this.selectedVehicle.longitude, v.longitude);
      if (distanceInKm == 0) { }
      else {
        // display surrounding vehicles if they are in the radius of 10 meters from the current one
        if (distanceInKm <= 0.01) {
          console.log('in range: ' + distanceInKm);
          let marker1 = new google.maps.Marker({
            map: this.map,
            position: new google.maps.LatLng(v.latitude, v.longitude),
            icon: image
          });
          this.surroundingVehicleMarkers.push(marker1)
          this.surroundingVehicles.push(v)
          let content1 = "<p style='font-size:15px'>VIN: <br><strong>" + v.vin_number + "<strong></p>";
          this.addInfoWindow(marker1, content1);
        }
      }
    }
  }
  // Sets the map on all markers in the array for surrounding vehicles.
  setMapOnAll(map) {
    for (var i = 0; i < this.surroundingVehicleMarkers.length; i++) {
      this.surroundingVehicleMarkers[i].setMap(map);
    }
  }

  calculateDistance(lat1: number, lat2: number, long1: number, long2: number) {
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    return dis; // in km
  }

  closeVehicleDetail() {
    this.displayVehicleInfo = false;
    this.displaySurroundingVehicles = false;
    // this.emptySearchbox = true;
    //remove all sourrounding markers
    // this.setMapOnAll(null);
    // this.populateVehiclesOnMap();
  }


  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  gotoVehicleProfile(vehicle) {
    let topContent = document.getElementById('page-content');
    this.navCtrl.push('VehicleProfileWithAnimationNewPage', {topContent:topContent.offsetTop , vehicle: vehicle}, this.animationsOptions);
    // this.navCtrl.push('VehicleProfilePage', {vehicle: vehicle}, this.animationsOptions);
    // this.navCtrl.push('VehicleProfileNewPage', { vehicle: vehicle }, this.animationsOptions);
  }

  back() {
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
