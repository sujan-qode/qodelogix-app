import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleProfileWithAnimationPage } from './vehicle-profile-with-animation';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VehicleProfileWithAnimationPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(VehicleProfileWithAnimationPage),
  ],
})
export class VehicleProfileWithAnimationPageModule {}
