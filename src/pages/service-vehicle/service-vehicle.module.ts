import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceVehiclePage } from './service-vehicle';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ServiceVehiclePage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(ServiceVehiclePage),
  ],
})
export class ServiceVehiclePageModule {}
