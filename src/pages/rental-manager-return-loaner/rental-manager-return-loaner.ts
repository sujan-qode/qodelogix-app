import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { HomePage } from '../home/home';
import { env } from '../../environment';
import { ApiProvider } from '../../providers/api/api';
import { customFunctions } from '../../providers/functions';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { RentalManagerVehicleProfilePage } from '../rental-manager-vehicle-profile/rental-manager-vehicle-profile';

/**
 * Generated class for the RentalManagerReturnLoanerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-return-loaner',
  templateUrl: 'rental-manager-return-loaner.html',
})
export class RentalManagerReturnLoanerPage {

  isRootPage = false;
  alertsCount;
  
  baseUrl = env.base;

  filterData;
  vehiclesData;
  tempData;
  isSearchbarEmpty: boolean = true;

  isTabletOrIpad: boolean;
  interface:string="popover";


  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }


  isAdvanceSearchDisabled = false;

  operation;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private platform: Platform,
              private api: ApiProvider,
              private func: customFunctions ) {
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if(!this.isTabletOrIpad){
      this.interface='action-sheet';
    }

    this.getLoanerAssigned();

    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentalManagerSelectVehiclePage');
  }

  getLoanerAssigned() {
    this.func.presentLoading();
    this.api.getLoanerAssigned()
      .then(data => {
        console.log(data)
        this.vehiclesData = data;
        this.filterData = data;
        this.tempData = data;
      }).then(()=>{
        this.func.dismissLoading();
      })
  }

  getItems(ev: any) {
    let val = ev.target.value;
    console.log(val)
    this.filterByVin(val)
  }

  filterByVin(val) {
    console.log(val)
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.vehiclesData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1));
      });
    }
    else {
      this.getLoanerAssigned();
      this.isSearchbarEmpty = true;
    }
  }

  gotoVehicleProfileWithAnimation(e, i, v){
    this.navCtrl.push('RentalManagerVehicleProfilePage', {operationType: this.operation, vehicle: v}, this.animationsOptions);
  }

  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back() {
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}

