import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { CameraOptions, Camera} from '@ionic-native/camera';
import { File,FileEntry } from "@ionic-native/file";
import { normalizeURL } from 'ionic-angular';
import { customFunctions } from '../../providers/functions';
import { ApiProvider } from '../../providers/api/api';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
// import { CustomerListPage } from '../customer-list/customer-list';


/**
 * Generated class for the CreateCustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-customer',
  templateUrl: 'create-customer.html',
})
export class CreateCustomerPage {

  alertsCount;
  isRootPage = false;

  customer = {
    first_name : '',
    last_name : '',
    phone_number : '',
    customer_email : '',
    address_one : '',
    address_two : '',
    city : '',
    country : '', 
    postal_code : '',
    dl_front : '',
    dl_back : '',
    cc_front : '',
    cc_back : '',
    ic_front : '',
    ic_back : '',
  }
  
  vin=[];
  year=[];
  make=[];
  model=[];
  plate_num=[];
  mileage=[];
  
  dl_front_imageUri;
  dl_front_imageName;
  dl_back_imageUri;
  dl_back_imageName;
  cc_front_imageUri;
  cc_front_imageName;
  cc_back_imageUri;
  cc_back_imageName;
  ic_front_imageUri;
  ic_front_imageName;
  ic_back_imageUri;
  ic_back_imageName;


  cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.PNG,
      targetWidth: 512,
      targetHeight: 512,
      saveToPhotoAlbum: true,
      correctOrientation: true,
  }

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  num = 1;
  vehicleNum;

  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform,
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private camera: Camera,
              private readonly file: File,
              private func: customFunctions,
              public api : ApiProvider,
              private barcodeScanner: BarcodeScanner,
              private alertCtrl: AlertController
            ) 
    {
    this.api.getAllAlertsCount();
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.vehicleNum  = Array.from(new Array(this.num), (x,i) => i);
  }

  scanBarcode(i){
    let options = {
      prompt : "Scan the Barcode. ", // Android Only
      orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
    }
    this.barcodeScanner.scan(options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.vin[i] = barcodeData.text
      this.getVehicleInfo(barcodeData.text, i);
     }).catch(err => {
         console.log('Error', err);
     });
  }

  getVehicleInfo(vin, i){
    let vehicle_data = ''
    this.api.getVehicleProfile(vin)
      .then(data => {
        console.log(data)
        if( data['vehicleCurrentProfileInfo']){
          vehicle_data = data['vehicleCurrentProfileInfo'];
          this.make[i] = vehicle_data['make'];
          this.model[i] = vehicle_data['model_trim'];
          this.year[i] = vehicle_data['year'];
        }else{
          this.make[i] = '';
          this.model[i] = '';
          this.year[i] = '';
        }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateCustomerPage');
  }

  addVehicleBox(){
    this.num = this.num+1
    this.vehicleNum.push(this.num)
  }


  showConfirmAlert(selectedBox) {
    console.log(selectedBox)
    let alert = this.alertCtrl.create({
        title: 'Confirm delete Vehicle?',
        // message: 'Are you sure you want to permanently delete this Customer?',
        buttons: [
            {
                text: 'No',
                handler: () => {
                    console.log('Cancel clicked');
                }
            },
            {
                text: 'Yes',
                handler: () => {
                   this.removeVehicleBox(selectedBox)
                }
            }
        ]
    })
    alert.present();
  }
  
  removeVehicleBox(selectedBox){
    let index = this.vehicleNum .indexOf(selectedBox);
    if (index !== -1) this.vehicleNum .splice(index, 1);
    this.func.presentToast('Vehicle Removed Successfully', 2000, 'bottom');
    console.log(this.vehicleNum)
  }



  capture_dl_front(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.customer.dl_front = normalizeURL(imageData);
        this.upload_dl_front(imageData);
      }, error => {
        // this.func.showAlert('Error',JSON.stringify(error));
        this.func.presentToast(JSON.stringify(error));
      });
  }
  upload_dl_front(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile_dl_front(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }
  private readFile_dl_front(file: any) {
    const reader = new FileReader();

    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.dl_front_imageName = data;
        })
      };
    }else{
      reader.onload= () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.dl_front_imageName = data;
        })
      };
    }

    
    reader.readAsArrayBuffer(file);
  }

   
  capture_dl_back(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.customer.dl_back = normalizeURL(imageData);
        this.upload_dl_back(imageData);
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }
  upload_dl_back(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile_dl_back(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }
  private readFile_dl_back(file: any) {
    const reader = new FileReader();

    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.dl_back_imageName = data;
        })
      };
    }else{
      reader.onload = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.dl_back_imageName = data;
        })
      };
    }

    
    reader.readAsArrayBuffer(file);
  }

   
  capture_cc_front(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.customer.cc_front = normalizeURL(imageData);
        this.upload_cc_front(imageData);
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }
  upload_cc_front(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile_cc_front(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }
  private readFile_cc_front(file: any) {
    const reader = new FileReader();

    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.cc_front_imageName = data;
        })
      };
    }else{
      reader.onload = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.cc_front_imageName = data;
        })
      };
    }

    
    reader.readAsArrayBuffer(file);
  }

   
  capture_cc_back(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.customer.cc_back = normalizeURL(imageData);
        this.upload_cc_back(imageData);
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }
  upload_cc_back(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile_cc_back(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }
  private readFile_cc_back(file: any) {
    const reader = new FileReader();

    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.cc_back_imageName = data;
        })
      };
    }else{
      reader.onload = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.cc_back_imageName = data;
        })
      };
    }

    
    reader.readAsArrayBuffer(file);
  }

   
  capture_ic_front(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.customer.ic_front = normalizeURL(imageData);
        this.upload_ic_front(imageData);
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }
  upload_ic_front(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile_ic_front(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }
  private readFile_ic_front(file: any) {
    const reader = new FileReader();

    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.ic_front_imageName = data;
        })
      };
    }else{
      reader.onload = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.ic_front_imageName = data;
        })
      };
    }

    
    reader.readAsArrayBuffer(file);
  }

   
  capture_ic_back(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.customer.ic_back = normalizeURL(imageData);
        this.upload_ic_back(imageData);
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }
  upload_ic_back(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile_ic_back(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }
  private readFile_ic_back(file: any) {
    const reader = new FileReader();

    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.ic_back_imageName = data;
        })
      };
    }else{
      reader.onload = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        const img = new FormData();
        img.append('image', imgBlob, file.name)
        this.api.uploadCardImage(img).then(data=>{
          this.ic_back_imageName = data;
        })
      };
    }
    
    reader.readAsArrayBuffer(file);
  }


  saveCustomer(formData){
    let customer_id = '';
    this.func.presentLoading("Adding new Customer...");
    this.api.createCustomer(this.getFormUrlEncoded(formData))
    .then(data => {
      customer_id = JSON.stringify(data);
    }).then(() => {
      this.uploadCardImages(customer_id);
      // this.navCtrl.push('CustomerListPage')
    });
  }

  uploadCardImages(customer_id){
    console.log(customer_id, this.dl_front_imageName)
    let images:FormData = new FormData();
    images.append('customer_id', customer_id);
    images.append('dl_front', this.dl_front_imageName);
    images.append('dl_back', this.dl_back_imageName);
    images.append('cc_front', this.cc_front_imageName );
    images.append('cc_back', this.cc_back_imageName );
    images.append('ic_front', this.ic_front_imageName );
    images.append('ic_back', this.ic_back_imageName );
    this.api.uploadCardImageNames(images)
      .then(data => {
        this.func.dismissLoading();
        this.navCtrl.push('CustomerListPage', {}, this.animationsOptions)
        this.func.presentToast("New Customer Added Successfully", 2000, "bottom");
      })
  }

  getFormUrlEncoded(toConvert) {
		const formBody = [];
		for (const property in toConvert) {
			const encodedKey = encodeURIComponent(property);
			const encodedValue = encodeURIComponent(toConvert[property]);
			formBody.push(encodedKey + '=' + encodedValue);
		}
		return formBody.join('&');
  }

  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back(){
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
