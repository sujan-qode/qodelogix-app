import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, Platform, AlertController } from 'ionic-angular';
// import { HomePage } from '../home/home';
import { env } from '../../environment';
import { ApiProvider } from '../../providers/api/api';
import { customFunctions } from '../../providers/functions';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { RentalManagerVehicleProfilePage } from '../rental-manager-vehicle-profile/rental-manager-vehicle-profile';

/**
 * Generated class for the RentalManagerSelectVehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-select-vehicle',
  templateUrl: 'rental-manager-select-vehicle.html',
})
export class RentalManagerSelectVehiclePage {

  @ViewChild('mySlider') slider: Slides;
  toggleTabs = "reserved";
  slides = [
    {
      id: "reserved"
    },
    {
      id: "allVehicles"
    }
  ];
  
  isRootPage = false;
  alertsCount;
  
  baseUrl = env.base;

  customerVehiclesData;
  customerRentalInfo;

  filterData;
  vehiclesData;
  tempData;
  isSearchbarEmpty: boolean = true;

  isTabletOrIpad: boolean;
  interface:string="popover";

  shoAdvanceMenu = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  vehicleType;
  vehicleColor;
  vehicleModels;

  vehicle_type = "";
  model_type = "";
  vehicle_color = "";
  trimAndFeatures = '';
  location = '';

  customer;

  isAdvanceSearchDisabled = false;

  operation;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private platform: Platform,
              private api: ApiProvider,
              private alertCtrl: AlertController,
              private func: customFunctions ) {
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if(!this.isTabletOrIpad){
      this.interface='action-sheet';
    }
    this.customer = this.navParams.get('customer');
    console.log(this.customer)
    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)

    this.getAvailableLoaners();
    this.getAllCOlors();
    this.getAllModels();
    this.getVehicleTypes();

    if(this.operation == "rental_manager_edit_loaner"){
      this.displayAssuCustomerVehicleList(this.customer.token, this.customer.vin_number);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentalManagerSelectVehiclePage');
  }

  ionViewDidEnter() {
    if(this.operation == "rental_manager_edit_loaner"){
      this.slider.lockSwipes(true);
    }
  }

  onSegmentChanged(segmentButton) {
    this.slider.lockSwipes(false);
    console.log("Segment changed to", segmentButton.value);
    const selectedIndex = this.slides.findIndex((slide) => {
      return slide.id === segmentButton.value;
    });
    this.slider.slideTo(selectedIndex);
    this.slider.lockSwipes(true);
  }


  getVehicleTypes(){
    this.api.getvehicleTypes()
      .then( data => {
        console.log(data)
        this.vehicleType = data;
      })
      .then(()=>{
        this.vehicleType = this.vehicleType.filter((type) => {
          return (type.name.toLowerCase() == "loaner")
        })
      })
      .then(()=>{
        this.vehicle_type = this.vehicleType[0].id;
      })
      .catch(err => {
        console.log(err)
      })
  }

  getAllModels() {
    this.api.getAllvehiclemodels()
      .then(data => {
        // console.log(data)
        this.vehicleModels = data;
      })
  }

  getAllCOlors() {
    this.api.getAllColorList()
      .then(data => {
        // console.log(data)
        this.vehicleColor = data;
      })
  }

  getAvailableLoaners() {
    this.func.presentLoading();
    this.api.getAvailableLoaners()
      .then(data => {
        console.log(data)
        this.vehiclesData = data;
        this.filterData = data;
        this.tempData = data;
      }).then(()=>{
        this.func.dismissLoading();
      })
  }

  displayAssuCustomerVehicleList(token, vin_number){
    this.api.displayAssuCustomerVehicleList(token, vin_number)
      .then(data => {
        this.customerVehiclesData = [];

        Object.keys(data).forEach(key=> {
          let profile = data[key]['profile'][0];
          if(profile){
            this.customerVehiclesData.push(profile)   
          }
        });
        this.customerRentalInfo = data[0]['loaner_in_out_info']
        localStorage.setItem('rentalInfo', JSON.stringify(this.customerRentalInfo));
        localStorage.setItem('signature', env.base + data[0]['signature']);
      })
  }

  toggleAdvanceMenu(){
    this.shoAdvanceMenu = !this.shoAdvanceMenu;
    const inputs: any = document.getElementById("searchbar").getElementsByTagName("INPUT");
    inputs[0].disabled=this.shoAdvanceMenu;
    this.filterData = this.tempData;
    if(!this.shoAdvanceMenu){
      setTimeout(()=>{
        // this.vehicle_type = "";
        this.model_type = "";
        this.vehicle_color = "";
        this.trimAndFeatures = '';
        this.location = '';
        // this.isAdvanceSearchDisabled = !this.isAdvanceSearchDisabled;
      },500)
    }
  }

  checkAdvanceSearchButton(){
    if(this.vehicle_type == "" &&
      this.model_type == "" &&
      this.vehicle_color == "" &&
      this.trimAndFeatures == '' &&
      this.location ==''){
        this.isAdvanceSearchDisabled = true
      }
    else{
      this.isAdvanceSearchDisabled = false;
    }
  }

  checkAdvanceSearchButtonForTrim(){
    if(this.trimAndFeatures == ''){
        this.isAdvanceSearchDisabled = true
      }
    else{
      this.isAdvanceSearchDisabled = false;
    }
    console.log(this.trimAndFeatures)
  }

  doAdvanceSearch(){
    console.log( "type"+this.vehicle_type+" model"+this.model_type+" model_trim"+this.trimAndFeatures+" color"+this.vehicle_color+" location"+this.location)
    this.func.presentLoading();
    this.api.advanceSearch(this.vehicle_type, this.model_type, this.trimAndFeatures, this.vehicle_color, this.location)
      .then(data =>{
        console.log(data)
        this.filterData = data;
        if(this.filterData.length == 0){
          this.func.showAlert('Notice', 'No Vehicle Available')
        } 
      }).then(()=>{
        this.func.dismissLoading();
        if(this.operation == "rental_manager_edit_loaner"){
          this.slider.slideTo(1);
          this.toggleTabs = "allVehicles"
        }
      })
      .catch(err=>{
        console.log(err);
        this.func.presentToast('Something Went Wrong.');
        this.filterData = this.tempData;
      })
  }


  getItems(ev: any) {
    let val = ev.target.value;
    console.log(val)
    this.filterByVin(val)
  }

  filterByVin(val) {
    console.log(val)
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.vehiclesData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1));
      });
    }
    else {
      this.getAvailableLoaners();
      this.isSearchbarEmpty = true;
    }
  }

  presentConfirm(e, i, v) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Replace',
      message: 'Do you want to replace the current assigned loaner <br><b>'+this.customer.vin_number+'</b><br> with <br><b>'+v.vin_number+'</b>?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Replace',
          handler: () => {
            this.gotoVehicleProfileWithAnimation(e, i, v);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }


  gotoVehicleProfileWithAnimation(e, i, v){
    console.log(v, this.customer);
    localStorage.setItem('new_vin_number', v.vin_number);
    this.navCtrl.push('RentalManagerVehicleProfilePage', {operationType: this.operation, vehicle: v, customer: this.customer}, this.animationsOptions);
  }

  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back() {
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
