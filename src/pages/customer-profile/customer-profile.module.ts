import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerProfilePage } from './customer-profile';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    CustomerProfilePage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(CustomerProfilePage),
  ],
})
export class CustomerProfilePageModule {}
