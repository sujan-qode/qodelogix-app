import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DamageReportingPage } from './damage-reporting';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    DamageReportingPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(DamageReportingPage),
  ],
})
export class DamageReportingPageModule {}
