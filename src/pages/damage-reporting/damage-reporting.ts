import { env } from './../../environment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ViewController, ModalController, Platform } from 'ionic-angular';
// import { AccessoriesChecklistPage } from '../accessories-checklist/accessories-checklist';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
import { ApiProvider } from '../../providers/api/api';
import { customFunctions } from '../../providers/functions';

// import * as $ from 'jquery';

/**
 * Generated class for the DamageReportingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-damage-reporting',
  templateUrl: 'damage-reporting.html',
})
export class DamageReportingPage {

  extColorList;
  intColorList;

  exteriorColor;
  interiorColor;

  vinDetail = null;
  baseUrl = env.base;

  alertsCount;
  vehicle;
  checkin_type;
  damageReport = {
    check_engine: -1,
    check_engine_note: ' ',
    tire_pressure: -1,
    tire_pressure_note: ' ',
    airbag: -1,
    airbag_note: ' ',
    dsc: -1,
    dsc_note: ' ',
    left_front: -1,
    left_front_tire_thread: null,
    left_front_tire_pressure: null,
    right_front: -1,
    right_front_tire_thread: null,
    right_front_tire_pressure: null,
    left_rear: -1,
    left_rear_tire_thread: null,
    left_rear_tire_pressure: null,
    right_rear: -1,
    right_rear_tire_thread: null,
    right_rear_tire_pressure: null,
    ext_body_note: ' ',
    ext_body: -1,
    windshield_note: ' ',
    windshield: -1,
    wiper_note: ' ',
    wiper: -1,
    light_note: ' ',
    light: -1,
    asset_id: ' ',
    vin_number: ' '
  };
  validaDamageReport = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }


  isTabletOrIpad: boolean;
  interface: string = "popover";

  user_id;
  dealer_id;

  interval;
  
  constructor(private platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public viewCtrl: ViewController,
    public api: ApiProvider,
    public func: customFunctions,
    public modalCtrl: ModalController) {
    this.api.getAllAlertsCount();
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if (!this.isTabletOrIpad) {
      this.interface = 'action-sheet';
    }
    this.user_id = localStorage.getItem('auth_id');
    this.dealer_id = localStorage.getItem('dealer_id');
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.vehicle = this.navParams.get('vehicle');
    this.exteriorColor = (this.vehicle.ext_color_code) ? this.vehicle.ext_color_code : 'gray'
    this.interiorColor = (this.vehicle.int_color_code) ? this.vehicle.int_color_code : 'gray'
    this.checkin_type = this.navParams.get('checkin_type');
    console.log("checkin type :" + this.checkin_type)

    console.log(this.vehicle)
    this.getVehicleDamage(this.vehicle.vin_number, this.checkin_type);

     this.interval = setInterval(() => {
      this.hasEmptyField();
    }, 2000);

    this.getColorList();
  }

  ionViewDidLeave(){
    clearInterval(this.interval);
  }


  getColorList() {
    this.api.getColorList()
      .then(data => {
        this.extColorList = data['extList'];
        this.intColorList = data['intList'];
        this.extColorList.unshift({ 'color_code': "gray", 'color_name': "Select Color" })
        this.intColorList.unshift({ 'color_code': "gray", 'color_name': "Select Color" })
      })
  }

  onColorChange(color, type) {
    let c = this.getColor(color, type)
    console.log(c, this.vehicle)
    if (c.length > 0 && c[0].id) {
      this.api.setColorList(c[0], this.vehicle.vin_number)
        .then(data => {
          // console.log(data);
          // (data == 1) ? this.func.presentToast("Color Updated Successfully") : this.func.presentToast("Please Select a Color")
          if(data){
            let color = c[0];
            if(type==0){
              this.vehicle.int_color_code = color.color_code;
              this.vehicle.int_color_name = color.color_name;
            }
            if(type==1){
              this.vehicle.ext_color_code = color.color_code;
              this.vehicle.ext_color_name = color.color_name;
            }
            this.func.presentToast("Color Updated Successfully")
          }
        })
        .catch(() => {
          // console.log('catch')
          this.func.presentToast("Please Select a Color")
        })
    } else {
      // console.log('else')
      this.func.presentToast("Please Select a Color")
    }
  }

  getColor(c, type) {
    if (type == 1) {
      return this.extColorList.filter((color) => {
        return color.color_code === c;
      })
    }
    else {
      return this.intColorList.filter((color) => {
        return color.color_code === c;
      })
    }
  }

  // reject a-z string in a input field
  checkInput(ev: any) {
    console.log(ev.key)
    let input = ev.key;
    if (/^[a-zA-Z]*$/.test(input)) {
      return false;
    }
  }

  getVehicleDamage(vin_number, checkin_type) {
    console.log(checkin_type)
    this.api.getVehicleDamage(vin_number, checkin_type)
      .then(data => {
        // console.log(data)
        if (!this.func.isEmptyObject(data)) {
          this.damageReport = data[0];
          console.log('vehicleDamage from api is not empty')
        }
      })
  }


  gotoAccessoriesChecklist(formValue) {

    console.log(formValue)
    this.api.createVehicleDamage(this.getFormUrlEncoded(formValue))
      .then(data => {
        console.log(data)
      })
    this.navCtrl.push('AccessoriesChecklistPage', {
      vehicle: this.vehicle,
      checkin_type: this.checkin_type
    },
      this.animationsOptions)
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }

  hasEmptyField() {
    if (this.damageReport.left_front_tire_pressure == null || this.damageReport.left_front_tire_thread == null || this.damageReport.right_front_tire_pressure == null ||
      this.damageReport.right_front_tire_thread == null || this.damageReport.right_rear_tire_pressure == null || this.damageReport.right_rear_tire_thread == null ||
      this.damageReport.left_rear_tire_pressure == null || this.damageReport.left_rear_tire_thread == null || this.damageReport.left_front < 0 || this.damageReport.right_front < 0 ||
      this.damageReport.left_rear < 0 || this.damageReport.right_rear < 0 || this.damageReport.check_engine < 0 || this.damageReport.tire_pressure < 0 || this.damageReport.airbag < 0
      || this.damageReport.dsc < 0 || this.damageReport.ext_body < 0 || this.damageReport.windshield < 0 || this.damageReport.wiper < 0 || this.damageReport.light < 0
    ) {
      this.validaDamageReport = false;
    } else {
      this.validaDamageReport = true;
    }
  }



  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back() {
    this.navCtrl.pop()
  }

}
