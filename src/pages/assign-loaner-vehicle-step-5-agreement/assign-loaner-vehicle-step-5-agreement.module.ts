import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignLoanerVehicleStep_5AgreementPage } from './assign-loaner-vehicle-step-5-agreement';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AssignLoanerVehicleStep_5AgreementPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AssignLoanerVehicleStep_5AgreementPage),
  ],
})
export class AssignLoanerVehicleStep_5AgreementPageModule {}
