// import { LoanerAgreementPage } from './../loaner-agreement/loaner-agreement';
import { customFunctions } from './../../providers/functions';
import { ApiProvider } from './../../providers/api/api';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';

/**
 * Generated class for the AssignLoanerVehicleStep_5AgreementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assign-loaner-vehicle-step-5-agreement',
  templateUrl: 'assign-loaner-vehicle-step-5-agreement.html',
})
export class AssignLoanerVehicleStep_5AgreementPage {

  // Canvas stuff
  @ViewChild('imageCanvas') canvas: any;
  canvasElement: any;
  saveX: number;
  saveY: number;
  selectedColor = '#9e2956';

  vehicle;
  customer;

  accept_tos = false;
  signed = false;

  alertsCount;
  checkin_type;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  agreement;
  formData:FormData = new FormData();

  
  isTabletOrIpad:boolean;
  
  constructor(private func: customFunctions ,private api: ApiProvider ,private platform:Platform, public navCtrl: NavController, public navParams: NavParams) {
    this.api.getAllAlertsCount();
    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.getLoanerAgreement();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignLoanerVehicleStep_5AgreementPage');
    // Set the Canvas Element and its size
    this.canvasElement = this.canvas.nativeElement;
    this.canvasElement.width = this.platform.width() + '';
    this.canvasElement.height = 200;

    this.preventScrollOnDrawing();
  }

  preventScrollOnDrawing(){
    let self=this
      document.body.addEventListener("touchstart", function (e) {
        if (e.target == self.canvasElement) {
            e.preventDefault();
        }
    }, { passive: false });
    document.body.addEventListener("touchend", function (e) {
        if (e.target == self.canvasElement) {
            e.preventDefault();
        }
    }, { passive: false });
    document.body.addEventListener("touchmove", function (e) {
        if (e.target == self.canvasElement) {
            e.preventDefault();
        }
    }, { passive: false });
  }

  getLoanerAgreement(){
    this.api.getLoanerAgreement()
      .then(data => {
        console.log(data[0])
        this.agreement = data[0].rental_agreement;
      })
      .catch(err => {
        console.log(err)
      })
  }

  assignVehicle(){
    this.func.presentLoading();
    this.getCanvasImage()
    this.formData.append('vin_number', this.vehicle.vin_number)
    this.formData.append('customer_id', this.customer.id)
    this.formData.append('dealer_id', localStorage.getItem('dealer_id'))
    this.api.setLoanerAssigned(this.formData)
      .then(data => {
        console.log(data)
        this.func.dismissLoading();
        // this.func.presentToast("Loaner Assigned Successfully.", 3000, "bottom");
        this.func.presentToast(data['message'], 3000, "bottom");
        this.navCtrl.push('LoanerAgreementPage', {vehicle: this.vehicle, customer: this.customer, operationType: 'assign'}, this.animationsOptions);
      })
      .catch(err => {
        console.log(err)
        this.func.dismissLoading();
        this.func.presentToast("Something went wrong. Please try again!", 3000, "bottom");
      })
  }

  clearCanvas(){
    this.signed = false;
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
  }
  
  getCanvasImage() {
  var dataUrl = this.canvasElement.toDataURL();
  let ctx = this.canvasElement.getContext('2d');
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
  // let name = new Date().getTime() + '.png';
  var data = dataUrl.split(',')[1];
  let blob = this.b64toBlob(data, 'image/png');

  this.formData.append('signature', blob, "signature.png")
  
  // return blob;
}
 
b64toBlob(b64Data, contentType) {
  contentType = contentType || '';
  var sliceSize = 512;
  var byteCharacters = atob(b64Data);
  var byteArrays = [];
  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);
    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }
  var blob = new Blob(byteArrays, { type: contentType });
  return blob;
}



startDrawing(ev) {
  console.log('started drawing');
  var canvasPosition = this.canvasElement.getBoundingClientRect();
  this.saveX = ev.touches[0].pageX - canvasPosition.x;
  this.saveY = ev.touches[0].pageY - canvasPosition.y;
}
 
moved(ev) {
  console.log('drawing');
  this.signed = true;
  var canvasPosition = this.canvasElement.getBoundingClientRect();
  let ctx = this.canvasElement.getContext('2d');
  let currentX = ev.touches[0].pageX - canvasPosition.x;
  let currentY = ev.touches[0].pageY - canvasPosition.y;
  ctx.lineJoin = 'round';
  ctx.strokeStyle = this.selectedColor;
  ctx.lineWidth = 5;
  ctx.beginPath();
  ctx.moveTo(this.saveX, this.saveY);
  ctx.lineTo(currentX, currentY);
  ctx.closePath();
  ctx.stroke();
  this.saveX = currentX;
  this.saveY = currentY;
}


  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }
 
  back(){
    this.navCtrl.pop()
  }

}
