import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  public lottieConfig: Object;

  splashWait:number;

  constructor(public viewCtrl: ViewController ,public navCtrl: NavController, public navParams: NavParams) {
    this.lottieConfig = {
      path: 'assets/splash/data.json',
      renderer: 'canvas',
      autoplay: true,
      loop: false
  };
      this.splashWait = 4000;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SplashPage');
  }

  ionViewDidEnter() {

    setTimeout(() => {
      if(localStorage.getItem('auth_id')){
      this.navCtrl.setRoot('HomePage');
    }else{
      this.navCtrl.setRoot('LoginPage');
    }
      // this.viewCtrl.dismiss();
    }, this.splashWait);

  }

}
