import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SplashPage } from './splash';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    SplashPage,
  ],
  imports: [
    IonicPageModule.forChild(SplashPage),
    LottieAnimationViewModule
  ]
})
export class SplashPageModule {}
