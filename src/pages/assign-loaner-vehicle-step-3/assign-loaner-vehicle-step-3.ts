import { ApiProvider } from './../../providers/api/api';
// import { AlertsPage } from './../alerts/alerts';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';

/**
 * Generated class for the AssignLoanerVehicleStep_3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assign-loaner-vehicle-step-3',
  templateUrl: 'assign-loaner-vehicle-step-3.html',
})
export class AssignLoanerVehicleStep_3Page {

   vehicle;
   alertsCount;
   
 
   animationsOptions = {
     animation: 'ios-transition',
     duration: 1000
   }
 
   
   isTabletOrIpad:boolean;
   
   constructor(private api:ApiProvider ,private platform:Platform, public navCtrl: NavController, public navParams: NavParams) {
     this.api.getAllAlertsCount();
     this.vehicle = this.navParams.get('vehicle');
     this.alertsCount = localStorage.getItem('allAlertsCount');
     this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignLoanerVehicleStep_3Page');
  }

  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back(){
    this.navCtrl.pop()
  }

}
