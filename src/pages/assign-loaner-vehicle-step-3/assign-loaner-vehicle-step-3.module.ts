import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignLoanerVehicleStep_3Page } from './assign-loaner-vehicle-step-3';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AssignLoanerVehicleStep_3Page,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AssignLoanerVehicleStep_3Page),
  ],
})
export class AssignLoanerVehicleStep_3PageModule {}
