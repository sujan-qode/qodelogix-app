import { BarcodeScanner } from '@ionic-native/barcode-scanner';
// import { DamageReportingPage } from './../damage-reporting/damage-reporting';
import { env } from './../../environment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { customFunctions } from '../../providers/functions';


@IonicPage()
@Component({
  selector: 'page-delivery-vehicle',
  templateUrl: 'delivery-vehicle.html',
})
export class DeliveryVehiclePage {

  baseUrl = env.base;
  alertsCount;
  vehicle_status;
  dropdown_text = "Vehicle Status";
  filterData;
  allVehicles;
  isSearchbarEmpty:boolean= true;
  searchItem;
  scannedVin;
  isRootPage = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  temp;

  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private _api: ApiProvider,
              private barcodeScanner: BarcodeScanner,
              private func: customFunctions,
              private api: ApiProvider
            ) {
    this.api.getAllAlertsCount();
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.func.presentLoading('Loading...')
    // this.getAllVehicles();
    this.getDeliveryVehicles();
  }

  scanBarcode(){
    let options = {
      prompt : "Scan the Barcode. ", // Android Only
      orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
    }
    this.barcodeScanner.scan(options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.scannedVin = barcodeData.text;
      this.gotoDamageReportingPageUsingVin(this.scannedVin);
     }).catch(err => {
         this.func.presentToast(err, 2000, 'bottom')
     });
  }
  
  gotoAlertsPage(){
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }

  getDeliveryVehicles() {
    this._api.getDeliveryVehicles()
    .then(data => {
      console.log(data)
      this.temp = data;
      this.filterData = data;
      this.func.dismissLoading();
    });
  }

  getAllVehicles() {
    this._api.getAllVehicles()
    .then(data => {
      console.log(data)
      this.allVehicles = data
    });
  }

  getItems(ev: any) {
    let val = ev.target.value;
    this.filterByVin(val)
  }
  
  filterByVin(val) {
    console.log(val)
    if (val && val.trim() != '') {
      console.log('in')
      this.isSearchbarEmpty = false;
      this.filterData = this.temp.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
        // return ((vehicle.vin_number.toLowerCase().indexOf(val.toLowerCase()) > -1) );
      });
    }
    else{
      this.getDeliveryVehicles();
      this.isSearchbarEmpty = true;
    }
  }

  gotoDamageReportingPageUsingVin(vin:any, checkin_type = 3){
    this.func.presentLoading("Please Wait");
      
    /*
      Verify if the scanned vin has the length of range 15-18
    */
    if(vin.length > 18 || vin.length< 15){
      this.func.presentToast('Invalid Vin Number. Please try again');
      this.func.dismissLoading();
    }else{
      this.api.decodeVin(vin)
        .then(data =>{
          let checkinData = vin+"&year="+data['year']+"&make="+data['make']+"&model_trim="+data['model_trim']+'&model_image='+data['model_image'];
          var vehicle = data;
          console.log(data, vehicle, data['year'])

          this.api.checkIn(checkinData, checkin_type)
            .then(data => {
              console.log(data)
              this.func.dismissLoading();
              // this.navCtrl.push('DamageReportingPage', {vehicle: vehicle, checkin_type:checkin_type}, this.animationsOptions);
            }).catch(err => {
              this.func.dismissLoading();
              this.func.presentToast('Error Occured. Please Try Again.')
            })
        }).catch(err => {
          this.func.dismissLoading();
          this.func.presentToast('Error Occured. Please Try Again.')
        })
      // this.navCtrl.push('DamageReportingPage', {vehicle: vehicleVinOnly, checkin_type:checkin_type}, this.animationsOptions);
    }
    // let tempvehicle, vehicle;
    // console.log(vin, checkin_type)
    // /*
    //   Verify if the scanned vin is in the list of all vehicles
    // */
    // tempvehicle = this.allVehicles.filter((vehicle) => {
    //   console.log(vin, vehicle)
    //   return (vehicle.vin_number == vin);
    // });
    // vehicle = tempvehicle[0]
    // console.log(vehicle)

    // if(vehicle == undefined || vehicle == null){
    //   // this.func.presentToast('Invalid Vin Number. Please try again')
    // }else{
    //   this.navCtrl.push('DamageReportingPage', {vehicle: vehicle, checkin_type:checkin_type}, this.animationsOptions);
    // }   
  }

  gotoDamageReportingPage(vehicle, checkin_type = 3){
    this.navCtrl.push('DamageReportingPage', {vehicle: vehicle, checkin_type:checkin_type}, this.animationsOptions);
  }

  back(){
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }
  
}
