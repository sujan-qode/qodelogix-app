import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeliveryVehiclePage } from './delivery-vehicle';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    DeliveryVehiclePage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(DeliveryVehiclePage),
  ],
})
export class DeliveryVehiclePageModule {}
