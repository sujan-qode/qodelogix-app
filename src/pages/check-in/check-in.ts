import { env } from './../../environment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { VoltPipe } from '../../pipes/volt/volt';
// import { DamageReportingPage } from '../damage-reporting/damage-reporting';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';

/**
 * Generated class for the CheckInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-check-in',
  templateUrl: 'check-in.html'
})
export class CheckInPage {

  alertsCount;
  filterData;
  alertsData;
  isSearchbarEmpty:boolean= true;
  searchItem;
  selectedVehicleIndex = -1;
  vehicle;
  isRootPage = false;

  checkin_status = "-1";
   
  loading = this.loadingCtrl.create({
    content: 'Loading Vehicle List...'
  });
  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  baseUrl = env.base;
  vinDetail = [];
  vehicleVinDetail;

  vin;

  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform, public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController, private _api: ApiProvider) {
    this._api.getAllAlertsCount();
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if(this.navParams.get('vehicle')){
      this.vehicle = this.navParams.get('vehicle');
      this._api.decodeVin(this.vehicle.vin_number)
      .then(data => {
        this.vehicleVinDetail = data;
      })
    }else{
      this.vehicle = 0;
    }
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.showLoader();
    this.getAllVehicles();
  }

  showLoader(){
    this.loading.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehicleListPage');
  }

  gotoAlertsPage(){
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }

  getAllVehicles() {
    this._api.getAllVehicles()
    .then(data => {
      console.log(data)
      this.loading.dismiss();
      this.alertsData = data;
      this.filterData = data;
    }).then(()=> {    });
  }

  getItems(ev: any) {
    this.vin = ev.target.value;
    console.log(this.checkin_status)
    this.doFilter();
  }

  OnCheckinStatusChange(){
    console.log(this.vin, this.checkin_status)
    this.doFilter();
  }

  doFilter(){
    if(this.checkin_status == '0'){
      this.filterByStatus('0')
    }
    else if(this.checkin_status == '1'){
      this.filterByStatus('1')
    }
    else{
      this.filterByVin()
    }
  }

  filterByVin(){
    let val = this.vin;
    console.log(val)
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.alertsData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " "+vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1) );
        // return (vehicle.vin_number.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
    else{
      this.getAllVehicles();
      this.isSearchbarEmpty = true;
    }
  }

  filterByStatus(ev: any){
    let val = this.checkin_status;
    let vin = this.vin;
    
    console.log(val, vin)
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.alertsData.filter((vehicle) => {
        if(vin !== undefined){
          let makeModelVin = vehicle.make + " "+ vehicle.model_trim+ " "+ vehicle.vin_number+" "+vehicle.stock_number;
          return ((vehicle.is_checked_in == val) && (makeModelVin.toLowerCase().indexOf(vin.toLowerCase()) > -1));
        }else{
          console.log(vehicle)
          return ((vehicle.is_checked_in == val));
        }
      });
    }
    else{
      this.getAllVehicles();
      this.isSearchbarEmpty = true;
    }
  }

  

  displayOption(index, vehicle){
    console.log(vehicle)
    if(vehicle.is_checked_in == 1){
      this.navCtrl.push('DamageReportingPage', {vehicle: vehicle, checkin_type:vehicle.check_in_type}, this.animationsOptions);
    }else{
      this.selectedVehicleIndex = (this.selectedVehicleIndex == -1) ? index : -1;
    }
  }

  gotoDamageReportingPage(vehicle, checkin_type){
    this.navCtrl.push('DamageReportingPage', {vehicle: vehicle, checkin_type:checkin_type}, this.animationsOptions);
  }

  back(){
    // this.isRootPage == false ? this.navCtrl.push('CheckInPage') : this.navCtrl.setRoot('HomePage');
    this.navCtrl.setRoot('HomePage')
  }

}
