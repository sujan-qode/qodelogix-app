import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { RentalManagerSelectCustomerPage } from '../rental-manager-select-customer/rental-manager-select-customer';
// import { HomePage } from '../home/home';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { RentalManagerReturnLoanerPage } from '../rental-manager-return-loaner/rental-manager-return-loaner';

/**
 * Generated class for the RentalManagerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager',
  templateUrl: 'rental-manager.html',
})
export class RentalManagerPage {

  isRootPage = false;
  alertsCount; 

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  constructor(private api:ApiProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.api.getAllAlertsCount();
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentalManagerPage');
  }

  navigateAssignLoanerPage(){
    this.navCtrl.push('RentalManagerSelectCustomerPage', {operationType: "rental_manager_assign_loaner"}, this.animationsOptions)
  }

  navigateReturnLoanerPage(){
    this.navCtrl.push('RentalManagerReturnLoanerPage', {operationType: "rental_manager_return_loaner"}, this.animationsOptions)
  }

  navigateEditLoanerPage(){
    this.navCtrl.push('RentalManagerSelectCustomerPage', {operationType: "rental_manager_edit_loaner"}, this.animationsOptions)
  }

  
  gotoAlertsPage() {
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }

  back() {
    // if(this.navCtrl.canGoBack()){
    //   this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    // }
    // else{
      this.navCtrl.setRoot('HomePage')
    // }
  }

}
