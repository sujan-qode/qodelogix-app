import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerPage } from './rental-manager';

@NgModule({
  declarations: [
    RentalManagerPage,
  ],
  imports: [
    IonicPageModule.forChild(RentalManagerPage),
  ],
})
export class RentalManagerPageModule {}
