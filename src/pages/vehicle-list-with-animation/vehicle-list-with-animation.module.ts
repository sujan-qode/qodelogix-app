import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleListWithAnimationPage } from './vehicle-list-with-animation';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    VehicleListWithAnimationPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(VehicleListWithAnimationPage),
  ],
})
export class VehicleListWithAnimationPageModule {}
