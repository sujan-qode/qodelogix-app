import { env } from './../../environment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { customFunctions } from '../../providers/functions';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
// import { VehicleProfileWithAnimationPage } from '../vehicle-profile-with-animation/vehicle-profile-with-animation';
// import { VehicleProfileWithAnimationNewPage } from '../vehicle-profile-with-animation-new/vehicle-profile-with-animation-new';
/**
 * Generated class for the VehicleListWithAnimationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehicle-list-with-animation',
  templateUrl: 'vehicle-list-with-animation.html',
})
export class VehicleListWithAnimationPage {

  baseUrl = env.base;
  alertsCount;

  filterData;
  vehiclesData;
  tempData;
  isSearchbarEmpty: boolean = true;
  isRootPage = false;

  isTabletOrIpad: boolean;
  interface:string="popover";

  shoAdvanceMenu = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  vehicleType;
  vehicleColor;
  vehicleModels;

  vehicle_type = "";
  model_type = "";
  vehicle_color = "";
  trimAndFeatures = '';
  location = '';

  isAdvanceSearchDisabled = true;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private api: ApiProvider, 
              private platform: Platform, 
              private func:customFunctions,
              private nativePageTransitions: NativePageTransitions
  ) {
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if(!this.isTabletOrIpad){
      this.interface='action-sheet';
    }
    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.getAllVehicles();
    this.getAllCOlors();
    this.getAllModels();
    this.getVehicleTypes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehicleListWithAnimationPage');
  }

  getVehicleTypes(){
    this.api.getvehicleTypes()
      .then( data => {
        this.vehicleType = data;
      })
      .catch(err => {
        console.log(err)
      })
  }

  getAllModels() {
    this.api.getAllvehiclemodels()
      .then(data => {
        // console.log(data)
        this.vehicleModels = data;
      })
  }

  getAllCOlors() {
    this.api.getAllColorList()
      .then(data => {
        // console.log(data)
        this.vehicleColor = data;
      })
  }

  getAllVehicles() {
    this.func.presentLoading();
    this.api.getAllVehicles()
      .then(data => {
        console.log(data)
        this.vehiclesData = data;
        this.filterData = data;
        this.tempData = data;
      }).then(()=>{
        this.func.dismissLoading();
      })
  }

  toggleAdvanceMenu(){
    this.shoAdvanceMenu = !this.shoAdvanceMenu;
    const inputs: any = document.getElementById("searchbar").getElementsByTagName("INPUT");
    inputs[0].disabled=this.shoAdvanceMenu;
    this.filterData = this.tempData;
    if(!this.shoAdvanceMenu){
      setTimeout(()=>{
        this.vehicle_type = "";
        this.model_type = "";
        this.vehicle_color = "";
        this.trimAndFeatures = '';
        this.location = '';
        // this.isAdvanceSearchDisabled = !this.isAdvanceSearchDisabled;
      },500)
    }
  }

  checkAdvanceSearchButton(){
    if(this.vehicle_type == "" &&
      this.model_type == "" &&
      this.vehicle_color == "" &&
      this.trimAndFeatures == '' &&
      this.location ==''){
        this.isAdvanceSearchDisabled = true
      }
    else{
      this.isAdvanceSearchDisabled = false;
    }
  }

  checkAdvanceSearchButtonForTrim(){
    if(this.trimAndFeatures == ''){
        this.isAdvanceSearchDisabled = true
      }
    else{
      this.isAdvanceSearchDisabled = false;
    }
    console.log(this.trimAndFeatures)
  }

  doAdvanceSearch(){
    console.log( "type"+this.vehicle_type+" model"+this.model_type+" model_trim"+this.trimAndFeatures+" color"+this.vehicle_color+" location"+this.location)
    this.func.presentLoading();
    this.api.advanceSearch(this.vehicle_type, this.model_type, this.trimAndFeatures, this.vehicle_color, this.location)
      .then(data =>{
        console.log(data)
        this.filterData = data;
        if(this.filterData.length == 0){
          this.func.showAlert('Notice', 'No Vehicle Available')
        } 
      }).then(()=>{
        this.func.dismissLoading();
      })
      .catch(err=>{
        console.log(err);
        this.func.presentToast('Something Went Wrong.');
        this.filterData = this.tempData;
      })
  }


  getItems(ev: any) {
    let val = ev.target.value;
    console.log(val)
    this.filterByVin(val)
  }

  filterByVin(val) {
    console.log(val)
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.vehiclesData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1));
      });
    }
    else {
      this.getAllVehicles();
      this.isSearchbarEmpty = true;
    }
  }


  gotoVehicleProfileWithAnimation(e, i, v){
    console.log(v)
    let id = "component"+i;
    let el = document.getElementById(id);
    let topContent = document.getElementById('page-content');
    let options: NativeTransitionOptions = {
      duration: 300
     };
    this.nativePageTransitions.fade(options);
    // this.navCtrl.push('VehicleProfileWithAnimationPage', { offsetTop:el.offsetTop, topContent:topContent.offsetTop, vehicle: v}, {animate: false});
    this.navCtrl.push('VehicleProfileWithAnimationNewPage', { offsetTop:el.offsetTop, topContent:topContent.offsetTop, vehicle: v}, {animate: false});
  }

  gotoAlertsPage() {
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }


  back() {
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
