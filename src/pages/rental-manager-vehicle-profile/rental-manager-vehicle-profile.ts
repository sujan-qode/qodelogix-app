import { env } from './../../environment';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Slides } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
// import { RentalManagerLocateVehiclePage } from '../rental-manager-locate-vehicle/rental-manager-locate-vehicle';
/**
 * Generated class for the RentalManagerVehicleProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-vehicle-profile',
  templateUrl: 'rental-manager-vehicle-profile.html',
})
export class RentalManagerVehicleProfilePage {

  @ViewChild('mySlider') slider: Slides;
  toggleTabs = "features";
  slides = [
    {
      id: "features"
    },
    {
      id: "review-section"
    }
  ];

  baseUrl = env.base;
  alertsCount;

  isRootPage = false;

  vehicle;

  allVehicles;
  currentSlideIndex = 0;
  totalSlides;
  damageReport = false;
  damageInfo;
  accessories = false;

  vinDetail = null;
  vinDetailTemp = null;

  vehicleType;

  isTabletOrIpad: boolean;
  interface: string = "popover";

  shoAdvanceMenu = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  moreDetailHeight = 0;

  customer;

  operation;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private platform: Platform,
  ) {
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if (!this.isTabletOrIpad) {
      this.interface = 'action-sheet';
    }
    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');
    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)

    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    
    this.getDamageInfo(this.vehicle.vin_number, this.vehicle.check_in_type);
    this.getVehicleDamage(this.vehicle.vin_number, this.vehicle.check_in_type);
    this.getAccessoriesChecklist(this.vehicle.vin_number, this.vehicle.check_in_type);
    
    console.log(this.vehicle);
    
    this.getVehicleTypes();
    this.getAlertsCount();
    // this.getMyLocation();

  }

  getAlertsCount(){
    this.api.getAllAlertsCount()
    .then(data=>{
      if(data){
        console.log(localStorage.getItem('allAlertsCount'))
        let AllAlertsCount = localStorage.getItem('allAlertsCount');
        (AllAlertsCount) ? this.alertsCount = parseInt(AllAlertsCount) : this.alertsCount = 0;
      }
    });
  }


  ionViewDidLoad() {

  }

  ionViewWillLeave(){
    
  }

  ionViewDidEnter() {
    this.slider.lockSwipes(true);
  }


  ngAfterViewInit() {
    this.slider.autoHeight = true;
  }

  onSegmentChanged(segmentButton) {
    this.slider.lockSwipes(false);
    console.log("Segment changed to", segmentButton.value);
    const selectedIndex = this.slides.findIndex((slide) => {
      return slide.id === segmentButton.value;
    });
    this.slider.slideTo(selectedIndex);
    if (segmentButton.value == "review-section") {
      this.setSLiderContentHeight();
    }else{
      this.setSLiderContentHeightToOriginal();
    }

    this.slider.lockSwipes(true);
  }

  setSLiderContentHeight() {
    let featuresHeight = document.getElementById('features').offsetHeight;
    let reviewHeight = document.getElementById('review-section').offsetHeight;

    var style = document.createElement('style');
    if (featuresHeight < reviewHeight) {
      style.innerHTML =
      '.swiper-wrapper {' +
        'height:'+reviewHeight+'px !important;' +
      '}';
    }
    else {
      style.innerHTML =
      '.swiper-wrapper {' +
        'height:'+featuresHeight+'px !important;' +
      '}';
    }
    // Get the first script tag
    var ref = document.querySelector('script');
    // Insert our new styles before the first script tag
    ref.parentNode.insertBefore(style, ref);
  }

  setSLiderContentHeightToOriginal(){
    let featuresHeight = document.getElementById('features').offsetHeight;
    var style = document.createElement('style');
      style.innerHTML =
      '.swiper-wrapper {' +
        'height:'+featuresHeight+'px !important;' +
      '}';
    var ref = document.querySelector('script');
    ref.parentNode.insertBefore(style, ref);
  }

  getVehicleTypes() {
    this.api.getvehicleTypes()
      .then(data => {
        this.vehicleType = data;
        this.vehicleType.unshift({
          name: "Vehicle Type"
        })
        // console.log(data)
      })
      .catch(err => {
        console.log(err)
      })
  }

  getVehicleDamage(vin_number, checkin_type) {
    this.api.getVehicleDamage(vin_number, checkin_type)
      .then(data => {
        // console.log("damageReport",data[0]);
        if (data[0] != undefined) {
          this.damageReport = data[0];
        }
      })
  }

  getDamageInfo(vin_number, checkin_type) {
    this.api.getDamageInfo(vin_number, checkin_type)
      .then(data => {
        // console.log("damageInfo",data);
        this.damageInfo = data;
      })
  }

  getAccessoriesChecklist(vin_number, checkin_type) {
    this.api.getAccessoriesChecklist(vin_number, checkin_type)
      .then(data => {
        // console.log("accessoriesChecklist",data[0]);
        this.accessories = data[0];
      })
  }

  gotoLocateVehicle(){
    this.navCtrl.push('RentalManagerLocateVehiclePage', {operationType: this.operation, vehicle: this.vehicle, customer: this.customer}, this.animationsOptions)
  }


  gotoAlertsPage() {
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }

  back() {
    if(this.navCtrl.canGoBack()){
      this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('HomePage')
    }
  }

}

