import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerVehicleProfilePage } from './rental-manager-vehicle-profile';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RentalManagerVehicleProfilePage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(RentalManagerVehicleProfilePage),
  ],
})
export class RentalManagerVehicleProfilePageModule {}
