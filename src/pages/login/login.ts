import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';
// import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { customFunctions } from '../../providers/functions';

// import { RentalManagerRentalInformationPage } from '../rental-manager-rental-information/rental-manager-rental-information';
// import { RentalManagerRentalInformationPage } from '../rental-manager-rental-information/rental-manager-rental-information';
// import { VehicleProfileWithAnimationNewPage } from '../vehicle-profile-with-animation-new/vehicle-profile-with-animation-new';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {
    email: '',
    password: ''
  };
  
  vehicle = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _auth: AuthProvider,
    private func: customFunctions) 
    {
    if(localStorage.getItem('auth_token')){
      this.navCtrl.setRoot('HomePage');
    }

    this.vehicle = this.navParams.get('vehicle') ? this.navParams.get('vehicle') : "";

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    this.func.presentLoading();
    this._auth.login(this.user)
      .then(data=> {
        if(data){
          this.func.dismissLoading();
          if(this.vehicle){
            this.navCtrl.push('HomePage', {vehicle: this.vehicle, topContent:0});
          }else{
            this.navCtrl.push('HomePage', {reload: 1})
          }
          // this.navCtrl.setRoot(HomePage);
        }else{
          this.func.dismissLoading();
          this.error();
        }
      })
    .catch( error => {
      this.func.dismissLoading();
      this.error();
  })
  }

  error(){
    this.user.email = '';
    this.user.password = '';
    this.func.showAlert('Error','Incorrect Email or Password. Please try again.');
  }


  forgotPassword() {
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
  }
    this.navCtrl.push('ForgotPasswordPage', {}, animationsOptions);
  }

}
