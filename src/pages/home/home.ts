import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
// import { RentalManagerPage } from '../rental-manager/rental-manager';
// import { CheckInPage } from '../check-in/check-in';
// import { VehicleListWithAnimationPage } from '../vehicle-list-with-animation/vehicle-list-with-animation';
import { ApiProvider } from '../../providers/api/api';
// import { VehicleProfileWithAnimationNewPage } from '../vehicle-profile-with-animation-new/vehicle-profile-with-animation-new';
// import { AlertsHomePage } from '../alerts-home/alerts-home';

/**
 * Generated class for the HomeAnimationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  menu = '';
  animate = false;
  alertsCount = 0;

  dealer_name = '';
  dealer_logo;
  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private nativePageTransitions: NativePageTransitions,
              private api: ApiProvider,
              ) {
    if(this.navParams.get('vehicle')){
      this.navCtrl.setRoot('VehicleProfileWithAnimationNewPage', {vehicle: this.navParams.get('vehicle'), topContent:0})
    }
    this.getAlertsCount();
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeAnimationPage');
  }

  ionViewWillLoad() {
    this.dealer_logo = localStorage.getItem('dealer_logo');
    this.dealer_name = localStorage.getItem('dealer_name');
    if(this.dealer_name){
      if(this.dealer_name.length > 15){
        this.dealer_name = this.dealer_name.slice(0, 15)+".."
      }
    }
  }

  ionViewDidLeave(){
    this.animate= false;
    this.menu = '';
  }

  getAlertsCount(){
    this.api.getAllAlertsCount()
    .then(data=>{
      if(data){
        console.log(localStorage.getItem('allAlertsCount'))
        let AllAlertsCount = localStorage.getItem('allAlertsCount');
        (AllAlertsCount) ? this.alertsCount = parseInt(AllAlertsCount) : this.alertsCount = 0;
      }else{
        this.alertsCount = 0
      }
    });
  }
  

  gotoRentalManager(menu){
    this.menu = menu;
    setTimeout(()=>{
      this.animate= true;
      let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 1000,
       };
      setTimeout(()=>{
        this.nativePageTransitions.slide(options);
        this.navCtrl.push('RentalManagerPage', {animate: false});
      }, 800)
    },1000)
  }

  gotoCheckIn(menu){
    this.menu = menu;
    setTimeout(()=>{
      this.animate= true;
      let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 1000,
       };
      setTimeout(()=>{
        this.nativePageTransitions.slide(options);
        this.navCtrl.push('CheckInPage', {animate: false});
      }, 800)
    },1000)
  }

  gotoVehiclesList(menu){
    this.menu = menu;
    setTimeout(()=>{
      this.animate= true;
      let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 1000,
       };
      setTimeout(()=>{
        this.nativePageTransitions.slide(options);
        this.navCtrl.push('VehicleListWithAnimationPage', {animate: false});
      }, 800)
    },1000)
  }

  gotoAlertsDashboard(menu){
    this.menu = menu;
    setTimeout(()=>{
      this.animate= true;
      let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 1000,
       };
      setTimeout(()=>{
        this.nativePageTransitions.slide(options);
        this.navCtrl.push('AlertsHomePage', {animate: false});
      }, 800)
    },1000)
  }

}
