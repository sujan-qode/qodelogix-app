import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountSettingPage } from './account-setting';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AccountSettingPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(AccountSettingPage),
  ],
})
export class AccountSettingPageModule {}
