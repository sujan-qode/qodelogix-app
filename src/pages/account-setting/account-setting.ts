import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Events, ActionSheetController, normalizeURL } from 'ionic-angular';
import { customFunctions } from '../../providers/functions';
import { ApiProvider } from '../../providers/api/api';
// import { HomePage } from '../home/home';
import { env } from '../../environment';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File, FileEntry } from "@ionic-native/file";
// import { AlertsHomePage } from '../alerts-home/alerts-home';
/**
 * Generated class for the AccountSettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account-setting',
  templateUrl: 'account-setting.html',
})
export class AccountSettingPage {

  alertsCount;
  isRootPage;
  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  first_name;
  last_name;
  image;
  full_name;
  phone_number;
  email;
  password1='';
  password2='';

  imageDataUri;
  imageName;

  notification_setting;

  
  isTabletOrIpad:boolean;
  
  constructor(private func: customFunctions ,
              private api: ApiProvider ,
              private platform:Platform, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private event: Events,
              private camera: Camera,
              private readonly file: File,
              public actionSheetCtrl : ActionSheetController,
            ) {
    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.setValues();
    this.getNotificationSettings();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountSettingPage');
  }

  getNotificationSettings(){
    let user_id = parseInt(localStorage.getItem('auth_id'))
    this.api.getAlertTypeForSettings(user_id)
      .then(data => {
        console.log(data)
        this.notification_setting = data;
      })
  }

  updateNotificationSetting(alert_type_id, status, status_type){
    status = (status == true) ? 1 : 0;
    console.log(status, status, alert_type_id)

    let formData = new FormData();
    formData.append('user_id', localStorage.getItem('auth_id'))
    formData.append('dealer_id', localStorage.getItem('dealer_id'))
    formData.append('alert_type_id', alert_type_id)
    formData.append('status', status)
    formData.append('status_type', status_type)
    formData.append('dealer_id', localStorage.getItem('dealer_id'))
    formData.append('user_id',  localStorage.getItem('auth_id'))

    this.api.updateAlertTypeForSettings(formData)
      .then(data => {
        console.log(data)
        this.func.presentToast('Notification Setting Updated Successfully.', 3000, "bottom")
      })
  }

  save(){

    if(this.password1 == '' && this.password2 == ''){
      this.updateUserProfile();
    }
    else if(this.password1 == this.password2){
      if((this.password1.length == this.password2.length) && this.password1.length >= 6){
        this.updateUserProfile();
      }else{
        this.func.showAlert('Error', 'Password must be atleast 6 characters.')
      }
    }else{
      this.func.showAlert('Error', 'Password do not match.')
    }
  }

  updateUserProfile(){
    let data = this.setFormData();
    this.func.presentLoading();
    this.api.updateUserProfile(data)
      .then(data => {
        console.log(data)
        this.func.dismissLoading();
        this.func.showAlert('Success', 'Profile Updated Sucessfully.')
        this.setLocalValues(data);
        this.event.publish('user:updated', data);
      })
      .catch(error => {
        this.func.dismissLoading();
        this.func.showAlert('Error', 'Something Went Wrong.')
      })
  }

  setFormData(){
    let formData = new FormData()
    formData.append('id', localStorage.getItem('auth_id'))
    formData.append('full_name', this.full_name)
    formData.append('phone_number', this.phone_number)
    formData.append('email', this.email)
    formData.append('password1', this.password1)

    if(this.imageDataUri){
      formData.append('image', this.imageDataUri, this.imageName)
    }
    formData.append('dealer_id', localStorage.getItem('dealer_id'))
    formData.append('user_id',  localStorage.getItem('auth_id'))
    return formData;
  }

  setValues(){
    setTimeout(()=> {
      this.full_name = localStorage.getItem('name');
      this.image = env.base + localStorage.getItem('avatar');
      this.email = localStorage.getItem('email');
      this.phone_number = localStorage.getItem('phone_number');
    },500)
  }

  setLocalValues(user){
    localStorage.setItem('name', user.full_name);
    localStorage.setItem('avatar', user.avatar);
    localStorage.setItem('email', user.email);
    localStorage.setItem('phone_number', user.phone_number);
  }

  
  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Image Source',
      buttons: [
        {
          text: 'Camera',
          handler: () => {
            this.takePhoto();
          }
        }, {
          text: 'Local',
          handler: () => {
            this.selectPhoto();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.PNG,
      targetWidth: 512,
      targetHeight: 512,
      saveToPhotoAlbum: true,
      correctOrientation: true,
    }
    this.camera.getPicture(options)
      .then(imageData => {
        this.image = normalizeURL(imageData);
        this.uploadPhoto(imageData);
      }, error => {
        this.func.presentToast(JSON.stringify(error), 3000, 'bottom')
      });
    }

  selectPhoto(): void {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      quality: 70,
      allowEdit:true,
      targetHeight: 300,
      targetWidth: 300,
      encodingType: this.camera.EncodingType.PNG,
    }).then(imageData => {
      this.image = normalizeURL(imageData);
      this.uploadPhoto(imageData);
    }, error => {
      this.func.presentToast(JSON.stringify(error), 3000, 'bottom')
    });
  }

  uploadPhoto(imageFileUri: any): void {
    this.file.resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile(file)))
      .catch(err => console.log('Error',JSON.stringify(err)));
  }

  private readFile(file: any) {
    const reader = new FileReader();
    if(this.platform.is('android')){
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        this.imageDataUri = imgBlob;
        this.imageName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
      };
    }else{
      reader.onload = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        this.imageDataUri = imgBlob;
        this.imageName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
      };
    }
    reader.readAsArrayBuffer(file);
  }


  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }
 
  back(){
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
