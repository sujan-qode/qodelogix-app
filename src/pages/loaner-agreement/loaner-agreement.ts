import { env } from './../../environment';
// import { AlertsPage } from './../alerts/alerts';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { AssignLoanerVehicleStep_1Page } from '../assign-loaner-vehicle-step-1/assign-loaner-vehicle-step-1';
// import { ReturnLoanerVehicleStep_1Page } from '../return-loaner-vehicle-step-1/return-loaner-vehicle-step-1';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';

/**
 * Generated class for the LoanerAgreementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loaner-agreement',
  templateUrl: 'loaner-agreement.html',
})
export class LoanerAgreementPage {

  isRootPage = false;
  alertsCount; 

  
  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  baseUrl = env.base;
  vehicle;
  customer;
  isAssigned = false;
  operation;
  
  isTabletOrIpad:boolean;
  today;
   
  constructor(private platform:Platform, public navCtrl: NavController, public navParams: NavParams, private api: ApiProvider) {
    this.api.getAllAlertsCount();
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');

    this.vehicle = this.navParams.get('vehicle')
    this.customer = this.navParams.get('customer')
    this.operation = this.navParams.get('operationType')
    console.log(this.operation)
    if(this.operation){
      this.isAssigned = true;
    }
    this.today = new Date().toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoanerAgreementPage');
  }

  gotoAssignVehiclePage(){
    this.navCtrl.push('AssignLoanerVehicleStep_1Page', {}, this.animationsOptions);
  }

  gotoReturnVehiclePage(){
    this.navCtrl.push('ReturnLoanerVehicleStep_1Page', {}, this.animationsOptions);
  }

  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back(){
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
