import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoanerAgreementPage } from './loaner-agreement';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    LoanerAgreementPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(LoanerAgreementPage),
  ],
})
export class LoanerAgreementPageModule {}
