import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { AlertsPage } from '../alerts/alerts';
import { ApiProvider } from '../../providers/api/api';
import { customFunctions } from '../../providers/functions';

/**
 * Generated class for the AlertsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alerts-home',
  templateUrl: 'alerts-home.html',
})
export class AlertsHomePage {

  isRootPage = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  fuelAlertsCount = 0;
  batteryAlertsCount = 0;
  gpsAlertsCount = 0;
  mileageAlertsCount = 0;

  fuel = 0;
  battery = 0;
  gps = 0;
  mileage = 0;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private api: ApiProvider,
              private func:customFunctions
              ) {
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.getAlertsCount();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlertsHomePage');
  }

  getAlertsCount(){
    // this.func.presentLoading();
    this.api.getAlertsCount()
      .then(data => {
        if(data){
          // console.log(data['fuel_low_count'], data)
          this.fuelAlertsCount = data['fuel_low_count'];
          this.batteryAlertsCount = data['battery_low_count'];
          this.gpsAlertsCount = data['geo_fence_count'];
          this.mileageAlertsCount = data['mileage_high_count'];
          this.func.dismissLoading();
        }else{
          throw('error');
        }
      })
      .then(()=>{
        this.counterFuelAlerts();
        this.counterBatteryAlerts();
        this.counterGpsAlerts();
        this.counterMileageAlerts();
      })
      .catch(err => {
        console.log(err);
        this.fuelAlertsCount = 0;
        this.batteryAlertsCount = 0;
        this.gpsAlertsCount = 0;
        this.mileageAlertsCount = 0;
        this.func.dismissLoading();
      })
  }

  counterBatteryAlerts(){
    // let interval = setInterval(()=>{
    //   if(this.batteryAlertsCount == 0) clearInterval(interval);
    //   if(this.batteryAlertsCount > 0) this.battery++;
    //   if(this.battery == this.batteryAlertsCount) clearInterval(interval);
    // },100)
    let self = this;
    let timesRun = 0;
    let interval = setInterval(function(){
      if(this.batteryAlertsCount == 0) clearInterval(interval);
        timesRun += 1;
        self.battery = self.getRandomInt(0, self.batteryAlertsCount)
        if(timesRun === 40){
            clearInterval(interval);
            self.battery = self.batteryAlertsCount;
        }
    }, 50); 
  }

  counterFuelAlerts(){
    // let interval = setInterval(()=>{
    //   if(this.fuelAlertsCount == 0) clearInterval(interval);
    //   if(this.fuelAlertsCount > 0) this.fuel++;
    //   if(this.fuel == this.fuelAlertsCount) clearInterval(interval);
    // },100)
    let self = this;
    let timesRun = 0;
    let interval = setInterval(function(){
        if(this.fuelAlertsCount == 0) clearInterval(interval);
        timesRun += 1;
        self.fuel = self.getRandomInt(0, self.fuelAlertsCount)
        if(timesRun === 40){
            clearInterval(interval);
            self.fuel = self.fuelAlertsCount;
        }
    }, 50); 
  }

  counterMileageAlerts(){
    // let interval = setInterval(()=>{
    //   if(this.mileageAlertsCount == 0) clearInterval(interval);
    //   if(this.mileageAlertsCount > 0) this.mileage++;
    //   if(this.mileage == this.mileageAlertsCount) clearInterval(interval);
    // },100)
    let self = this;
    let timesRun = 0;
    let interval = setInterval(function(){
      if(this.mileageAlertsCount == 0) clearInterval(interval);
        timesRun += 1;
        self.mileage = self.getRandomInt(0, self.mileageAlertsCount)
        if(timesRun === 40){
            clearInterval(interval);
            self.mileage = self.mileageAlertsCount;
        }
    }, 50); 
  }

  counterGpsAlerts(){
    // let interval = setInterval(()=>{
    //   if(this.gpsAlertsCount == 0) clearInterval(interval);
    //   if(this.gpsAlertsCount > 0) this.gps++;
    //   if(this.gps == this.gpsAlertsCount) clearInterval(interval);
    // },100) 
    let self = this;
    let timesRun = 0;
    let interval = setInterval(function(){
      if(this.gpsAlertsCount == 0) clearInterval(interval);
        timesRun += 1;
        self.gps = self.getRandomInt(0, self.gpsAlertsCount)
        if(timesRun === 40){
            clearInterval(interval);
            self.gps = self.gpsAlertsCount;
        }
    }, 50); 

  }


  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  navigateToAlertsPage(vehicle_status){
    this.navCtrl.push('AlertsPage', {vehicle_status: vehicle_status}, this.animationsOptions);
  }

  back(){
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
