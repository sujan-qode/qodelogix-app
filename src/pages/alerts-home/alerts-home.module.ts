import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlertsHomePage } from './alerts-home';

@NgModule({
  declarations: [
    AlertsHomePage,
  ],
  imports: [
    IonicPageModule.forChild(AlertsHomePage),
  ],
})
export class AlertsHomePageModule {}
