import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the CustomerListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-customer-list',
  templateUrl: 'customer-list.html',
})
export class CustomerListPage {

  alertsCount;
  isRootPage = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform, public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider) {
    this.api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
  }


  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }


  back(){
    // this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    this.navCtrl.setRoot('HomePage')
  }

}
