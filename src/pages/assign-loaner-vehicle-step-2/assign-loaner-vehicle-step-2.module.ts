import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignLoanerVehicleStep_2Page } from './assign-loaner-vehicle-step-2';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AssignLoanerVehicleStep_2Page,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AssignLoanerVehicleStep_2Page),
  ],
})
export class AssignLoanerVehicleStep_2PageModule {}
