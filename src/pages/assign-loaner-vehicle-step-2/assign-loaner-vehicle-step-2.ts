// import { AssignLoanerVehicleStep_3Page } from './../assign-loaner-vehicle-step-3/assign-loaner-vehicle-step-3';
// import { AlertsPage } from './../alerts/alerts';
import { env } from './../../environment';
import { ApiProvider } from './../../providers/api/api';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { AssignLoanerVehicleStep_4Page } from '../assign-loaner-vehicle-step-4/assign-loaner-vehicle-step-4';
// import { AlertsHomePage } from '../alerts-home/alerts-home';

/**
 * Generated class for the AssignLoanerVehicleStep_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-assign-loaner-vehicle-step-2',
  templateUrl: 'assign-loaner-vehicle-step-2.html',
})
export class AssignLoanerVehicleStep_2Page {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  
   baseUrl = env.base;
   vehicle;
   allVehicles;
   alertsCount;
   displaySurroundingVehicles = true;
   surroundingVehicleMarkers = [];
   
 
   animationsOptions = {
     animation: 'ios-transition',
     duration: 1000
   }
 
   operation;
   
   isTabletOrIpad:boolean;
   
   geofences;
   facilityblock = new google.maps.Polygon({ })
   
   constructor(private platform:Platform, public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider) {
     this.api.getAllAlertsCount();
     this.operation = this.navParams.get('operation');
     this.vehicle = this.navParams.get('vehicle');
     this.alertsCount = localStorage.getItem('allAlertsCount');
     this.getAllVehicles();
     this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
     this.getGeofence();
   }
  
  ionViewDidLoad(){
    this.loadMap();
   }

   getGeofence() {
    this.api.getGeofence()
      .then(data => {
        this.geofences = data;
        
        this.setMapSettingsDropdown(this.map);
        this.setGeofenceDropdown(this.map);
        this.setMapZoomControl(this.map);
        console.log(data)
      })
  }

   
  setGeofenceDropdown(map) {
    let self = this;
    var GeofencecontrolDiv = document.createElement('div');
    GeofencecontrolDiv.className = 'dropdown-container';

    var geoFenceDropdownWrapper = document.createElement('div');
    geoFenceDropdownWrapper.className = 'dropdown-content';
    geoFenceDropdownWrapper.innerText = 'Geo-Fence Settings';
    GeofencecontrolDiv.appendChild(geoFenceDropdownWrapper);

    var arrow = document.createElement('div');
    arrow.className = 'dropDownArrow';
    geoFenceDropdownWrapper.appendChild(arrow);
    // Adding dropdown to map
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(GeofencecontrolDiv);

    GeofencecontrolDiv.addEventListener('click', function () {
      if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-none")) {
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-none")
      }
      document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-block");
    });

    var MapOptionDiv = document.createElement('div');
    MapOptionDiv.className = 'geofence-dropdown-option-container';
    MapOptionDiv.style.display = "none";

    for(var i=0; i<this.geofences.length; i++){

      var MapOption1Div = document.createElement('div');
      MapOption1Div.className = 'option';
      MapOption1Div.id = ""+i+"";
      MapOption1Div.innerText = this.geofences[i]['region_name']
      MapOptionDiv.appendChild(MapOption1Div);

      MapOption1Div.addEventListener('click', function () {
        if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-block")) {
          document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-block")
        }
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-none");
      });
    }

    var MapOption2Div = document.createElement('div');
    MapOption2Div.className = 'option clear-fence';
    MapOption2Div.id = "clear-fence";
    MapOption2Div.innerText = "Clear"
      MapOptionDiv.appendChild(MapOption2Div);

      MapOption2Div.addEventListener('click', function () {
        if (document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.contains("display-block")) {
          document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.remove("display-block")
        }
        document.getElementsByClassName('geofence-dropdown-option-container')[0].classList.add("display-none");
      });


    document.addEventListener('click',function(e){
      if(e.target && e.srcElement.id == "clear-fence"){
        self.facilityblock.setMap(null); // remove previous geofence
      }
      else if(e.target && e.srcElement.id){
        if(e.srcElement.className == "option"){
          let id = e.srcElement.id;
          self.facilityblock.setMap(null); // remove previous geofence
          self.plotGeofence(self.geofences[id])
        }
      }
   });
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(MapOptionDiv);
  }

  plotGeofence(geofence){
    console.log(geofence)
    var polygonCoords = geofence["plotData"];
    this.map.setCenter(new google.maps.LatLng(polygonCoords[0].lat.toString(), polygonCoords[0].lng.toString()));
    this.map.setZoom(15)
    this.facilityblock = new google.maps.Polygon({
      paths: polygonCoords,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#FF0000',
      fillOpacity: 0.2
  });
  this.facilityblock.setMap(this.map);
    console.log(polygonCoords)
  }

  setMapSettingsDropdown(map) {
    var MapSettingscontrolDiv = document.createElement('div');
    MapSettingscontrolDiv.className = 'dropdown-container';

    var MapSettingsDropdownWrapper = document.createElement('div');
    MapSettingsDropdownWrapper.className = 'dropdown-content';
    MapSettingsDropdownWrapper.innerText = 'Map Settings';
    MapSettingscontrolDiv.appendChild(MapSettingsDropdownWrapper);

    var arrow = document.createElement('div');
    arrow.className = 'dropDownArrow';
    MapSettingsDropdownWrapper.appendChild(arrow);

    // Adding dropdown to map
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(MapSettingscontrolDiv);

    MapSettingscontrolDiv.addEventListener('click', function () {
      if(document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-none")){
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-none")
      }
      document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-block");
    });

    var MapOptionDiv = document.createElement('div');
    MapOptionDiv.className = 'dropdown-option-container';
    MapOptionDiv.style.display = "none";

    var MapOption1Div = document.createElement('div');
    MapOption1Div.className = 'option';
    MapOption1Div.innerText = 'Map';
    MapOptionDiv.appendChild(MapOption1Div);

    var MapOption2Div = document.createElement('div');
    MapOption2Div.className = 'option';
    MapOption2Div.innerText = 'Satellite';
    MapOptionDiv.appendChild(MapOption2Div);

    var MapOption3Div = document.createElement('div');
    MapOption3Div.className = 'option';
    MapOption3Div.innerText = 'Terrian';
    MapOptionDiv.appendChild(MapOption3Div);
    // Adding dropdown to map
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(MapOptionDiv);

    MapOption1Div.addEventListener('click', function () {
      map.setMapTypeId('roadmap')
      if(document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-block")){
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-block")
      }
      document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-none");
    });
    MapOption2Div.addEventListener('click', function () {
      map.setMapTypeId('satellite')
      if(document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-block")){
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-block")
      }
      document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-none");
    });
    MapOption3Div.addEventListener('click', function () {
      map.setMapTypeId('terrain')
      if(document.getElementsByClassName('dropdown-option-container')[0].classList.contains("display-block")){
        document.getElementsByClassName('dropdown-option-container')[0].classList.remove("display-block")
      }
      document.getElementsByClassName('dropdown-option-container')[0].classList.add("display-none");
    });

  }

  setMapZoomControl(map) {
    // Creating divs & styles for custom zoom control
    var controlDiv = document.createElement('div');
    controlDiv.className = 'zoom-btn-container';

    // Set CSS for the control wrapper
    var controlWrapper = document.createElement('div');
    controlWrapper.className = 'zoom-btn-controlWrapper';
    controlDiv.appendChild(controlWrapper);

    // Set CSS for the reset
    var resetButton = document.createElement('div');
    resetButton.className = 'zoom-btn-resetButton';
    controlWrapper.appendChild(resetButton);

    // Set CSS for the zoomIn
    var zoomInButton = document.createElement('div');
    zoomInButton.className = 'zoom-btn-zoomInButton';
    controlWrapper.appendChild(zoomInButton);

    // CSS for divider
    var bar = document.createElement('div');
    bar.className = 'zoom-btn-bar';
    // adding divider
    controlWrapper.appendChild(bar);

    // Set CSS for the zoomOut
    var zoomOutButton = document.createElement('div');
    zoomOutButton.className = 'zoom-btn-zoomOutButton';
    controlWrapper.appendChild(zoomOutButton);

    //css for bar2
    var bar2 = document.createElement('div');
    bar2.className = 'zoom-btn-bar2';
    // adding divider
    controlWrapper.appendChild(bar2);

    // Adding buttons to map
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);

    // Setup the click event listener - zoomIn
    zoomInButton.addEventListener('click', function () {
      map.setZoom(map.getZoom() + 1);
    });
    // Setup the click event listener - zoomOut
    zoomOutButton.addEventListener('click', function () {
      map.setZoom(map.getZoom() - 1);
    });
    // Setup the click event listener - reset
    let self = this;
    resetButton.addEventListener('click', function () {
      self.setMapOnAll(null);
      self.setMapOnAll(map)
      map.setCenter({lat:parseFloat(self.vehicle.latitude), lng:parseFloat(self.vehicle.longitude)});
      map.setZoom(20)
    });
  }
  


  getAllVehicles() {
    this.api.getAllVehicles()
    .then(data => {
      this.allVehicles = data;
    }).then(()=> {
      this.setSorroundingvehicleMarkers();
    })
  }
 
  loadMap(){
    let latLng = new google.maps.LatLng(this.vehicle.latitude, this.vehicle.longitude);
    let mapOptions = {
      center: latLng,
      zoom:20,
      tilt: 0,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      disableDefaultUI: true
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.vehicle);
  }

  addMarker(vehicle){
    let image = {url: 'assets/icon/car-icon-black.png'};
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(vehicle.latitude, vehicle.longitude),
      icon: image
    });
    let content = "<p style='font-size:15px'>VIN: <br><strong>"+ vehicle.vin_number+"<strong></p>";         
    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content){
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  setSorroundingvehicleMarkers(){
    this.surroundingVehicleMarkers = [];
    let image = {url: 'assets/icon/car-icon-grey.png'};
    for(var i=0;i<this.allVehicles.length;i++){
      let v = this.allVehicles[i];
      // console.log(v)
      let distanceInKm = this.calculateDistance(this.vehicle.latitude, v.latitude, this.vehicle.longitude, v.longitude);
      if(distanceInKm == 0){}
      else{
        // display surrounding vehicles if they are in the radius of 10 meters from the current one
        if(distanceInKm <= 0.01){
          console.log('in range: '+distanceInKm);
          let marker1 = new google.maps.Marker({
            map: this.map,
            // animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(v.latitude, v.longitude),
            icon: image
          });
          this.surroundingVehicleMarkers.push(marker1)
          let content1 = "<p style='font-size:15px'>VIN: <br><strong>"+ v.vin_number+"<strong></p>";         
          this.addInfoWindow(marker1, content1);
        }
      }
    }
  }
  // Sets the map on all markers in the array.
  setMapOnAll(map) {
    for (var i = 0; i < this.surroundingVehicleMarkers.length; i++) {
      this.surroundingVehicleMarkers[i].setMap(map);
    }
  }

  calculateDistance(lat1:number,lat2:number,long1:number,long2:number){
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat1-lat2) * p) / 2 + c(lat2 * p) *c((lat1) * p) * (1 - c(((long1- long2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    return dis; // in km
  }

  gotoAssignLoanerStep3(){
    if(this.operation == 'return'){
      this.navCtrl.push('AssignLoanerVehicleStep_4Page', {customer:this.vehicle.customer_id, vehicle: this.vehicle, operation: "return"}, this.animationsOptions)
    }else{
      this.navCtrl.push('AssignLoanerVehicleStep_3Page', {vehicle: this.vehicle}, this.animationsOptions);
    }
    
  }
  
  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back(){
    this.navCtrl.pop()
  }

}
