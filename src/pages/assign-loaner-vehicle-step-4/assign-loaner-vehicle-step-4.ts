import { env } from './../../environment';
import { customFunctions } from './../../providers/functions';
// import { LoanerAgreementPage } from './../loaner-agreement/loaner-agreement';
// import { AssignLoanerVehicleCreateCustomerPage } from './../assign-loaner-vehicle-create-customer/assign-loaner-vehicle-create-customer';
// import { AssignLoanerVehicleStep_2Page } from './../assign-loaner-vehicle-step-2/assign-loaner-vehicle-step-2';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { AssignLoanerVehicleStep_5AgreementPage } from '../assign-loaner-vehicle-step-5-agreement/assign-loaner-vehicle-step-5-agreement';

/**
 * Generated class for the AssignLoanerVehicleStep_4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assign-loaner-vehicle-step-4',
  templateUrl: 'assign-loaner-vehicle-step-4.html',
})
export class AssignLoanerVehicleStep_4Page {

  baseUrl = env.base;
  
  vehicle;
  customer;

  damageInfo;

  alertsCount;
  checkin_type;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  
  isTabletOrIpad:boolean;
  operation;
  
  constructor(private func: customFunctions ,private api: ApiProvider ,private platform:Platform, public navCtrl: NavController, public navParams: NavParams) {
    this.api.getAllAlertsCount();
    this.operation = this.navParams.get('operation');
    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');
    if(this.vehicle.customer_id && this.operation == "return"){
      this.func.presentLoading();
      this.getCustomerProfile(this.vehicle.customer_id)
    }
    console.log(this.vehicle)
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');

    this.getDamageInfo(this.vehicle.vin_number, this.checkin_type);

    console.log(this.customer)
  }

 ionViewDidLoad() {
   console.log('ionViewDidLoad AssignLoanerVehicleStep_3Page');
 }

 getCustomerProfile(customer_id){
  let data = {
    "CustomerId" : customer_id,
    "DealerId" : ""
  }
  this.api.getCustomerAccessTokenExternalAPI()
      .then((token)=> {
        this.api.getCustomerProfileFromExternalAPI(data, token)
          .then(data => {
            console.log(data);
            this.func.dismissLoading();
            this.customer = data;
          })  
      })
      .catch(err => {
        console.log(err)
        this.func.dismissLoading();
        this.func.presentToast("An Error Occurred While Fetching The Data");
        this.navCtrl.pop();
      })
}

 getDamageInfo(vin_number,checkin_type){
  this.api.getDamageInfo(vin_number, checkin_type)
      .then( data => {
        console.log(data)
        this.damageInfo = data;
      })
} 

gotoAssignLoanerVehicleStep2(){
  this.navCtrl.push('AssignLoanerVehicleStep_2Page', {vehicle: this.vehicle}, this.animationsOptions);
}

gotoEditCustomerProfileForLoaner(){
  this.navCtrl.push('AssignLoanerVehicleCreateCustomerPage', {listFor:"assign_loaner",vehicle: this.vehicle,customer_id:this.customer.id}, this.animationsOptions)
}

gotoAssignLoanerVehicleStep5(){
  if(this.operation == "return"){
    this.func.presentLoading();
    this.api.returnLoanerAssigned(this.vehicle.customer_id, this.vehicle.vin_number)
      .then(data => {
        this.func.dismissLoading()
        console.log(data)
        this.func.presentToast(data['message'], 3000, "bottom")
        this.navCtrl.push('LoanerAgreementPage', {vehicle: this.vehicle, customer: this.customer, operationType: 'return'}, this.animationsOptions);
      })
      .catch( err => {
        this.func.dismissLoading()
        console.log(err)
      })
  }else{
    this.navCtrl.push('AssignLoanerVehicleStep_5AgreementPage', {vehicle: this.vehicle, customer: this.customer}, this.animationsOptions);
  }
}

 gotoAlertsPage(){
   this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
 }

 back(){
   this.navCtrl.pop()
 }

}
