import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignLoanerVehicleStep_4Page } from './assign-loaner-vehicle-step-4';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AssignLoanerVehicleStep_4Page,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AssignLoanerVehicleStep_4Page),
  ],
})
export class AssignLoanerVehicleStep_4PageModule {}
