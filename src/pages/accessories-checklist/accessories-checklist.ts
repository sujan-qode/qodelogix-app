import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
// import { ReviewAndCheckInPage } from '../review-and-check-in/review-and-check-in';
import { ApiProvider } from '../../providers/api/api';
import { customFunctions } from '../../providers/functions';
import { env } from '../../environment';
// import { AlertsHomePage } from '../alerts-home/alerts-home';

/**
 * Generated class for the AccessoriesChecklistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accessories-checklist',
  templateUrl: 'accessories-checklist.html',
})
export class AccessoriesChecklistPage {
  vinDetail = null;
  baseUrl = env.base;

  alertsCount;
  vehicle;
  accessories = {
    books: false,
    books_note: null,
    pouch: false,
    pouch_note: null,
    keys: false,
    keys_note: null,
    tablet: false,
    tablet_note: null,
    headphones: false,
    headphones_note: null,
    mats: false,
    mats_note: null,
  };
  validaAccessories = true;
  checkin_type;

  isRootPage = false;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  user_id;
  dealer_id;
  
  isTabletOrIpad:boolean;
   
  constructor(private platform:Platform, 
            public navCtrl: NavController, 
            public navParams: NavParams, 
            public api: ApiProvider,
            public func: customFunctions
          ) {
    this.api.getAllAlertsCount();
    this.user_id = localStorage.getItem('auth_id');
    this.dealer_id = localStorage.getItem('dealer_id');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.isRootPage = this.navParams.get('rootPage') ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.vehicle = this.navParams.get('vehicle');
    this.checkin_type = this.navParams.get('checkin_type');
    console.log('check_in_type: '+this.checkin_type);
    console.log('vehicle: ',this.vehicle);
    this.getAccessoriesChecklist(this.vehicle.vin_number, this.checkin_type);
    // this.updateVehicle(this.vehicle.vin_number);
  }

  updateVehicle(vin) {
    this.func.presentLoading();
    let response, vehicle;
    this.api.getAllVehicles()
    .then(data => {
      this.func.dismissLoading();
      response = data;
      vehicle = response.filter(v => {
        return v.vin_number.toLowerCase() === vin.toLowerCase()
      })
      this.vehicle = vehicle[0]
    })
    .catch(()=> {
      this.func.dismissLoading();
      this.func.presentToast('Error Occured. Please Try Again.');
      this.navCtrl.pop();
    })
  }

  getAccessoriesChecklist(vin_number, checkin_type){
    this.api.getAccessoriesChecklist(vin_number, checkin_type)
      .then( data => {
        console.log(data)
        if(!this.func.isEmptyObject(data)){
          this.accessories = data[0];
          // console.log(this.accessories)
          // console.log('api is not empty')
        }
      })

      // setInterval(() => {
      //   this.hasEmptyField();
      // }, 1000);
  }
  
  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  gotoReview(formValue){
    this.api.createAccessoriesChecklist(this.getFormUrlEncoded(formValue))
        .then( data => {
          console.log(data)
          if(data['status'] != 'Success'){
            this.func.presentToast('An error occured. Please try again.', 3000, 'bottom')
            return false;
         }
         this.navCtrl.push('ReviewAndCheckInPage', {
            vehicle: this.vehicle,
            checkin_type: this.checkin_type
          },
          this.animationsOptions)
        })
  }

  getFormUrlEncoded(toConvert) {
		const formBody = [];
		for (const property in toConvert) {
			const encodedKey = encodeURIComponent(property);
			const encodedValue = encodeURIComponent(toConvert[property]);
			formBody.push(encodedKey + '=' + encodedValue);
		}
		return formBody.join('&');
	}

  hasEmptyField(){
    if(this.accessories.books_note== ''||
      this.accessories.pouch_note== ''||
      this.accessories.keys_note== ''||
      this.accessories.tablet_note== ''||
      this.accessories.headphones_note== ''||
      this.accessories.mats_note== ''
    ){
      this.validaAccessories = false;
    }else{
      this.validaAccessories = true;
    }
  }

  back(){
    this.navCtrl.pop()
  }

}
