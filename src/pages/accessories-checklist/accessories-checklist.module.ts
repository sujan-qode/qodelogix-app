import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccessoriesChecklistPage } from './accessories-checklist';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AccessoriesChecklistPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AccessoriesChecklistPage),
  ],
})
export class AccessoriesChecklistPageModule {}
