import { env } from './../../environment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { VehicleProfilePage } from '../vehicle-profile/vehicle-profile';
// import { HomePage } from '../home/home';
// import { VoltPipe } from '../../pipes/volt/volt';
// import { VehicleProfileNewPage } from '../vehicle-profile-new/vehicle-profile-new';
import { customFunctions } from '../../providers/functions';

/**
 * Generated class for the VehicleListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehicle-list',
  templateUrl: 'vehicle-list.html'
})
export class VehicleListPage {

  searchedParam;

  baseUrl = env.base;
  alertsCount;
  vehicle_status = 'all';
  dropdown_text = "Vehicle Status";
  filterData;
  alertsData;
  isSearchbarEmpty: boolean = true;
  searchItem;
  vinDetail = [];

  isRootPage = false;
  loading = this.loadingCtrl.create({
    content: 'Loading Vehicle List...'
  });

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  vehicleType;

  searchbarInput;
  isTabletOrIpad: boolean;
  interface:string="popover";
  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController,
    private _api: ApiProvider, private platform: Platform, private func:customFunctions) {
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if(!this.isTabletOrIpad){
      this.interface='action-sheet';
    }
    this._api.getAllAlertsCount();
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.searchedParam = this.navParams.get('searchedParam') ? this.navParams.get('searchedParam') : false;
    this.showLoader();
    this.getAllVehicles();
    this.getVehicleTypes();
  }

  showLoader() {
    this.loading.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehicleListPage');
  }
  ionViewDidLeave(){
    this.func.dismissLoading();
  }

  getVehicleTypes(){
    this._api.getvehicleTypes()
      .then( data => {
        this.vehicleType = data;
        this.vehicleType.unshift({
          name: "All"
        })
        console.log(data)
      })
      .catch(err => {
        console.log(err)
      })
  }

  doInfinite(): Promise<any> {
    console.log('Begin async operation');
    return new Promise((resolve) => {
      setTimeout(() => {
        // for (var i = 0; i < 30; i++) {
        //   this.items.push( this.items.length );
        // }
        console.log('Async operation has ended');
        resolve();
      }, 500);
    })
  }

  gotoAlertsPage() {
    const animationsOptions = {
      animation: 'ios-transition',
      duration: 1000
    }
    this.navCtrl.push('AlertsHomePage', {}, animationsOptions);
  }

  getAllVehicles() {
    this._api.getAllVehicles()
      .then(data => {
        console.log(data)
        this.alertsData = data;
        this.filterData = data;
        this.loading.dismiss();
      }).then(() => {
        if (this.searchedParam) {
          console.log('searchbarInput')
          this.searchbarInput = this.searchedParam;
          this.searchedParam = false;
          this.filterByVin(this.searchbarInput)
        }
      });
  }

  onSearchCancel(ev: any) {
    this.getAllVehicles();
    this.searchbarInput = '';
  }

  getItems(ev: any) {
    let val = ev.target.value;
    this.searchItem = val;
    this.OnVehicleStatusChange();
  }

  

  OnVehicleStatusChange() {
    let searchTerm = this.searchItem;
    console.log(this.vehicle_status, searchTerm)
    if(this.vehicle_status.toLowerCase() == "all"){
      this.filterByVin(searchTerm)
      this.dropdown_text = "Vehicle Status"
      console.log("all vehicles")
    }
    // if (this.vehicle_status == 0) {
    //   this.filterByStatus('demo')
    //   this.dropdown_text = "Demo"
    // }
    // else if (this.vehicle_status == 1) {
    //   this.filterByStatus('loaner')
    //   this.dropdown_text = "Loaner"
    // }
    // else if (this.vehicle_status == 2) {
    //   this.filterByStatus('inventory')
    //   this.dropdown_text = "Inventory"
    // }
    else {
      this.filterByStatus(this.vehicle_status)
      this.dropdown_text = this.vehicle_status
    }
  }


  filterByStatus(status: any) {
    let val = status;
    let vin = this.searchItem;
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.alertsData.filter((vehicle) => {
        if (vin !== undefined) {
          let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number+" "+vehicle.stock_number;
          return ((vehicle.Vehicle_type.toLowerCase().indexOf(val.toLowerCase()) > -1) && (makeModelVin.toLowerCase().indexOf(vin.toLowerCase()) > -1));
        } else {
          return ((vehicle.Vehicle_type.toLowerCase().indexOf(val.toLowerCase()) > -1));
        }
      });
    }
    else {
      // this.filterData = this.alertsData;
      this.getAllVehicles();
      this.isSearchbarEmpty = true;
    }
  }


  filterByVin(val) {
    console.log(val)
    if (val && val.trim() != '') {
      this.isSearchbarEmpty = false;
      this.filterData = this.alertsData.filter((vehicle) => {
        let makeModelVin = vehicle.year + " " + vehicle.make + " " + vehicle.model_trim + " " + vehicle.vin_number+" "+vehicle.stock_number;
        return ((makeModelVin.toLowerCase().indexOf(val.toLowerCase()) > -1));
      });
    }
    else {
      this.getAllVehicles();
      this.isSearchbarEmpty = true;
    }
  }


  gotoVehicleProfile(vehicle) {
    this.func.presentLoading();
    // this.navCtrl.push('VehicleProfilePage', {vehicle: vehicle}, this.animationsOptions);
    this.navCtrl.push('VehicleProfileNewPage', { vehicle: vehicle }, this.animationsOptions);
  }

  back() {
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}
