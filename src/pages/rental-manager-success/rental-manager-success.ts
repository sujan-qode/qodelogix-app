import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
// import { RentalManagerPage } from '../rental-manager/rental-manager';
import { customFunctions } from '../../providers/functions';

/**
 * Generated class for the RentalManagerSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-success',
  templateUrl: 'rental-manager-success.html',
})
export class RentalManagerSuccessPage {

  isRootPage = false;
  alertsCount; 

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  vehicle;
  customer;
  operation;

  today;

  constructor(private func: customFunctions, private api:ApiProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.api.getAllAlertsCount();
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');

    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');
    this.operation = this.navParams.get('operationType');
    console.log(this.operation)

    this.today = new Date().toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentalManagerSuccessPage');
  }


  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  resendAgreementCopy(){
    let vin_number = this.vehicle.vin_number;
    let customer_email = this.customer.customerEmail;

    this.func.presentLoading();
    this.api.resendLoanerAssignEmail(vin_number, customer_email)
      .then((data) => {
        this.func.dismissLoading();
        this.func.presentToast(data, '2000', 'bottom')
      })
  }

  backToMenu(){
    this.navCtrl.push('RentalManagerPage', {}, this.animationsOptions);
  }

  back() {
    if(this.navCtrl.canGoBack()){
      this.isRootPage == false ? this.navCtrl.setRoot('RentalManagerPage'): this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('HomePage')
    }
  }

}
