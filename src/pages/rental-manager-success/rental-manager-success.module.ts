import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerSuccessPage } from './rental-manager-success';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RentalManagerSuccessPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(RentalManagerSuccessPage),
  ],
})
export class RentalManagerSuccessPageModule {}
