import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RentalManagerRentalInformation_2Page } from './rental-manager-rental-information-2';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    RentalManagerRentalInformation_2Page,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(RentalManagerRentalInformation_2Page),
  ],
})
export class RentalManagerRentalInformation_2PageModule {}
