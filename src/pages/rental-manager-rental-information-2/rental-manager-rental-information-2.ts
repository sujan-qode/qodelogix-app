import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { customFunctions } from '../../providers/functions';
// import { RentalManagerSuccessPage } from '../rental-manager-success/rental-manager-success';
/**
 * Generated class for the RentalManagerRentalInformation_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rental-manager-rental-information-2',
  templateUrl: 'rental-manager-rental-information-2.html',
})
export class RentalManagerRentalInformation_2Page {
 
  // Canvas stuff
  @ViewChild('imageCanvas') canvas: any;
  canvasElement: any;
  saveX: number;
  saveY: number;
  selectedColor = '#9e2956';

  
  isRootPage = false;
  alertsCount; 

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  vehicle;
  customer;
  rentalInfo;
  driver;

  isTabletOrIpad: boolean;
  interface: string = "popover";

  damageInfo;

  agreement;
  formData:FormData = new FormData();

  accept_tos = false;
  signed = false;

  operation;

  new_vin_number;
  my_signature;


  constructor(private platform : Platform,
               private api:ApiProvider, 
               public navCtrl: NavController, 
               public func: customFunctions,
               public navParams: NavParams) {
    this.api.getAllAlertsCount();
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.alertsCount = localStorage.getItem('allAlertsCount');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if (!this.isTabletOrIpad) {
      this.interface = 'action-sheet';
    }
    this.operation = this.navParams.get('operationType');
    console.log("opertation_type => "+this.operation)

    this.vehicle = this.navParams.get('vehicle');
    this.customer = this.navParams.get('customer');
    this.rentalInfo = this.navParams.get('rentalInfo');
    this.driver = this.navParams.get('driver');
    
    console.log(this.rentalInfo, this.vehicle, this.customer, this.driver)

    this.getDamageInfo(this.vehicle.vin_number, '0');
    this.getLoanerAgreement();


    if(this.operation == "rental_manager_edit_loaner"){
      this.new_vin_number = localStorage.getItem('new_vin_number') ? localStorage.getItem('new_vin_number') : this.customer.vin_number;
      this.my_signature = localStorage.getItem('signature');

      console.log(this.new_vin_number, this.my_signature)
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RentalManagerRentalInformation_2Page');
    // Set the Canvas Element and its size
      this.canvasElement = this.canvas.nativeElement;
      this.canvasElement.width = this.platform.width() + '';
      this.canvasElement.height = 200;
      this.preventScrollOnDrawing();
  }

  getLoanerAgreement(){
    this.api.getLoanerAgreement()
      .then(data => {
        console.log(data[0])
        this.agreement = data[0].rental_agreement;
      })
      .catch(err => {
        console.log(err)
      })
  }

  getDamageInfo(vin_number,checkin_type){
    this.api.getDamageInfo(vin_number, checkin_type)
        .then( data => {
          console.log(data)
          this.damageInfo = data;
        })
  } 

  preventScrollOnDrawing(){
    let self=this
      document.body.addEventListener("touchstart", function (e) {
        if (e.target == self.canvasElement) {
            e.preventDefault();
        }
    }, { passive: false });
    document.body.addEventListener("touchend", function (e) {
        if (e.target == self.canvasElement) {
            e.preventDefault();
        }
    }, { passive: false });
    document.body.addEventListener("touchmove", function (e) {
        if (e.target == self.canvasElement) {
            e.preventDefault();
        }
    }, { passive: false });
  }


  clearCanvas(){
    this.signed = false;
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
  }
  
  getCanvasImage() {
  var dataUrl = this.canvasElement.toDataURL();
  let ctx = this.canvasElement.getContext('2d');
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
  // let name = new Date().getTime() + '.png';
  var data = dataUrl.split(',')[1];
  let blob = this.b64toBlob(data, 'image/png');

  this.formData.append('signature', blob, "signature.png")
  
  // return blob;
}
 
b64toBlob(b64Data, contentType) {
  contentType = contentType || '';
  var sliceSize = 512;
  var byteCharacters = atob(b64Data);
  var byteArrays = [];
  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);
    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }
  var blob = new Blob(byteArrays, { type: contentType });
  return blob;
}



startDrawing(ev) {
  console.log('started drawing');
  var canvasPosition = this.canvasElement.getBoundingClientRect();
  this.saveX = ev.touches[0].pageX - canvasPosition.x;
  this.saveY = ev.touches[0].pageY - canvasPosition.y;
}
 
moved(ev) {
  console.log('drawing');
  this.signed = true;
  var canvasPosition = this.canvasElement.getBoundingClientRect();
  let ctx = this.canvasElement.getContext('2d');
  let currentX = ev.touches[0].pageX - canvasPosition.x;
  let currentY = ev.touches[0].pageY - canvasPosition.y;
  ctx.lineJoin = 'round';
  ctx.strokeStyle = this.selectedColor;
  ctx.lineWidth = 5;
  ctx.beginPath();
  ctx.moveTo(this.saveX, this.saveY);
  ctx.lineTo(currentX, currentY);
  ctx.closePath();
  ctx.stroke();
  this.saveX = currentX;
  this.saveY = currentY;
}

assignVehicle(){
  this.func.presentLoading();
  this.getCanvasImage()
  this.formData.append('vin_number', this.vehicle.vin_number)
  this.formData.append('customer_id', this.customer.id)
  this.formData.append('dealer_id', localStorage.getItem('dealer_id'))
  this.formData.append('rental_type', this.rentalInfo.rentalType)
  this.formData.append('rental_fee', this.rentalInfo.rentalFee)
  this.formData.append('service_advisor', this.rentalInfo.serviceAdvisor)
  this.formData.append('ro_number', this.rentalInfo.roNumber)
  this.formData.append('pickup_date', this.rentalInfo.pickupDate)
  this.formData.append('drop_off_date', this.rentalInfo.dropoffDate)
  this.formData.append('additional_notes', this.rentalInfo.note)
  this.formData.append('additional_driver', this.driver.addDriver)
  this.formData.append('driver_first_name', this.driver.FirstName)
  this.formData.append('driver_last_name', this.driver.LastName)
  this.formData.append('driver_license_front', this.driver.DLFrontName)
  this.formData.append('driver_license_back', this.driver.DLBackName)
  this.formData.append('insurance_card_front', this.driver.ICFrontName)
  this.formData.append('insurance_card_front', this.driver.ICBackName)
  this.api.setLoanerAssigned(this.formData)
    .then(data => {
      console.log(data)
      if(data){
        this.func.dismissLoading();
        this.func.presentToast(data['message'], 3000, "bottom");
        this.navCtrl.push('RentalManagerSuccessPage', {operationType: this.operation, vehicle: this.vehicle, customer: this.customer}, this.animationsOptions);
      }else{
        throw('error occured');
      }
    })
    .catch(err => {
      console.log(err)
      this.func.dismissLoading();
      this.func.presentToast("Something went wrong. Please try again!", 3000, "bottom");
    })
}


updateLoanerAssigned(){
  this.func.presentLoading();

  if(this.signed){
    this.getCanvasImage();
  }
  this.formData.append('vin_number', this.customer.vin_number)
  this.formData.append('customer_id', this.customer.id)
  this.formData.append('token', this.customer.token)
  this.formData.append('dealer_id', localStorage.getItem('dealer_id'))
  this.formData.append('rental_type', this.rentalInfo.rentalType)
  this.formData.append('rental_fee', this.rentalInfo.rentalFee)
  this.formData.append('service_advisor', this.rentalInfo.serviceAdvisor)
  this.formData.append('ro_number', this.rentalInfo.roNumber)
  this.formData.append('pickup_date', this.rentalInfo.pickupDate)
  this.formData.append('drop_off_date', this.rentalInfo.dropoffDate)
  this.formData.append('additional_notes', this.rentalInfo.note)
  this.formData.append('additional_driver', this.driver.addDriver)
  this.formData.append('driver_first_name', this.driver.FirstName)
  this.formData.append('driver_last_name', this.driver.LastName)
  this.formData.append('driver_license_front', this.driver.DLFrontName)
  this.formData.append('driver_license_back', this.driver.DLBackName)
  this.formData.append('insurance_card_front', this.driver.ICFrontName)
  this.formData.append('insurance_card_front', this.driver.ICBackName)
  this.formData.append('new_vin_number', this.new_vin_number)

  this.api.updateLoanerAssigned(this.formData)
    .then(data => {
      console.log(data)
      if(data){
        this.func.dismissLoading();
        this.func.presentToast(data['message'], 3000, "bottom");
        this.navCtrl.push('RentalManagerSuccessPage', {operationType: this.operation, vehicle: this.vehicle, customer: this.customer}, this.animationsOptions);
      }else{
        throw('error occured');
      }
    })
    .catch(err => {
      console.log(err)
      this.func.dismissLoading();
      this.func.presentToast("Something went wrong. Please try again!", 3000, "bottom");
    })
}

  gotoAlertsPage() {
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back() {
    if(this.navCtrl.canGoBack()){
      this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('HomePage')
    }
  }

}
