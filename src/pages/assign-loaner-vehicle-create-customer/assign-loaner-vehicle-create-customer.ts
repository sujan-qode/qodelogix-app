import { Config } from './../../config';
// import { AssignLoanerVehicleStep_4Page } from './../assign-loaner-vehicle-step-4/assign-loaner-vehicle-step-4';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, Platform } from 'ionic-angular';
// import { AlertsHomePage } from '../alerts-home/alerts-home';
// import { HomePage } from '../home/home';
import { CameraOptions, Camera} from '@ionic-native/camera';
import { customFunctions } from '../../providers/functions';
import { ApiProvider } from '../../providers/api/api';
// import { CustomerListPage } from '../customer-list/customer-list';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Braintree, ApplePayOptions, PaymentUIResult, PaymentUIOptions } from '@ionic-native/braintree';

import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { RentalManagerSelectVehiclePage } from '../rental-manager-select-vehicle/rental-manager-select-vehicle';
// import { AlertsHomePage } from '../alerts-home/alerts-home';

/**
 * Generated class for the AssignLoanerVehicleCreateCustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assign-loaner-vehicle-create-customer',
  templateUrl: 'assign-loaner-vehicle-create-customer.html',
})
export class AssignLoanerVehicleCreateCustomerPage {

  alertsCount;
  isRootPage = false;
  vehicle;
  checkin_type;

  delaer_id = localStorage.getItem('dealer_id')
  customer_id;
  cc_added = false;

  customer = {
    Id  : '',
    DealerId  : '',
    FirstName : '',
    LastName : '',
    PhoneNumber : '',
    CustomerEmail : '',
    AddressOne : '',
    AddressTwo : '',
    City : '',
    State : '',
    Country : '', 
    PostalCode : '',
    Token : '',
    DLFront : '',
    DLBack : '',
    ICFront : '',
    ICBack : '',
    nonce : ''
  }
  
  dl_front_imageUri;
  dl_front_imageName;
  dl_back_imageUri;
  dl_back_imageName;
  ic_front_imageUri;
  ic_front_imageName;
  ic_back_imageUri;
  ic_back_imageName;

  dl_front_blob;
  dl_back_blob;
  ic_front_blob;
  ic_back_blob;

  from_customer_list = false;


  cameraOptions: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.PNG,
      targetWidth: 512,
      targetHeight: 512,
      saveToPhotoAlbum: false,
      correctOrientation: true,
  }

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  
  isTabletOrIpad:boolean;

  listFor;

  private createCustomer: FormGroup;

  private loading: Loading;

  private appleOptions: ApplePayOptions = {
		merchantId: Config.brainTree.merchantId,
		currency: 'USD',
		country: 'US'
  };
   
  constructor(private platform:Platform,
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private camera: Camera,
              private func: customFunctions,
              public api : ApiProvider,
              private formBuilder: FormBuilder,
              private brainTree: Braintree,
              private http: HttpClient,
              private loadingController: LoadingController,
              private alertController: AlertController
            ) 
    { 
    this.isRootPage = this.navParams.get('rootPage') == 1 ? this.navParams.get('rootPage') : false;
    this.from_customer_list = this.navParams.get('from_customer_list') ? this.navParams.get('from_customer_list') : false;
    this.api.getAllAlertsCount();
    this.listFor = this.navParams.get('listFor');
    this.customer_id = this.navParams.get('customer_id');
    if(this.customer_id){
      this.getCustomerProfile(this.customer_id)
    }
    this.vehicle = this.navParams.get('vehicle');
    console.log(this.listFor, this.vehicle)
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    this.alertsCount = localStorage.getItem('allAlertsCount');
   
    this.createCustomer = this.formBuilder.group({
      Id: [this.customer_id],
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      PhoneNumber: ['', [Validators.required,
                  Validators.pattern("^[0-9]*$")
                ]],
                CustomerEmail: ['', [Validators.required,
                Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
              ]],
      AddressOne: ['', Validators.required],
      AddressTwo: [''],
      City: ['', Validators.required],
      State: ['', Validators.required],
      PostalCode: ['', Validators.required],
      Country: ['', Validators.required],
      DealerId: [this.delaer_id],
      Token: [''],
      nonce: [''],
      DLFront: [''],
      DLBack: [''],
      ICFront: [''],
      ICBack: [''],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateCustomerPage');
  }

  getCustomerProfile(customer_id){
    this.func.presentLoading("Fetching Customer Detail")
    let data = {
      // "CustomerId" : "aa9444d7-544c-4b1e-a68a-bc103dec4986",
      "CustomerId" : this.customer_id,
      "DealerId" : "",
      "WithDocmentImages" : "YES"
    }
    this.api.getCustomerAccessTokenExternalAPI()
      .then((token)=> {
      this.api.getCustomerProfileFromExternalAPI(data, token)
        .then(data => {
          console.log(data);
          this.setCustomer(data);
          this.func.dismissLoading();
        })
        .catch(()=> {
          this.func.dismissLoading();
          this.func.presentToast("An Error Occurred While Fetching The Data");
          this.navCtrl.pop();
        })
      })
      .catch(err => {
        console.log(err)
        this.func.dismissLoading();
        this.func.presentToast("An Error Occurred While Fetching The Data");
        this.navCtrl.pop();
      })
  }

  setCustomer(data){
    this.createCustomer.controls['Id'].patchValue(data.id);
    this.createCustomer.controls['FirstName'].patchValue(data.firstName);
    this.createCustomer.controls['LastName'].patchValue(data.lastName);
    this.createCustomer.controls['PhoneNumber'].patchValue(data.phoneNumber);
    this.createCustomer.controls['CustomerEmail'].patchValue(data.customerEmail);
    this.createCustomer.controls['AddressOne'].patchValue(data.addressOne);
    this.createCustomer.controls['AddressTwo'].patchValue(data.addressTwo);
    this.createCustomer.controls['City'].patchValue(data.city);
    this.createCustomer.controls['State'].patchValue(data.state);
    this.createCustomer.controls['PostalCode'].patchValue(data.postalCode);
    this.createCustomer.controls['Country'].patchValue(data.country);
    if(data.token){
      this.createCustomer.controls['Token'].patchValue(data.token);
      this.cc_added = true;
    }
    if(data.dlFront.image != ""){
      this.createCustomer.controls['DLFront'].setValue(data.dlFront.image);
    }
    if(data.dlBack.image != ""){
      this.createCustomer.controls['DLBack'].setValue(data.dlBack.image);
    }
    if(data.icFront.image != ""){
      this.createCustomer.controls['ICFront'].setValue(data.icFront.image);
    }
    if(data.icBack.image != ""){
      this.createCustomer.controls['ICBack'].setValue(data.icBack.image);
    }
  }

  submitCustomer(formData){
    console.log(formData)
    this.customer.Id = formData['Id']
    this.customer.FirstName = formData['FirstName']
    this.customer.LastName = formData['LastName']
    this.customer.PhoneNumber = formData['PhoneNumber']
    this.customer.CustomerEmail = formData['CustomerEmail']
    this.customer.AddressOne = formData['AddressOne']
    this.customer.AddressTwo = formData['AddressTwo']
    this.customer.City = formData['City']
    this.customer.State = formData['State']
    this.customer.PostalCode = formData['PostalCode']
    this.customer.Country = formData['Country']
    this.customer.Token = formData['Token']
    this.customer.DealerId = this.delaer_id
    this.customer.DLFront = formData['DLFront']
    this.customer.DLBack = formData['DLBack']
    this.customer.ICFront = formData['ICFront']
    this.customer.ICBack = formData['ICBack']
    /*
      if customer_id is got as Param, update the customer, else create new customer
    */
    if(this.customer_id){
      if(this.customer.nonce){
        this.func.presentLoading()
        let headers = new HttpHeaders();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.http.post(Config.brainTree.apiUrl + 'create_customer.php',JSON.stringify(this.customer))
          .subscribe((checkoutResult: any) => {
            this.func.dismissLoading()
            console.log("checkoutResult",checkoutResult);
            if (checkoutResult.success) {
              this.customer.Token = checkoutResult.customer_id;
              this.updateTheCustomer(this.customer)
            } else {
              this.func.showAlert('Error', 'There was an issue with your card.');
            }
          }),
          error => {
            this.func.dismissLoading()
            console.log(error.message);
            console.log(JSON.stringify(error)) // error path	
          };
      }else{
        this.updateTheCustomer(this.customer)
      }
    }
    else if(!this.cc_added){
      this.saveTheCustomer(this.customer)
    }
    else{  
      this.func.presentLoading()
      let headers = new HttpHeaders();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post(Config.brainTree.apiUrl + 'create_customer.php',JSON.stringify(this.customer))
        .subscribe((checkoutResult: any) => {
          this.func.dismissLoading()
          console.log("checkoutResult",checkoutResult);
          if (checkoutResult.success) {
            this.customer.Token = checkoutResult.customer_id;
            // this.createCustomer.controls['Token'].setValue(checkoutResult.customer_id);
            this.saveTheCustomer(this.customer)
          } else {
            this.func.showAlert('Error', 'There was an issue with your card.');
          }
        }),
        error => {
          this.func.dismissLoading()
          console.log(error.message);
          console.log(JSON.stringify(error)) // error path	
        };
      }
  }

  saveTheCustomer(data){
    // alert(JSON.stringify(data))
    this.func.presentLoading("Adding New Customer...");
    this.api.getCustomerAccessTokenExternalAPI()
    .then((token)=> {
      this.api.postCustomerToExternalAPI(data, token)
        .then(data => {
          console.log(data)
          // alert(JSON.stringify(data))
          this.func.dismissLoading();
          if(data){
            let c = {
              id: data,
              firstName: this.customer.FirstName,
              lastName: this.customer.LastName,
              phoneNumber: this.customer.PhoneNumber,
              customerEmail: this.customer.CustomerEmail
            }
            if(this.listFor == "assign_loaner"){
              this.navCtrl.push('AssignLoanerVehicleStep_4Page', {customer:c, vehicle: this.vehicle}, this.animationsOptions)
            }
            else if(this.listFor == "rental_manager_assign_loaner"){
              this.navCtrl.push('RentalManagerSelectVehiclePage', {customer:c, operationType: this.listFor}, this.animationsOptions)
            }else{
              this.navCtrl.push('CustomerListPage', {}, this.animationsOptions)
            }
            this.func.presentToast("New Customer Added Successfully", 2000, "bottom");  
          }
          else{
            this.func.presentToast("Error Occured. Please Try Again", 2000, "bottom");
          }
      })
      .catch(err => {
        this.func.dismissLoading();
        this.func.presentToast("Error Occured. Please Try Again", 2000, "bottom");
      })
    })
    .catch(err => {
      console.log(err)
      this.func.dismissLoading();
      this.func.presentToast('Something Went Wrong. Please Try Again')
    })
      
  }

  updateTheCustomer(data){
    // alert(JSON.stringify(data))
    this.func.presentLoading("Updating The Customer...");
    this.api.getCustomerAccessTokenExternalAPI()
      .then((token)=> {
        this.api.updateCustomerToExternalAPI(data, token)
          .then(data => {
            console.log(data)
            this.func.dismissLoading();
            if(data){
              if(this.listFor == "assign_loaner"){
                let c = {
                  id: this.customer.Id,
                  firstName: this.customer.FirstName,
                  lastName: this.customer.LastName,
                  phoneNumber: this.customer.PhoneNumber,
                  customerEmail: this.customer.CustomerEmail
                }
                this.navCtrl.push('AssignLoanerVehicleStep_4Page', {customer:c, vehicle: this.vehicle}, this.animationsOptions)
              }else{
                this.navCtrl.push('CustomerListPage', {}, this.animationsOptions)
              }
              this.func.presentToast("Customer Updated Successfully", 2000, "bottom");
            }
            else{
              this.func.presentToast("Error Occured. Please Try Again", 2000, "bottom");
            }
        })
      })
      .catch(err => {
        console.log(err)
        this.func.dismissLoading();
        this.func.presentToast('Something Went Wrong. Please Try Again')
      })
  }

  showConfirm() {
    const confirm = this.alertController.create({
      title: 'Are You Sure?',
      message: 'Are you sure to delete the customer?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.DeleteTheCustomer();
          }
        }
      ]
    });
    confirm.present();
  }


  DeleteTheCustomer(){
    this.func.presentLoading('Deleting The Customer...')
    let data = {
      "CustomerId" : this.customer_id
    }
    this.api.getCustomerAccessTokenExternalAPI()
      .then((token)=> {
        this.api.removeCustomerProfileFromExternalAPI(data, token)
          .then(data => {
            this.func.dismissLoading();
            if(data){
              this.navCtrl.push('CustomerListPage', {}, this.animationsOptions);
            }else{
              this.func.presentToast("Error Occured. Please Try Again", 2000, "bottom");
            }
          })
          .catch(()=>{
            this.func.dismissLoading();
            this.func.presentToast("Error Occured. Please Try Again", 2000, "bottom");
          })
        })
    .catch(err => {
      console.log(err)
      this.func.dismissLoading();
      this.func.presentToast('Something Went Wrong. Please Try Again')
    })

  }


  
  initiateCardCreation() {
		this.loading = this.loadingController.create({
			content: 'Receiving client token...'
		});
    this.loading.present();

		console.log('Receiving client token...');
		console.log(Config.brainTree.apiUrl + 'token.php');
		this.http.get(Config.brainTree.apiUrl + 'token.php',{responseType: 'text' })
			.subscribe(
				(data) => {
				console.log(`Client token received: ${data}`);
				this.showCardCreationUI(data);
				},
				error => {
          this.loading.dismissAll();
          let alert = this.alertController.create({
            title: 'Error',
            message: 'The was an issue retrieving a token. Please try again.',
            buttons: ['OK']
          });
          alert.present();
					console.log(JSON.stringify(error)) // error path	
				}
			);
  }
  private showCardCreationUI(token) {
		const paymentOptions: PaymentUIOptions = {
			amount: "0",
			primaryDescription: 'Card Creation'
		};

		this.loading.setContent('Initializing BrainTree...');
		console.log('Initializing BrainTree...');

		this.brainTree.initialize(token)
			.then(() => {
				this.loading.setContent('Setting up Apple Pay...');
				console.log('Setting up Apple Pay...');
				this.brainTree.setupApplePay(this.appleOptions);
			})
			.then(() => {
				this.loading.setContent('Presenting payment UI...');
				return this.brainTree.presentDropInPaymentUI(paymentOptions);
			})
			.then((result: PaymentUIResult) => {
				this.loading.dismissAll();
				if (result.userCancelled) {
					console.log('User cancelled payment dialog.');
				} else {
					console.log('User successfully completed payment!');
					console.log('Payment Nonce: ' + result.nonce);
					console.log('Payment Result.', JSON.stringify(result));

					this.proceedCardCreation(result.nonce);
				}
			})
			.catch((error) => {
				this.loading.dismissAll();

				let alert = this.alertController.create({
					title: 'Error',
					message: 'The was an issue with BrainTree. Check the console for details',
					buttons: ['OK']
				});
				alert.present();

				console.log('BrainTree plugin error');
				console.error(error);
			});
	}
  proceedCardCreation(nonce: string){
    this.customer.nonce = nonce;
    this.createCustomer.controls['nonce'].setValue(nonce);
    this.cc_added = true;
  }



  capture_dl_front(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        // alert(JSON.stringify(imageData))
        this.createCustomer.controls['DLFront'].patchValue(("data:image/jpeg;base64,"+imageData))
      }, error => {
        // this.func.showAlert('Error',JSON.stringify(error));
        this.func.presentToast(JSON.stringify(error));
      });
  }

   
  capture_dl_back(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.createCustomer.controls['DLBack'].setValue(("data:image/jpeg;base64,"+imageData))
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }

   
  capture_ic_front(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.createCustomer.controls['ICFront'].setValue(("data:image/jpeg;base64,"+imageData))
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }

   
  capture_ic_back(){
    this.camera.getPicture(this.cameraOptions)
      .then(imageData => {
        this.createCustomer.controls['ICBack'].setValue(("data:image/jpeg;base64,"+imageData))
      }, error => {
        this.func.presentToast(JSON.stringify(error));
      });
  }

  gotoAlertsPage(){
    this.navCtrl.push('AlertsHomePage', {}, this.animationsOptions);
  }

  back(){
    this.isRootPage == false ? this.navCtrl.pop() : this.navCtrl.setRoot('HomePage');
  }

}

