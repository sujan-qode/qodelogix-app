import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignLoanerVehicleCreateCustomerPage } from './assign-loaner-vehicle-create-customer';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AssignLoanerVehicleCreateCustomerPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AssignLoanerVehicleCreateCustomerPage),
  ],
})
export class AssignLoanerVehicleCreateCustomerPageModule {}
