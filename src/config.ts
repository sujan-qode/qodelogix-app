import { Injectable } from '@angular/core';

@Injectable()
export class Config {
	static stripePubKey = 'pk_test_SK3XyKtx23zROuzfjfXWJjwO';

	static payPalEnvironmentSandbox = 'AUp3xO-yveZDMTjZ20GWJO6c_tv7bbHrj3sZC__XyaQ7N64iVd49HRyi5WBPD00ojcHK41_hvl76PbzH';
	static payPalEnvironmentProduction = '';

	static brainTree = {
		merchantId: 'f69x887vdypwypbq',
		apiUrl: 'https://qodelogix.com/braintree/'
	}
}
