import { NgModule } from '@angular/core';
import { VehiclePartsDamageReportComponent } from './vehicle-parts-damage-report/vehicle-parts-damage-report';
import { CustomerListComponent } from './customer-list/customer-list';
import { HeaderVehicleDetailComponent } from './header-vehicle-detail/header-vehicle-detail';
import { VehicleDetailListComponent } from './vehicle-detail-list/vehicle-detail-list';
import { VehicleDetailListNewComponent } from './vehicle-detail-list-new/vehicle-detail-list-new';
import { CustomerList_2Component } from './customer-list-2/customer-list-2';
import { VehicleDetailListNew_2Component } from './vehicle-detail-list-new-2/vehicle-detail-list-new-2';
import { HeaderVehicleDetailNewComponent } from './header-vehicle-detail-new/header-vehicle-detail-new';
import { VehiclePartsDamageReport_2Component } from './vehicle-parts-damage-report-2/vehicle-parts-damage-report-2';
import { RentalManagerProgressBarComponent } from './rental-manager-progress-bar/rental-manager-progress-bar';
import { PipesModule } from '../pipes/pipes.module'
import { IonicModule } from 'ionic-angular';

@NgModule({
	declarations: [
        VehiclePartsDamageReportComponent,
        CustomerListComponent,
        HeaderVehicleDetailComponent,
        VehicleDetailListComponent,
        VehicleDetailListNewComponent,
        CustomerList_2Component,
        VehicleDetailListNew_2Component,
        HeaderVehicleDetailNewComponent,
        VehiclePartsDamageReport_2Component,
        RentalManagerProgressBarComponent
    ],
	imports: [
        PipesModule, 
        IonicModule,
    ],
	exports: [
        VehiclePartsDamageReportComponent,
        CustomerListComponent,
        HeaderVehicleDetailComponent,
        VehicleDetailListComponent,
        VehicleDetailListNewComponent,
        CustomerList_2Component,
        VehicleDetailListNew_2Component,
        HeaderVehicleDetailNewComponent,
        VehiclePartsDamageReport_2Component,
        RentalManagerProgressBarComponent
    ]
})
export class ComponentsModule {}
