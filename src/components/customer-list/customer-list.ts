import { customFunctions } from './../../providers/functions';
// import { AssignLoanerVehicleCreateCustomerPage } from './../../pages/assign-loaner-vehicle-create-customer/assign-loaner-vehicle-create-customer';
import { NavController } from 'ionic-angular';
import { ApiProvider } from './../../providers/api/api';
import { Component, Input } from '@angular/core';
// import { AssignLoanerVehicleStep_4Page } from '../../pages/assign-loaner-vehicle-step-4/assign-loaner-vehicle-step-4';


/**
 * Generated class for the CustomerListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'customer-list',
  templateUrl: 'customer-list.html'
})
export class CustomerListComponent {

  customers;
  filterData;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  @Input() listFor;
  @Input() vehicle;

  constructor(public navCtrl: NavController,public api: ApiProvider, private func: customFunctions) {
    
    this.getAllCustomers();
  }

  getAllCustomers(){
    this.func.presentLoading();
    let data = {
                "CustomerId" : "",
                "DealerId" : localStorage.getItem('dealer_id')
              }
    
    this.api.getCustomerAccessTokenExternalAPI()
      .then((token)=> {
        this.api.getCustomerFromExternalAPI(data, token)
          .then(data => {
            console.log(data)
            this.func.dismissLoading();
            if(data){
              this.customers = data;
              this.filterData = data;
            }else{
              this.func.dismissLoading();
              this.func.presentToast('Something Went Wrong. Please Try Again')
            }
          })
          .catch(()=>{
            this.func.dismissLoading();
            this.func.presentToast('Something Went Wrong. Please Try Again')
          })
      })
      .catch(err => {
        console.log(err)
        this.func.dismissLoading();
        this.func.presentToast('Something Went Wrong. Please Try Again')
      })
  }

  filterCustomer(input: any) {
    let val = input.target.value;
    console.log(this.customers)
    if (val && val.trim() != '') {
      this.filterData = this.customers.filter((customer) => {
        if(val !== undefined){
          let customerDetail = customer.firstName + " "+ customer.lastName+ " "+ customer.license_plate+" "+customer.phoneNumber;
          return ((customerDetail.toLowerCase().indexOf(val.toLowerCase()) > -1) );
          // return ((customer.customer_first.toLowerCase().indexOf(val.toLowerCase()) > -1) || (customer.customer_last.toLowerCase().indexOf(val.toLowerCase()) > -1) || (customer.license_plate.toLowerCase().indexOf(val.toLowerCase()) > -1)  || (customer.phone.indexOf(val.toLowerCase()) > -1));
        }
      });
    }
    else{
      this.getAllCustomers();
    }
  }

  
  gotoCreateCustomerPage(){
    this.navCtrl.push('AssignLoanerVehicleCreateCustomerPage', {listFor: this.listFor, vehicle: this.vehicle, from_customer_list:"Create Customer"}, this.animationsOptions)
    // if(this.listFor == "assign_loaner"){
    //   this.navCtrl.push('AssignLoanerVehicleCreateCustomerPage', {vehicle: this.vehicle}, this.animationsOptions)
    // }else{
    //   this.navCtrl.push('CreateCustomerPage', {}, this.animationsOptions)
    // }
  }

  gotoCustomerProfile(customer){
    console.log(this.listFor, this.vehicle)
    if(this.listFor == "assign_loaner"){
      this.navCtrl.push('AssignLoanerVehicleStep_4Page', {customer:customer, vehicle: this.vehicle}, this.animationsOptions)
    }else{
      this.navCtrl.push('AssignLoanerVehicleCreateCustomerPage', {customer_id:customer.id, from_customer_list:"Customer Profile"}, this.animationsOptions)
    }
  }

}
