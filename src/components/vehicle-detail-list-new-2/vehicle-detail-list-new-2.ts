import { Component, Input } from '@angular/core';
import { env } from '../../environment';

/**
 * Generated class for the VehicleDetailListNew_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'vehicle-detail-list-new-2',
  templateUrl: 'vehicle-detail-list-new-2.html'
})
export class VehicleDetailListNew_2Component {
  @Input() vehicle;
  @Input() showIndicators = true;
  baseUrl = env.base;
  hasAlert = false;

  constructor() {
    console.log('Hello VehicleDetailListNewComponent Component');
    setTimeout(()=>{
      this.vehicle.battery_level = parseFloat(this.vehicle.battery_level);
      this.vehicle.fuel_level = parseFloat(this.vehicle.fuel_level);
      this.vehicle.mileage = parseFloat(this.vehicle.mileage);

      if(this.vehicle.battery_level/1000 < this.vehicle.min_batt_voltage){
        this.hasAlert = true;
      }
      if(this.vehicle.fuel_level < this.vehicle.min_fuel_level){
        this.hasAlert = true;
      }
      if(this.vehicle.mileage > this.vehicle.max_mileage_level){
        this.hasAlert = true;
      }
      //Disconnected vehicle alert
      if(this.vehicle.isactive == 0){
        this.hasAlert = true;
      }

      // console.log(this.vehicle.battery_level/1000,
      //   this.vehicle.min_batt_voltage,
      //   this.vehicle.fuel_level,
      //   this.vehicle.min_fuel_level,
      //   this.vehicle.mileage,
      //   this.vehicle.max_mileage_level)
    }, 500)
  }

}
