import { Component, Input } from '@angular/core';

/**
 * Generated class for the RentalManagerProgressBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'rental-manager-progress-bar',
  templateUrl: 'rental-manager-progress-bar.html'
})
export class RentalManagerProgressBarComponent {


  @Input() total : number;
  @Input() active : number;

  progress = [];

  constructor() {
    console.log('Hello RentalManagerProgressBarComponent Component');

    setTimeout(()=>{
      console.log(this.total, this.active)

      for( let i = 0; i <this.total; i++){
        if(i < this.active){
          this.progress.push('<div class="active">'+(i+1)+'</div>');
        }else{
          this.progress.push('<div class="">'+(i+1)+'</div>');
        }
      }
    },100)
    
  }

}
