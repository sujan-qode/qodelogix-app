import { Platform } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { env } from '../../environment';

/**
 * Generated class for the HeaderVehicleDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'header-vehicle-detail',
  templateUrl: 'header-vehicle-detail.html'
})
export class HeaderVehicleDetailComponent {

  @Input() vehicle;
  @Input() checkin_type;
  @Input() show_spray_name = 1;
  
  baseUrl = env.base


  isTabletOrIpad: boolean;
  interface:string="popover";

  constructor(private platform: Platform) {
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
    if(!this.isTabletOrIpad){
      this.interface='action-sheet';
    }
    if(this.checkin_type == 3 || this.checkin_type == 4){
      this.show_spray_name == 0;
    }
    setTimeout(()=>{
      console.log(this.vehicle)
    },100)
  }

}
