import { Component, Input } from '@angular/core';
import { env } from '../../environment';

/**
 * Generated class for the HeaderVehicleDetailNewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'header-vehicle-detail-new',
  templateUrl: 'header-vehicle-detail-new.html'
})
export class HeaderVehicleDetailNewComponent {

  @Input() vehicle;
  
  baseUrl = env.base


  distance_unit;
  constructor() {
    this.distance_unit =  localStorage.getItem('distance_unit') ? localStorage.getItem('distance_unit') : 'km';
  }

}
