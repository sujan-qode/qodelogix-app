import { ApiProvider } from './../../providers/api/api';
import { Component, Input } from '@angular/core';
// import { DamageReportingAddPopoverPage } from '../../pages/damage-reporting-add-popover/damage-reporting-add-popover';
import { PopoverController } from 'ionic-angular';
/**
 * Generated class for the VehiclePartsDamageReportComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'vehicle-parts-damage-report',
  templateUrl: 'vehicle-parts-damage-report.html'
})

export class VehiclePartsDamageReportComponent {

  
  damageInfo:any;

  @Input() vehicle;
  @Input() checkin_type;

  constructor(private api: ApiProvider,
              private popoverCtrl: PopoverController
  ) {
    console.log('Hello VehiclePartsDamageReportComponent Component');
    setTimeout(()=> {
      this.getDamageInfo(this.vehicle.vin_number, this.checkin_type);
    },100);
    
  }

  getDamageInfo(vin_number, checkin_type){
    this.api.getDamageInfo(vin_number, checkin_type)
        .then( data => {
          // console.log(data)
          this.damageInfo = data;
        })
  }

  openPopover(damage_location) {
    console.log(damage_location)
    let popover = this.popoverCtrl.create('DamageReportingAddPopoverPage', {damage_location: damage_location, vehicle:this.vehicle, checkin_type: this.checkin_type}, { cssClass: 'damage-report-popover'});
    popover.present();
    popover.onDidDismiss(() => {
      this.getDamageInfo(this.vehicle.vin_number, this.checkin_type);
    })
  }

  

}
