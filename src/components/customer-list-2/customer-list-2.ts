import { Component, Input } from '@angular/core';
// import { AssignLoanerVehicleCreateCustomerPage } from '../../pages/assign-loaner-vehicle-create-customer/assign-loaner-vehicle-create-customer';
import { NavController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { customFunctions } from '../../providers/functions';
// import { RentalManagerSelectVehiclePage } from '../../pages/rental-manager-select-vehicle/rental-manager-select-vehicle';

/**
 * Generated class for the CustomerList_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'customer-list-2',
  templateUrl: 'customer-list-2.html'
})
export class CustomerList_2Component {

  customers;
  filterData;

  animationsOptions = {
    animation: 'ios-transition',
    duration: 1000
  }

  @Input() listFor;

  constructor(public navCtrl: NavController,
              public api: ApiProvider, 
              private func: customFunctions) {

    setTimeout(()=> {
      this.func.presentLoading();
      console.log("operation => "+this.listFor)
      if(this.listFor == "rental_manager_edit_loaner"){
        this.displayAssuCustomerList();
      }else{
        this.getAllCustomers();
      }
    }, 500)
  }

  getAllCustomers(){
    let data = {
                "CustomerId" : "",
                "DealerId" : localStorage.getItem('dealer_id')
              }
     this.api.getCustomerAccessTokenExternalAPI()
      .then((token)=> {
        this.api.getCustomerFromExternalAPI(data, token)
          .then(data => {
            console.log(data)
            this.func.dismissLoading();
            if(data){
              this.customers = data;
              this.filterData = data;
            }else{
              this.func.dismissLoading();
              this.func.presentToast('Something Went Wrong. Please Try Again')
            }
          })
          .catch(()=>{
            this.func.dismissLoading();
            this.func.presentToast('Something Went Wrong. Please Try Again')
          })
      })
      .catch(err => {
        console.log(err)
        this.func.dismissLoading();
        this.func.presentToast('Something Went Wrong. Please Try Again')
      })
  }

  displayAssuCustomerList(){
    this.api.displayAssuCustomerList()
      .then(data => {
        console.log(data)
        this.func.dismissLoading();
        if(data){
          this.customers = data;
          this.filterData = data;
        }else{
          this.func.dismissLoading();
          this.func.presentToast('Something Went Wrong. Please Try Again')
        }
      })
      .catch(()=>{
        this.func.dismissLoading();
        this.func.presentToast('Something Went Wrong. Please Try Again')
       
      })
  }

  filterCustomer(input: any) {
    let val = input.target.value;
    console.log(this.customers)
    if (val && val.trim() != '') {
      this.filterData = this.customers.filter((customer) => {
        if(val !== undefined){
          let customerDetail = customer.firstName + " "+ customer.lastName+ " "+ customer.license_plate+" "+customer.phoneNumber;
          return ((customerDetail.toLowerCase().indexOf(val.toLowerCase()) > -1) );
          // return ((customer.customer_first.toLowerCase().indexOf(val.toLowerCase()) > -1) || (customer.customer_last.toLowerCase().indexOf(val.toLowerCase()) > -1) || (customer.license_plate.toLowerCase().indexOf(val.toLowerCase()) > -1)  || (customer.phone.indexOf(val.toLowerCase()) > -1));
        }
      });
    }
    else{
      if(this.listFor == "rental_manager_edit_loaner"){
        this.displayAssuCustomerList();
      }else{
        this.getAllCustomers();
      }
    }
  }

  
  gotoRentalManagerSelectVehicle(customer){
    this.navCtrl.push('RentalManagerSelectVehiclePage', {operationType: this.listFor, listFor: this.listFor, customer:customer}, this.animationsOptions)
  }

  gotoCreateCustomerPage(customer){
    if(this.listFor == "rental_manager_assign_loaner"){
      this.navCtrl.push('AssignLoanerVehicleCreateCustomerPage', {listFor: this.listFor}, this.animationsOptions)
    }else{
    }
  }

}
