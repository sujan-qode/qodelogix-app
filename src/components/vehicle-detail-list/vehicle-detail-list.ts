import { env } from './../../environment';
import { Component, Input } from '@angular/core';
import { Platform } from 'ionic-angular';

/**
 * Generated class for the VehicleDetailListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'vehicle-detail-list',
  templateUrl: 'vehicle-detail-list.html'
})
export class VehicleDetailListComponent {

  @Input() vehicle;
  @Input() showIndicators = true;
  baseUrl = env.base;
  isTabletOrIpad:boolean;

  constructor(private platform: Platform) {
    console.log('Hello VehicleDetailListComponent Component');
    this.isTabletOrIpad = this.platform.is('tablet') || this.platform.is('ipad');
  }



}
