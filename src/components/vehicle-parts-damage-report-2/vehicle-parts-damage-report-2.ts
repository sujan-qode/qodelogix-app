import { Component, Input } from '@angular/core';
import { ApiProvider } from '../../providers/api/api';
import { PopoverController } from 'ionic-angular';
// import { DamageReportingAddPopoverPage } from '../../pages/damage-reporting-add-popover/damage-reporting-add-popover';

/**
 * Generated class for the VehiclePartsDamageReport_2Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'vehicle-parts-damage-report-2',
  templateUrl: 'vehicle-parts-damage-report-2.html'
})
export class VehiclePartsDamageReport_2Component {
  damageInfo:any;

  @Input() vehicle;
  

  constructor(private api: ApiProvider,
              private popoverCtrl: PopoverController
  ) {
    console.log('Hello VehiclePartsDamageReportComponent Component');
    setTimeout(()=> {
      this.getDamageInfo(this.vehicle.vin_number, 0);
    },100);
    
  }

  getDamageInfo(vin_number, checkin_type){
    this.api.getDamageInfo(vin_number, checkin_type)
        .then( data => {
          // console.log(data)
          this.damageInfo = data;
        })
  }

  openPopover(damage_location) {
    console.log(damage_location)
    let popover = this.popoverCtrl.create('DamageReportingAddPopoverPage', {damage_location: damage_location, vehicle:this.vehicle}, { cssClass: 'damage-report-popover'});
    popover.present();
    popover.onDidDismiss(() => {
      this.getDamageInfo(this.vehicle.vin_number, 0);
    })
  }

}

