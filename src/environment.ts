export const env = {
    base                : "",
    url                 : "http://dealer.assurantdrive.com/api/",
    // base                : "http://admin.qodelogix.com/",
    // url                 : "http://admin.qodelogix.com/api/",
    login_point         : "user-login", //
    change_password     : "change-password",//
    alerts_point        : "all-alert",//
    battery_alert       : "battery-alert",//
    fuel_alert          : "fuel-alert",//
    mileage_alert       : "mileage-alert",//
    gps_alert           : "show-gps-alert-app",//
    update_profile      : "update-profile",//
    vehicles_point      : "all-vehicle-list",//
    update_vehicle      : "update-vehicle-profile-status", //unused
    damage_locations    : "damage-locations",//
    damage_types        : "damage-types",//
    insert_damage_info  : "insertVehicleDamageInfo",//
    update_damage_info  : "updateVehicleDamageInfo", //
    delete_damage_info  : "deleteDamageInfo",//
    get_damage_info     : "displayDamageInfo",//
    create_accessories_checklist    : "createAccessoriesChecklist", //
    create_vehicle_damage   :   "createVehicleDamages",//
    get_vehicle_damage  :   "displayVehicleDamage",//
    get_accessories_checklist   :   "accessoriesChecklist",//
    decode_vin  :   "decode-vin", //
    customer_list   :   "customer-list",//
    create_customer :   "create-customer", //
    upload_card_images  :   "card-images", //
    get_vehicle_profile :   "vehicle-profile",//
    get_customer_profile    :   "customerProfile",// 
    update_customer_profile :   "update-customer", //
    update_customer_cards   :   "update-customer-cards", //unused
    delete_customer_vehicle :   "delete-customer-vehicle", //
    checkin :   "check-in", 
    deleteCustomer : "deleteCustomer",
    getDeliveryVehicles : 'getDeliveryVehicles',
    getServiceVehicles  :   'getServiceVehicles',
    displayAvailableLoaners :   'displayAvailableLoaners',
    updateUserProfile  :    'user-profile-update',
    getAlertTypeForSettings    :   'getAlertType',
    updateAlertTypesForSettings : "updateAlertTypes",
    getDealerLonerAgreement :   "getDealerLonerAgreement",
    getloanerassigned   :   "getloanerassigned",
    setLoanerAssigned   :   "setLoanerAssigned",
    returnLoanerAssigned :  "returnLoanerAssigned",
    getGeoFence :   "getGeoFence",
    getVehicleTypes :   "getVehicleTypes",
    getColorList    :   "get-color-list",
    setColor    :   "set-color-list",
    advanceSearch   :   "advanceSearch",
    modelAvailableList  :   "modelAvailableList",
    colorList   :   "colorList",
    setUserDevice   :   "setUserDevice",
    getRentalType   :   "getRentalType",
    displayAssuCustomerList :   "displayAssuCustomerList",
    displayAssuCustomerVehicleList    :   "displayAssuCustomerVehicleList",
    loanerReassignEmail :   "loanerReassignEmail",
    updateLoanerAssigned    :   "updateLoanerAssigned",
    allAlertCount   :   "allAlertCount",
    getTokenforCustomer :   "getTokenforCustomer",  
};