import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { env } from '../../environment';
import { HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Events, Platform } from 'ionic-angular';
import 'rxjs/add/operator/timeout';

@Injectable()
export class ApiProvider {

  vehicle_api         = env.url + env.vehicles_point;
  alerts_api          = env.url + env.alerts_point;
  battery_alert_api   = env.url + env.battery_alert;
  fuel_alert_api      = env.url + env.fuel_alert;
  mileage_alert_api   = env.url + env.mileage_alert;
  gps_alert_api       = env.url + env.gps_alert;
  damage_location_api = env.url + env.damage_locations;
  damage_type_api     = env.url + env.damage_types; 
  insert_vehicle_damage_api = env.url + env.insert_damage_info;
  update_vehicle_damage_api = env.url + env.update_damage_info;
  delete_vehicle_damage_api = env.url + env.delete_damage_info;
  get_vehicle_damage_info_api    = env.url + env.get_damage_info;
  create_accessories_checklist_api  = env.url + env.create_accessories_checklist;
  create_vehicle_damage_api = env.url + env.create_vehicle_damage;
  get_vehicle_damage_api = env.url + env.get_vehicle_damage;
  get_accessories_checklist_api = env.url + env.get_accessories_checklist;
  decode_vin_api = env.url + env.decode_vin;
  customer_list_api = env.url + env.customer_list;
  create_customer_api = env.url + env.create_customer;
  upload_card_images = env.url + env.upload_card_images;
  get_vehicle_profile = env.url + env.get_vehicle_profile;
  get_customer_profile  = env.url + env.get_customer_profile;
  update_customer_profile = env.url + env.update_customer_profile;
  update_customer_cards = env.url + env.update_customer_cards;
  delete_customer_vehicle =  env.url + env.delete_customer_vehicle;
  checkin = env.url + env.checkin;
  delete_customer = env.url + env.deleteCustomer;
  get_delivery_vehicles = env.url + env.getDeliveryVehicles;
  get_service_vehicles = env.url + env.getServiceVehicles;
  get_available_loaners = env.url + env.displayAvailableLoaners;
  update_user_profile = env.url + env.updateUserProfile;
  get_alert_type_for_settings = env.url + env.getAlertTypeForSettings;
  update_alert_type_for_settings = env.url + env.updateAlertTypesForSettings;
  get_loaner_agreement  =   env.url + env.getDealerLonerAgreement;
  get_loaner_assigned = env.url + env.getloanerassigned;
  set_loaner_assigned = env.url + env.setLoanerAssigned;
  return_loaner_assigned = env.url + env.returnLoanerAssigned;
  get_geofence  = env.url + env.getGeoFence;
  get_vehicle_types = env.url + env.getVehicleTypes;
  get_color_list  = env.url + env.getColorList;
  set_color_list = env.url + env.setColor;
  advance_search = env.url + env.advanceSearch;
  color_list = env.url + env.colorList;
  model_available_list = env.url + env.modelAvailableList;
  set_user_device = env.url + env.setUserDevice;
  get_rental_type = env.url + env.getRentalType;
  display_assu_customer_list = env.url + env.displayAssuCustomerList;
  display_assu_customer_vehicle_list = env.url + env.displayAssuCustomerVehicleList;
  resend_loaner_assign_email = env.url + env.loanerReassignEmail;
  update_loaner_assigned = env.url + env.updateLoanerAssigned;
  all_alerts_count = env.url + env.allAlertCount;
  get_assurant_customer_token = env.url + env.getTokenforCustomer;

  allVehiclesStorageData: any;
  isValidToken = true;

  idParams;
  
  constructor(private event:Events ,public http: HttpClient, private storage:Storage, private platform: Platform) {
    if(localStorage.getItem('auth_token')){
      // this.getFromStorageStandard();
      // this.getAllAlertsCount();
    }
  }

  
  // async getFromStorageStandard(){
  //   this.storage.ready().then(() => {
  //     this.storage.get('allVehicles').then(data => {
  //       if (data != undefined) {
  //         this.allVehiclesStorageData = data;
  //       }
  //     });
  //   });
  // }

  // getAllVehiclesFromStorage(){
  //    let data = (JSON.parse(JSON.stringify(this.allVehiclesStorageData)));
  //    // console.log(data)
  //    return data;
  // }


  getAllVehicles(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.vehicle_api, {headers: headers, params:idParams})
        .subscribe(
          data => {
            this.storage.set('allVehicles', data['message']);
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  
  async getAllAlertsCount(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
        this.http.get(this.alerts_api+"/1", {headers: headers, params:idParams})
        .subscribe(
          data => {
            localStorage.setItem('allAlertsCount', data['data']['alert-list'].length);
            localStorage.setItem('distance_unit', data['data']['dealerSelectedUnits'][0]['distance_unit'])
            localStorage.setItem('fuel_unit', data['data']['dealerSelectedUnits'][0]['fuel_unit'])
            resolve(true);
          },
          error => {
            // console.log('error')
            this.event.publish('token-changed');
            resolve(false);
          }
        );
    });
  }

  getAllAlerts(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.alerts_api+"/1" , {headers: headers, params:idParams})
        .subscribe(
          data => {
            console.log(data);
            resolve(data['data']['alert-list']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getBatteryAlerts(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      // this.http.get(this.battery_alert_api, {headers: headers, params:idParams})
      this.http.get(this.alerts_api+"/2" , {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['data']['alert-list']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getFuelAlerts(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      // this.http.get(this.fuel_alert_api, {headers: headers, params:idParams})
      this.http.get(this.alerts_api+"/3" , {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['data']['alert-list']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getGpsAlerts(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      // this.http.get(this.gps_alert_api, {headers: headers, params:idParams})
      this.http.get(this.alerts_api+"/4" , {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['data']['alert-list']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getMileageAlerts(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      // this.http.get(this.mileage_alert_api, {headers: headers, params:idParams})
      this.http.get(this.alerts_api+"/5" , {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['data']['alert-list']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  
  getDisconnectAlerts(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.alerts_api+"/6" , {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['data']['alert-list']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }


  getDamageLocations(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.damage_location_api, {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getDamageTypes(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.damage_type_api, {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }


  
  insertVehicleDamageInfo(info){
    info.append('dealer_id', localStorage.getItem('dealer_id'))
    let headers = new HttpHeaders({
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    return new Promise( resolve => {
      this.http.post(this.insert_vehicle_damage_api, info, { headers: headers})
        .subscribe( data => {
          // console.log(data)
            resolve(data['message']);
        });
    });
  }

  updateVehicleDamageInfo(info){
    let headers = new HttpHeaders({
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    return new Promise( resolve => {
      this.http.post(this.update_vehicle_damage_api, info, { headers: headers})
        .subscribe( data => {
            resolve(data['message']);
        });
    });
  }

  deleteVehicleDamageInfo(info){
    let headers = new HttpHeaders({
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    return new Promise( resolve => {
      this.http.post(this.delete_vehicle_damage_api, info, { headers: headers})
        .subscribe( data => {
            resolve(data['message']);
        });
    });
  }

  getDamageInfo(vin_number, checkin_type){
    //Create new HttpParams
    let params= new HttpParams()
                  .set("vin_number",vin_number)
                  .set("checkin_type",checkin_type)
                  .set('dealer_id', localStorage.getItem('dealer_id'))
                  .set('user_id', localStorage.getItem('auth_id'))
     let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise( resolve => {
      this.http.get(this.get_vehicle_damage_info_api, { headers: headers, params:params})
        .subscribe( data => {
          // // console.log(data)
          if(data['status'] == 'success'){
            resolve(data['message']);
          }
        });
    });
  }

  createVehicleDamage(damageReport){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    return new Promise( resolve => {
      this.http.post(this.create_vehicle_damage_api, damageReport, { headers: headers})
        .subscribe( data => {
          resolve(data);
        });
    });
  }

  
  createAccessoriesChecklist(checkInAccessories){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    return new Promise( resolve => {
      this.http.post(this.create_accessories_checklist_api, checkInAccessories, { headers: headers})
        .subscribe( data => {
          resolve(data);
        });
    });
  }

  getVehicleDamage(vin_number, checkin_type){
    let params= new HttpParams()
                  .set("vin_number",vin_number)
                  .set("checkin_type",checkin_type)
                  .set('dealer_id', localStorage.getItem('dealer_id'))
                  .set('user_id', localStorage.getItem('auth_id'))
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise(resolve => {
      this.http.get(this.get_vehicle_damage_api, {headers: headers, params:params})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getAccessoriesChecklist(vin_number, checkin_type){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    let params= new HttpParams()
                  .set("vin_number",vin_number)
                  .set("checkin_type",checkin_type)
                  .set('dealer_id', localStorage.getItem('dealer_id'))
                  .set('user_id', localStorage.getItem('auth_id'))
    return new Promise(resolve => {
      this.http.get(this.get_accessories_checklist_api, {headers: headers, params:params})
        .subscribe(
          data => {
            // // console.log(data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }


  decodeVin(vin_number){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    let params= new HttpParams()
                  .set("vinNumber",vin_number)
                  .set('dealer_id', localStorage.getItem('dealer_id'))
                  .set('user_id', localStorage.getItem('auth_id'))
    return new Promise(resolve => {
      this.http.get(this.decode_vin_api, {headers: headers, params:params})
        .subscribe(
          data => {
            resolve(data['msg']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  customerList(){
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.customer_list_api, {params:idParams})
        .subscribe(
          data => {
            // // console.log(data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getVehicleProfile(vin){
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.get_vehicle_profile+'/'+vin, {params:idParams})
        .subscribe(
          data => {
            // // console.log('getVehicleProfile', data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  createCustomer(customer){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise( resolve => {
      this.http.post(this.create_customer_api, customer, { headers: headers})
        .subscribe(
          data => {
            // // console.log(data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  

  uploadCardImage(image){
    return new Promise( resolve => {
      this.http.post(env.url+'uploadCardImages', image)
        .subscribe(
          data => {
            if(data['status'] == "Success"){
              resolve(data['message']);
            }else{
              resolve(null)
            }
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }


  uploadCardImageNames(images){
    return new Promise( resolve => {
      this.http.post(this.upload_card_images, images)
        .subscribe(
          data => {
            // // console.log('here '+data);
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  updateCardImages(images){
    return new Promise( resolve => {
      this.http.post(this.update_customer_cards, images)
        .subscribe(
          data => {
            // // console.log('here '+data);
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getCustomerProfile(customer_id){
    let params= new HttpParams()
                  .set("customer_id",customer_id)
                  .set('dealer_id', localStorage.getItem('dealer_id'))
                  .set('user_id', localStorage.getItem('auth_id'))
    return new Promise(resolve => {
      this.http.get(this.get_customer_profile, {params: params})
        .subscribe(
          data => {
            // // console.log(data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  updateCustomerProfile(customer){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise( resolve => {
      this.http.post(this.update_customer_profile, customer, { headers: headers})
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  deleteCustomerVehicle(vehicle_id){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    let body = 'id=' + vehicle_id;
    return new Promise( resolve => {
      this.http.post(this.delete_customer_vehicle, body, { headers: headers})
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  deleteCustomer(customer_id){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    let body = 'customer_id=' + customer_id;
    return new Promise( resolve => {
      this.http.post(this.delete_customer, body,  {headers: headers})
        .subscribe(
          data => {
            if(data['status'] == "Success"){
              resolve(data['message']);
            }else{
              resolve(null)
            }
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  checkIn(vin_number, checkin_type){ 
  let headers = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': "Bearer "+ localStorage.getItem('auth_token')
  });

    let body = 'vin_number='+vin_number+'&checkin_type='+checkin_type+'&user_id='+localStorage.getItem('auth_id')+'&dealer_id='+localStorage.getItem('dealer_id');
    // const body = new FormData();
    // body.append('vin_number',vin_number);
    // body.append('check_in_type',checkin_type);
    // {vin_number: vin_number, check_in_type: checkin_type};
    return new Promise( resolve => {
      this.http.post(this.checkin, body,  {headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            if(data['status'] == "Success"){
              resolve(data['message']);
            }else{
              resolve(false)
            }
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getDeliveryVehicles(){
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.get_delivery_vehicles, {params:idParams})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getServiceVehicles(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.get_service_vehicles, {headers: headers, params:idParams})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  
  getAvailableLoaners(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.get_available_loaners, {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  updateUserProfile(data){
    let headers = new HttpHeaders({
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    return new Promise( resolve => {
      this.http.post(this.update_user_profile, data, { headers: headers})
        .subscribe( data => {
            resolve(data['message']);
        });
    });
  }

  getAlertTypeForSettings(user_id){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise(resolve => {
      this.http.get(this.get_alert_type_for_settings+"/"+localStorage.getItem('dealer_id')+"/"+user_id,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['message']);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  updateAlertTypeForSettings(data){
    let headers = new HttpHeaders({
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    return new Promise( resolve => {
      this.http.post(this.update_alert_type_for_settings, data, { headers: headers})
        .subscribe( data => {
          // console.log(data)
            resolve(data['message']);
        });
    });
  }

  getLoanerAgreement(){
    return new Promise(resolve => {
      this.http.get(this.get_loaner_agreement+"/"+localStorage.getItem('dealer_id'))
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getCustomerAccessTokenExternalAPI(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': "application/json"
    });

    return new Promise( resolve => {
      this.http.get(this.get_assurant_customer_token, { headers:headers})
        .subscribe(
          data => {
            let status = data['status'];
            if( status.toLowerCase() == "success"){
              resolve(data['message']['access_token']);
            }
            resolve(false)
          },
          error => {
            console.log(error)
            resolve(false);
          }
        );
    });
  }
  
  
  getCustomerFromExternalAPI(data, token){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ token
    });

    let apiUrl;
    if (this.platform.is('cordova')) {                // <<< is Cordova available?
      apiUrl = 'http://model-api.assuredrive.com/api/WebApiDealership/CustomerList';
    }else{
      apiUrl = "/CustomerList"
    }
    return new Promise( resolve => {
      this.http.post(apiUrl, data, { headers:headers})
      //  .timeout(30000)
        .subscribe(
          data => {
            // console.log(data)
            if(data['status'] == "OK"){
              resolve(data['customerData']);
            }
            resolve(false)
          },
          error => {
            resolve(false);
          }
        );
    });
  }

  postCustomerToExternalAPI(data, token){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ token
    });

    let apiUrl1;
    if (this.platform.is('cordova')) {                // <<< is Cordova available?
      apiUrl1 = 'http://model-api.assuredrive.com/api/WebApiDealership/CreateCustomerProfile';
    }else{
      apiUrl1 = "/CreateCustomerProfile"
    }
    return new Promise( resolve => {
      this.http.post(apiUrl1, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            if(data['status'] == "OK"){
              resolve(data['customerId']);
            }
            resolve(false)
          },
          error => {
            resolve(false);
          }
        );
    });
  }

  updateCustomerToExternalAPI(data, token){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ token
    });

    let apiUrl2;
    if (this.platform.is('cordova')) {                // <<< is Cordova available?
      apiUrl2 = 'http://model-api.assuredrive.com/api/WebApiDealership/UpdateCustomerProfile';
    }else{
      apiUrl2 = "/EditCustomerProfile"
    }
    return new Promise( resolve => {
      this.http.post(apiUrl2, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            if(data['status'] == "OK"){
              resolve(data);
            }
            resolve(false)
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  removeCustomerProfileFromExternalAPI(data, token){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ token
    });

    let apiUrl2;
    if (this.platform.is('cordova')) {                // <<< is Cordova available?
      apiUrl2 = 'http://model-api.assuredrive.com/api/WebApiDealership/RemoveCustomerProfile';
    }else{
      apiUrl2 = "/RemoveCustomerProfile"
    }
    return new Promise( resolve => {
      this.http.post(apiUrl2, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            if(data['status'] == "OK"){
              resolve(true);
            }
            resolve(false)
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }


  getCustomerProfileFromExternalAPI(data, token){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ token
    });

    let apiUrl3;
    if (this.platform.is('cordova')) {                // <<< is Cordova available?
      apiUrl3 = 'http://model-api.assuredrive.com/api/WebApiDealership/GetCustomerProfile';
    }else{
      apiUrl3 = "/GetCustomerProfile"
    }

    return new Promise( resolve => {
      this.http.post(apiUrl3, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            if(data['status'] == "OK"){
              resolve(data['customerData']);
            }
            resolve(false)
          },
          error => {
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  setLoanerAssigned(data){
    
    let headers = new HttpHeaders({
      'enctype': 'multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise( resolve => {
      this.http.post(this.set_loaner_assigned, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data)
          },
          error => {
            console.log(error.statusText)
            resolve(false);
          }
        );
    });
  }

  updateLoanerAssigned(data){
    let headers = new HttpHeaders({
      'enctype': 'multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise( resolve => {
      this.http.post(this.update_loaner_assigned, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data)
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getLoanerAssigned(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise( resolve => {
      this.http.get(this.get_loaner_assigned+"/"+localStorage.getItem('dealer_id'),  {headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data)
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  returnLoanerAssigned(customer_id, vin_number){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    let data = 'vin_number='+vin_number+'&customer_id='+customer_id+'&user_id='+localStorage.getItem('auth_id')+'&dealer_id='+localStorage.getItem('dealer_id');
    return new Promise( resolve => {
      this.http.post(this.return_loaner_assigned, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data)
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getGeofence(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    let data = 'dealer_id='+localStorage.getItem('dealer_id')
    return new Promise( resolve => {
      this.http.post(this.get_geofence, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data)
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getvehicleTypes(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise( resolve => {
      this.http.get(this.get_vehicle_types,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message'])
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getColorList(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    return new Promise( resolve => {
      this.http.get(this.get_color_list+'/'+localStorage.getItem('dealer_id'),  {headers: headers})
        .subscribe(
          data => {
            // console.log(data['message'])
            resolve(data['message'])
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  setColorList(color, vin){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });

    let data = "vin_number="+vin+"&color_id="+color.id+"&status="+color.status;
    // console.log(data)
    return new Promise( resolve => {
      this.http.post(this.set_color_list, data, { headers: headers})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message'])
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getAllColorList(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise( resolve => {
      this.http.get(this.color_list,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message'])
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  getAllvehiclemodels(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise( resolve => {
      this.http.get(this.model_available_list,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message'])
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  advanceSearch(vehicle_type, model_type, trimAndFeature, vehicle_color, location){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))
                    .set("vehicle_type",vehicle_type)
                    .set("model",model_type)
                    .set("model_trim",trimAndFeature)
                    .set("vehicle_color",vehicle_color)
                    .set("location",location)

    return new Promise( resolve => {
      this.http.get(this.advance_search,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            // console.log(data)
            resolve(data['message'])
          },
          error => {
            // console.log(error.statusText)
            console.log(error.statusText);             resolve(false);
          }
        );
    });
  }

  setUserDevice(device_id){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let data = 'user_id='+localStorage.getItem('auth_id')+'&dealer_id='+localStorage.getItem('dealer_id')+'&device_id='+device_id;
    console.log(data)
    return new Promise( resolve => {
      this.http.post(this.set_user_device, data, { headers: headers})
      .subscribe(
        data => {
          console.log(data)
          resolve(true)
        },
        error => {
          console.log(error.statusText)
          resolve(false);
        }
      );
    })
  }

  getRentalType(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise( resolve => {
      this.http.get(this.get_rental_type,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['message'])
          },
          error => {
            console.log(error.statusText)
            resolve(false);
          }
        );
    });
  }

  displayAssuCustomerList(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise( resolve => {
      this.http.get(this.display_assu_customer_list,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['message'])
          },
          error => {
            console.log(error.statusText)
            resolve(false);
          }
        );
    });
  }

  displayAssuCustomerVehicleList(token, vin_number){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))
                    .set("token",token)
                    .set("vin_number",vin_number)

    return new Promise( resolve => {
      this.http.get(this.display_assu_customer_vehicle_list,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['message'])
          },
          error => {
            console.log(error.statusText)
            resolve(false);
          }
        );
    });
  }

  resendLoanerAssignEmail(vin_number, customer_email){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))
                    .set("vin_number",vin_number)
                    .set("customer_email",customer_email)

    return new Promise( resolve => {
      this.http.get(this.resend_loaner_assign_email,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['message'])
          },
          error => {
            console.log(error.statusText)
            resolve(false);
          }
        );
    });
  }

  getAlertsCount(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': "Bearer "+ localStorage.getItem('auth_token')
    });
    let idParams = new HttpParams()
                    .set("dealer_id",localStorage.getItem('dealer_id'))
                    .set("user_id",localStorage.getItem('auth_id'))

    return new Promise( resolve => {
      this.http.get(this.all_alerts_count,  {headers: headers, params:idParams})
        .subscribe(
          data => {
            resolve(data['message'])
          },
          error => {
            console.log(error.statusText)
            resolve(false);
          }
        );
    });
  }


}
