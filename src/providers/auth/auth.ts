import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { env } from '../../environment';
// import { Storage } from '@ionic/storage';
// import { LoginPage } from '../../pages/login/login';
import { App } from 'ionic-angular';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor(public http: HttpClient, private app: App) { }

  logout(){
    this.removeUser();
    var nav = this.app.getRootNav();
    nav.setRoot('LoginPage');
  }

  login(user){
    let auth_url = env.url+env.login_point;
    var data = {
        email : user.email,
        password: user.password
      };
      
      let headers = new HttpHeaders();
      headers.set('Content-Type', 'application/x-www-form-urlencoded');
      return new Promise( resolve => {
        this.http.post(auth_url, data, { headers: headers})
          .subscribe( data => {
            console.log(data)
            let dt=data['status'];
            if(dt == 'success'){
              this.storeUser(data['message']);
              this.storeDealer(data['dealer']);
              resolve(true);
            }
            else{
              resolve(false);
            }
          },
          error => {
            resolve(false);
          }
        );
          
      });
    }

    changePassword(email){
      let change_pass_url = env.url+env.change_password;
      var data = {
          email: email
        };
      return new Promise( resolve => {
        this.http.post(change_pass_url, data)
          .subscribe( data => {
            resolve(data);
          });
      });
    }

    storeUser(user){
      localStorage.setItem('auth_token', user.api_token);
      localStorage.setItem('auth_id', user.id);
      localStorage.setItem('name', user.full_name);
      localStorage.setItem('avatar', user.avatar);
      localStorage.setItem('email', user.email);
      localStorage.setItem('phone_number', user.phone_number);
      localStorage.setItem('dealer_id', user.dealer_id);
    }

    storeDealer(dealer){
      localStorage.setItem('dealer_name', dealer.name);
      localStorage.setItem('dealer_logo', dealer.logo);
    }

    removeUser(){
      localStorage.clear();
    }

}
