import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, } from 'ionic-angular';

import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { EmailComposer } from '@ionic-native/email-composer';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { ApiProvider } from '../providers/api/api';

import { Camera } from '@ionic-native/camera';
import { File } from "@ionic-native/file";
import { FileTransfer} from '@ionic-native/file-transfer';
import { customFunctions } from '../providers/functions';
import { Network } from '@ionic-native/network';

import { TextMaskModule } from 'angular2-text-mask';
import { Braintree } from '@ionic-native/braintree';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Geolocation } from '@ionic-native/geolocation';

import { OneSignal } from '@ionic-native/onesignal'
import { LottieAnimationViewModule } from 'ng-lottie';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md'
      // backButtonText: 'Go Back',
      // mode: 'md',
      // iconMode: 'ios',
      // modalEnter: 'modal-slide-in',
      // modalLeave: 'modal-slide-out',
      // tabsPlacement: 'bottom',
      // pageTransition: 'ios'
    }),
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['sqlite', 'websql', 'indexeddb']
      }),
    IonicImageViewerModule,
    TextMaskModule,
    LottieAnimationViewModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    EmailComposer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    ApiProvider,
    customFunctions,
    Camera,
    File,
    FileTransfer,
    BarcodeScanner,
    Network,
    Braintree,
    NativePageTransitions,
    Geolocation,
    OneSignal
  ]
})
export class AppModule {}
