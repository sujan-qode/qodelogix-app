import { Component, ViewChild } from '@angular/core';
import { App, Nav, Platform, Events, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { SplashScreen } from '@ionic-native/splash-screen';
import { customFunctions } from './../providers/functions';
import { AuthProvider } from '../providers/auth/auth';
import { env } from '../environment';
import { ApiProvider } from '../providers/api/api';

import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // rootPage: any = 'SplashPage';

  name;
  avatar;

  pages: Array<{icon: string, title: string, component: any}>;

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen,
              private auth: AuthProvider, 
              public _api: ApiProvider, 
              private network: Network, 
              public alertCtrl: AlertController,
              private event: Events, 
              private func: customFunctions,
              private app: App, 
              private menuCtrl: MenuController,
              private oneSignal : OneSignal,
  ) {
    this.initializeApp();
    
    setTimeout(()=>{
    var testinterval = setInterval(() => {
      if(localStorage.getItem('name') != undefined){
        this.name = localStorage.getItem('name');
        this.avatar = env.base+localStorage.getItem('avatar');
        clearInterval(testinterval)
      }
      else{
        this.name = localStorage.getItem('name');
        this.avatar = env.base+localStorage.getItem('avatar');
      }
    }, 500);
    },500)

    this.pages = [
      { icon:'dashboard.png', title: 'Dashboard', component: 'HomePage' },
      { icon:'alert.png', title: 'Alerts', component: 'AlertsHomePage' },
      { icon:'vehicles.png', title: 'Vehicle List', component: 'VehicleListWithAnimationPage' },
      { icon:'map.png', title: 'Map', component: 'MapPage' },
      { icon:'check-in.png', title: ' Check-in Vehicle', component: 'CheckInPage' },
      { icon:'customer-profile.png', title: 'Customer List', component: 'CustomerListPage' },
      // { icon:'deliver-vehicle.png', title: 'Deliver Vehicles', component: DeliveryVehiclePage },
      // { icon:'deliver-vehicle.png', title: 'Service Intake', component: ServiceVehiclePage },
      // { icon:'deliver-vehicle.png', title: 'Loaner Agreement', component: LoanerAgreementPage },
      { icon:'deliver-vehicle.png', title: 'Loaner Agreement', component: 'RentalManagerPage' },
      { icon:'setting.png', title: 'Settings', component: 'AccountSettingPage' },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {

        if(localStorage.getItem('auth_id')){
          this.nav.setRoot('HomePage');
        }else{
          this.nav.setRoot('LoginPage');
        }

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();

      if(this.platform.is('android')) {
        this.statusBar.overlaysWebView(false);
        this.statusBar.backgroundColorByHexString('#0066cc');
      }
      this.splashScreen.hide();

      this.listenConnection();
      this.listenToEvents();
      this.exitApp();

        let isNotofied = setInterval(()=>{
          if(localStorage.getItem('auth_id')){
            // this.oneSignalInitialize();
            clearInterval(isNotofied);
          }
        },1000)
      
    });
  }

  oneSignalInitialize(){
    this.oneSignal.startInit('225aadb4-8d28-45a8-a45a-4e9a304f045a');
    this.oneSignal.enableSound(true);

    this.oneSignal.inFocusDisplaying(2);

      this.oneSignal.getIds()
        .then(data => {
          // alert(data['userId'])
          let device_id = data['userId'];
          this._api.setUserDevice(device_id)
            .then(data => {
              console.log(data)
            })
            .catch(err =>{
              console.log(err)
            })
        })

    this.oneSignal.handleNotificationOpened().subscribe((data) => {
      data = JSON.parse(JSON.stringify(data));
      let vehicle = data['notification']['payload']['additionalData']['vehicle'];
      alert(JSON.stringify(vehicle))
      if(localStorage.getItem('auth_id')){
        this.nav.setRoot('HomePage', {vehicle: vehicle, topContent:0});
      }
      else{
        this.nav.setRoot('LoginPage', {vehicle: vehicle});
      }
    });

    this.oneSignal.endInit();
  }

  private listenConnection(): void {
    this.network.onDisconnect()
      .subscribe(() => {
        this.func.presentToast('You are offline. Please turn internet on to use the app.');
        // this.func.showAlert("Connection Failed !", "There may be a problem in your internet connection. Please try again ! ")
        // this.auth.logout();
      });
    this.network.onConnect()
      .subscribe(()=> {
        this.func.presentToast('You are Online.')
      })
  }

  
  listenToEvents(){
    this.event.subscribe('user:updated', data => {
      this.name = data.full_name;
      this.avatar = env.base+data.avatar;
    })
    this.event.subscribe('token-changed', data => {
      // this.func.presentToast("You have logged in from another device. Please try logging in again !", 3000, "bottom")
      // this.auth.logout();
    })
  }

  exitApp(){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        // Catches the active view
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();                
        // Checks if can go back before show up the alert

        if(this.menuCtrl.isOpen()){
          this.menuCtrl.close();  
          return;
        }
        if(activeView.name === 'HomePage') {
                  const alert = this.alertCtrl.create({
                      title: 'Exit App?',
                      message: 'You will be logged out of the app.',
                      buttons: [{
                          text: 'Cancel',
                          role: 'cancel',
                          handler: () => {
                            // this.nav.setRoot('HomePage');
                          }
                      },
                      {
                          text: 'Exit',
                          handler: () => {
                            localStorage.clear();
                            this.platform.exitApp();
                          }
                      }]
                  });
                  alert.present();
        }
        else if(activeView.name === 'CheckInPage' || activeView.name === 'RentalManagerPage') {
          this.nav.setRoot('HomePage')
        }
        else if(activeView.name == "RentalManagerSuccessPage"){
          this.nav.push('RentalManagerPage');
        }
        else{
          nav.pop();
        }
      });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, {rootPage: 1});
  }

  logout(){
    this.auth.logout();
  }

}
