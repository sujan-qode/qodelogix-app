import { NgModule } from '@angular/core';
import { VoltPipe } from './volt/volt';
import { GetFirstCharacterPipe } from './get-first-character/get-first-character';
import { GetNcharactersPipe } from './get-ncharacters/get-ncharacters';
import { RemoveNullPipe } from './remove-null/remove-null';
import { ReversePipe } from './reverse/reverse';
import { ValueOrNaPipe } from './value-or-na/value-or-na';
import { ToBooleanPipe } from './to-boolean/to-boolean';
import { KeysPipe } from './keys/keys';
import { DistanceConverterPipe } from './distance-converter/distance-converter';
@NgModule({
	declarations: [
        VoltPipe,
        GetFirstCharacterPipe,
        GetNcharactersPipe,
        RemoveNullPipe,
        ReversePipe,
        ValueOrNaPipe,
        ToBooleanPipe,
        KeysPipe,
        DistanceConverterPipe
    ],
	imports: [],
	exports: [
        VoltPipe,
        GetFirstCharacterPipe,
        GetNcharactersPipe,
        RemoveNullPipe,
        ReversePipe,
        ValueOrNaPipe,
        ToBooleanPipe,
        KeysPipe,
        DistanceConverterPipe
    ]
})
export class PipesModule {}
