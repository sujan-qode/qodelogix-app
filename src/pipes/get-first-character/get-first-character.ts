import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the GetFirstCharacterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'getFirstCharacter',
})
export class GetFirstCharacterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return value.split(' ').map(function(item){return item[0]}).join('');
  }
}
