import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ValueOrNaPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'valueOrNa',
})
export class ValueOrNaPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    // console.log(value)
    if(value == null){
      // return 'NA';
      return 'STOCK<span class="black-text">_</span>#';
    }
    return value;
  }
}
