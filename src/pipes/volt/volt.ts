import { Pipe, PipeTransform, Injectable } from '@angular/core';

/**
 * Generated class for the VoltPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */

@Pipe({
  name: 'volt',
})

@Injectable()
export class VoltPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: number, ...args) {
    return (value/1000).toFixed(2);
  }
}
