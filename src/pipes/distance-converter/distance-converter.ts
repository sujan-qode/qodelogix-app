import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the DistanceConverterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'distanceConverter',
})
export class DistanceConverterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: number) { 
    let unit = localStorage.getItem('distance_unit') ? localStorage.getItem('distance_unit') : 'km';
    unit = unit.toLocaleLowerCase();

    let distance = 0;
    switch(unit){
      case 'km' : {
        distance = value;
        break;
      }
      case 'miles' : {
        distance = parseInt((value * 0.621371).toFixed(2));
        break;
      }
      default: {
        distance = value;
        break;
      }
    }
    console.log(value, unit, distance);
    return distance;
  }
}
