import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the GetNcharactersPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'getNcharacters',
})
export class GetNcharactersPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return value.substring(0, args[0]);
  }
}
