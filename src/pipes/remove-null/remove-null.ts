import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the RemoveNullPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'removeNull',
})
export class RemoveNullPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    if( value == 'null' || value == 'undefined' || !value )
      value= '';
    return value;
  }
}
