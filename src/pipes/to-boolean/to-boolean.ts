import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ToBooleanPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'toBoolean',
})
export class ToBooleanPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return (parseInt(value) == 0) ? false : true;
  }
}
