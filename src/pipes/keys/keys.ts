import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the KeysPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'keysPipe',
})
export class KeysPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let keys = [];
    console.log("pipe.value",value);
    for (let key in value) {
      keys.push({key: key, value: value[key]});
    }
    console.log("Pipe",keys)
    return keys;
  }
}
